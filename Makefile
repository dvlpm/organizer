.PHONY: build

### ---------------------
###	Build
### ---------------------
include docker/Makefile

#-------------------------------------
# Shortcuts
#-------------------------------------
dc = docker-compose -f docker-compose.yaml -f docker-compose.nginx.yaml
dci = $(dc) -f docker-compose.app-infrastructure.yaml -f docker-compose.postgres.yaml -f docker-compose.ui.app.yaml
dcai = $(dc) -f docker-compose.app-infrastructure.yaml
dcp = $(dc) -f docker-compose.postgres.yaml
dcn = $(dc) -f docker-compose.nginx.yaml
dcua = $(dc) -f docker-compose.ui.app.yaml
gradle = docker run -v $(PWD):/app -w /app -v ~/.gradle:/home/gradle/.gradle gradle:7.6.1-jdk17 gradle

#-------------------------------------
# Variables
#-------------------------------------
VERSION=latest
PWD=$(shell pwd)
LOCAL_HOST=organizer.local

#-------------------------------------
# Build
#-------------------------------------
build: build-app-jar build-app build-node build-nginx

build-app-jar:
	# Add -S -d for explicit log
	$(gradle) build --no-daemon -x test

#-------------------------------------
# Init App
#-------------------------------------
init: cp-env build up-infrastructure migrate up-nginx up

cp-env:
	cp -n .env .env.local 2>/dev/null; true

migrate:
	$(gradle) flywayMigrate --no-daemon

#-------------------------------------
# Run App
#-------------------------------------
up:
	$(dc) up -d

ups:
	$(dc) up

down:
	$(dc) down

restart:
	$(dc) restart app

sh:
	$(dc) exec app bin/bash

logs:
	$(dc) logs -f app

#-------------------------------------
# Go external
#-------------------------------------
ngrok:
	ngrok http $(LOCAL_HOST)

#-------------------------------------
# Run Infrastructure
#-------------------------------------
up-infrastructure:
	$(dci) up -d

ups-infrastructure:
	$(dci) up

down-infrastructure:
	$(dci) down

temporal-admin-sh:
	$(dci) exec temporal-admin-tools sh

#-------------------------------------
# Run App Infrastructure
#-------------------------------------
up-app-infrastructure:
	$(dcai) up -d

ups-app-infrastructure:
	$(dcai) up

down-app-infrastructure:
	$(dcai) down

#-------------------------------------
# Run postgres
#-------------------------------------
up-postgres:
	$(dcp) up -d

ups-postgres:
	$(dcp) up

down-postgres:
	$(dcp) down

#-------------------------------------
# Run nginx
#-------------------------------------
up-nginx:
	$(dcn) up -d

ups-nginx:
	$(dcn) up

down-nginx:
	$(dcn) down

#-------------------------------------
# Run UI App
#-------------------------------------
up-ui-app:
	$(dcua) up -d

sh-ui-app:
	$(dcua) exec ui-app-node sh

ups-ui-app:
	$(dcua) up

down-ui-app:
	$(dcua) down
