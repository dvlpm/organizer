import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.6.2"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("org.flywaydb.flyway") version "8.5.10"
    kotlin("jvm") version "1.8.22"
    kotlin("plugin.spring") version "1.8.22"
    kotlin("plugin.jpa") version "1.8.22"
}

group = "pm.dvl"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
    maven {
        url = uri("https://oss.sonatype.org/content/repositories/snapshots")
        mavenContent {
            snapshotsOnly()
        }
    }
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:1.6.4")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.15.0")
    implementation("com.fasterxml.jackson:jackson-bom:2.15.0")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("io.temporal:temporal-sdk:1.6.0")
    implementation("io.temporal:temporal-kotlin:1.6.0")
    implementation("com.github.applicaai:spring-boot-starter-temporal:0.9.0-SNAPSHOT")
    implementation("org.mnode.ical4j:ical4j:3.2.1")
    implementation("org.mnode.ical4j:ical4j-serializer:0.1.6")
    implementation("com.github.pengrad:java-telegram-bot-api:6.2.0")
    implementation("io.grpc:grpc-api:1.42.1")
    implementation("io.grpc:grpc-stub:1.42.1")
    implementation("io.grpc:grpc-core:1.42.1")
    implementation("org.springframework.boot:spring-boot-starter-amqp")
    implementation("org.axonframework:axon-spring-boot-starter:4.5.9")
    implementation("org.axonframework:axon-spring:4.5.9")
    implementation("org.axonframework:axon-modelling:4.5.9")
    implementation("org.axonframework:axon-messaging:4.5.9")
    implementation("org.axonframework:axon-eventsourcing:4.5.9")
    implementation("org.axonframework.extensions.amqp:axon-amqp:4.5")
    implementation("org.axonframework.extensions.amqp:axon-amqp-spring-boot-autoconfigure:4.5")
    implementation("org.axonframework.extensions.kotlin:axon-kotlin:0.2.0")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.7.13")
    implementation("org.postgresql:postgresql:42.3.8")
    implementation("org.jetbrains.kotlin:kotlin-maven-noarg:1.8.20")
    implementation("org.jetbrains.kotlin:kotlin-maven-allopen:1.8.20")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
}

noArg {
    annotation("pm.dvl.organizer.common.annotations.Persistable")
}

sourceSets {
    main {
        resources {
            srcDirs("resources", "python")
        }
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.jar {
    enabled = false
}

tasks.withType<ProcessResources>() {
    duplicatesStrategy = DuplicatesStrategy.INCLUDE
}

buildscript {
    dependencies {
        classpath("com.h2database:h2:1.4.197")
    }
}

flyway {
    configFiles = arrayOf("./src/main/resources/flyway.conf")
}
