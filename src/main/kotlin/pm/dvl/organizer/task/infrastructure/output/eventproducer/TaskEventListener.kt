package pm.dvl.organizer.task.infrastructure.output.eventproducer

import org.axonframework.eventhandling.EventHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.event.TaskCreatedEvent
import pm.dvl.organizer.task.domain.event.TaskStateChangedEvent

@Component
class TaskEventListener(
    @Autowired private val applicationEventPublisher: ApplicationEventPublisher,
) {
    @EventHandler
    fun on(event: TaskCreatedEvent) {
        applicationEventPublisher.publishEvent(
            pm.dvl.organizer.task.infrastructure.output.eventproducer.event.TaskCreatedEvent(
                event.taskId, event.taskData, event.taskState, event.taskSchedule
            )
        )
    }

    @EventHandler
    fun on(event: TaskStateChangedEvent) {
        applicationEventPublisher.publishEvent(
            pm.dvl.organizer.task.infrastructure.output.eventproducer.event.TaskStateChangedEvent(
                event.taskId, event.taskState
            )
        )
    }
}
