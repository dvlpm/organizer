package pm.dvl.organizer.task.infrastructure.input.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.toTaskState
import pm.dvl.organizer.task.domain.query.projection.TaskProjection
import pm.dvl.organizer.task.infrastructure.input.facade.TaskFacade
import pm.dvl.organizer.task.infrastructure.input.facade.dto.AddParentToTaskDto
import pm.dvl.organizer.task.infrastructure.input.facade.dto.RemoveParentFromTaskDto
import pm.dvl.organizer.task.infrastructure.input.controller.request.AddParentToTaskRequest
import pm.dvl.organizer.task.infrastructure.input.controller.request.ChangeTaskStateRequest
import pm.dvl.organizer.task.infrastructure.input.controller.request.RemoveParentFromTaskRequest
import pm.dvl.organizer.task.infrastructure.input.facade.dto.ChangeTaskStateDto
import java.util.*

@RestController
class TaskController(
    @Autowired private val taskFacade: TaskFacade
) {
    // TODO add user id (by token) to controller methods injection
    @GetMapping(PATH)
    fun list(): List<TaskProjection> {
        return taskFacade.findAll()
    }

    @GetMapping("${PATH}/{taskId}")
    fun find(
        @PathVariable("taskId")
        taskId: String?,
    ): TaskProjection? {
        taskId ?: return null

        return taskFacade.findOneOrNullById(taskId.toTaskId())
    }

    @PostMapping("${PATH}/{taskId}/change-state")
    fun changeState(
        @PathVariable("taskId")
        taskId: String?,
        @RequestBody
        request: ChangeTaskStateRequest
    ) {
        taskId ?: return
        request.state ?: return

        taskFacade.changeState(
            ChangeTaskStateDto(
                taskId.toTaskId(),
                request.state.toTaskState(),
            )
        )
    }

    @PostMapping("${PATH}/{taskId}/add-parent")
    fun addParent(
        @PathVariable("taskId")
        taskId: String?,
        @RequestBody
        request: AddParentToTaskRequest
    ) {
        taskId ?: return
        request.parentTaskId ?: return

        taskFacade.addParent(
            AddParentToTaskDto(
                taskId.toTaskId(),
                request.parentTaskId.toTaskId(),
            )
        )
    }

    @PostMapping("${PATH}/{taskId}/remove-parent")
    fun removeParent(
        @PathVariable("taskId")
        taskId: String?,
        @RequestBody
        request: RemoveParentFromTaskRequest
    ) {
        taskId ?: return
        request.parentTaskId ?: return

        taskFacade.removeParent(
            RemoveParentFromTaskDto(
                taskId.toTaskId(),
                request.parentTaskId.toTaskId(),
            )
        )
    }

    companion object {
        const val PATH = "/api/task-schedule"
    }

    fun String.toTaskId(): TaskId {
        return TaskId(UUID.fromString(this))
    }
}
