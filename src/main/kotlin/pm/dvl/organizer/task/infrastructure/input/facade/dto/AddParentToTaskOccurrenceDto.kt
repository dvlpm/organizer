package pm.dvl.organizer.task.infrastructure.input.facade.dto

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId

data class AddParentToTaskOccurrenceDto(
    val taskOccurrenceId: TaskOccurrenceId,
    val parentTaskOccurrenceId: TaskOccurrenceId,
)