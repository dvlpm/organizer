package pm.dvl.organizer.task.infrastructure.input.facade

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.command.AddParentToTaskCommand
import pm.dvl.organizer.task.domain.command.ChangeTaskStateCommand
import pm.dvl.organizer.task.domain.command.CreateTaskCommand
import pm.dvl.organizer.task.domain.command.RemoveParentFromTaskCommand
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.query.projection.TaskProjection
import pm.dvl.organizer.task.domain.query.repository.TaskProjectionRepositoryInterface
import pm.dvl.organizer.task.infrastructure.input.facade.dto.AddParentToTaskDto
import pm.dvl.organizer.task.infrastructure.input.facade.dto.ChangeTaskStateDto
import pm.dvl.organizer.task.infrastructure.input.facade.dto.CreateTaskDto
import pm.dvl.organizer.task.infrastructure.input.facade.dto.RemoveParentFromTaskDto

@Component
class TaskFacade(
    @Autowired private val commandGateway: CommandGateway,
    @Autowired private val taskProjectionRepository: TaskProjectionRepositoryInterface
) {
    fun create(dto: CreateTaskDto) {
        commandGateway.send<Unit>(
            CreateTaskCommand(
                dto.taskId,
                dto.taskData,
                dto.schedule,
            )
        )
    }

    fun changeState(dto: ChangeTaskStateDto) {
        commandGateway.send<Unit>(
            ChangeTaskStateCommand(
                dto.taskId,
                dto.state
            )
        )
    }

    fun addParent(dto: AddParentToTaskDto) {
        commandGateway.send<Unit>(
            AddParentToTaskCommand(
                dto.taskId,
                dto.parentTaskId
            )
        )
    }

    fun removeParent(dto: RemoveParentFromTaskDto) {
        commandGateway.send<Unit>(
            RemoveParentFromTaskCommand(
                dto.taskId,
                dto.parentTaskId
            )
        )
    }

    fun findOneOrNullById(id: TaskId): TaskProjection? {
        return this.taskProjectionRepository.findOneOrNullById(id)
    }

    fun findAll(): List<TaskProjection> {
        return this.taskProjectionRepository.findAll()
    }
}
