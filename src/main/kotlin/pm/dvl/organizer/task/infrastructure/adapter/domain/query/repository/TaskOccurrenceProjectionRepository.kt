package pm.dvl.organizer.task.infrastructure.adapter.domain.query.repository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.query.projection.TaskOccurrenceProjection
import pm.dvl.organizer.task.domain.query.repository.TaskOccurrenceProjectionRepositoryInterface
import pm.dvl.organizer.user.domain.model.UserId

@Component
class TaskOccurrenceProjectionRepository(
    @Autowired private val jpaRepository: TaskOccurrenceProjectionJpaRepository
) : TaskOccurrenceProjectionRepositoryInterface {
    override val attempts: Int
        get() = 10
    override val delay: Long
        get() = 1000L

    override fun save(projection: TaskOccurrenceProjection) {
        jpaRepository.saveAndFlush(projection)
    }

    override fun findOneOrNullById(id: TaskOccurrenceId): TaskOccurrenceProjection? {
        return jpaRepository.findByIdOrNull(id)
    }

    override suspend fun waitForFindOneOrNullById(id: TaskOccurrenceId): TaskOccurrenceProjection? {
        return waitFor { findOneOrNullById(id) }
    }

    override fun findAllByReporterId(reporterUserId: UserId): List<TaskOccurrenceProjection> {
        return jpaRepository.findAllByReporterUserId(reporterUserId)
    }

    override fun findAll(): List<TaskOccurrenceProjection> {
        return jpaRepository.findAll()
    }
}
