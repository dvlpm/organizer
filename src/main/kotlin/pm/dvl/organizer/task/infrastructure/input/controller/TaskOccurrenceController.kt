package pm.dvl.organizer.task.infrastructure.input.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.query.projection.TaskOccurrenceProjection
import pm.dvl.organizer.task.infrastructure.input.facade.TaskOccurrenceFacade
import java.util.*

@RestController
class TaskOccurrenceController(
    @Autowired private val taskOccurrenceFacade: TaskOccurrenceFacade
) {
    @GetMapping(PATH)
    fun list(): List<TaskOccurrenceProjection> {
        return taskOccurrenceFacade.findAll()
    }

    @GetMapping("${PATH}/{taskId}")
    fun find(
        @PathVariable("taskId")
        taskId: String?,
    ): TaskOccurrenceProjection? {
        taskId ?: return null

        return taskOccurrenceFacade.findOneOrNullById(taskId.toTaskId())
    }

    companion object {
        const val PATH = "/api/task"
    }

    fun String.toTaskId(): TaskOccurrenceId {
        return TaskOccurrenceId(UUID.fromString(this))
    }
}
