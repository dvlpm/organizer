package pm.dvl.organizer.task.infrastructure.output.eventproducer.event

import org.springframework.context.ApplicationEvent
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId

class TaskOccurrenceStartedEvent(
    val taskOccurrenceId: TaskOccurrenceId,
    val taskData: TaskData,
) : ApplicationEvent(taskOccurrenceId)
