package pm.dvl.organizer.task.infrastructure.input.controller.request

data class AddParentToTaskRequest(
    val parentTaskId: String? = null,
)
