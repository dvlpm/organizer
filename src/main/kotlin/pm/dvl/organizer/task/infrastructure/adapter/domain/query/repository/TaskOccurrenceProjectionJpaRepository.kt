package pm.dvl.organizer.task.infrastructure.adapter.domain.query.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.query.projection.TaskOccurrenceProjection
import pm.dvl.organizer.user.domain.model.UserId

@Repository
interface TaskOccurrenceProjectionJpaRepository : JpaRepository<TaskOccurrenceProjection, TaskOccurrenceId>
{
    fun findAllByReporterUserId(reporterUserId: UserId): List<TaskOccurrenceProjection>
}
