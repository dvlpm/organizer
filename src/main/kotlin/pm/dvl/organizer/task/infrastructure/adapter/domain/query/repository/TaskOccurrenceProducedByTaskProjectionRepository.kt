package pm.dvl.organizer.task.infrastructure.adapter.domain.query.repository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.query.projection.TaskOccurrenceProducedByTaskProjection
import pm.dvl.organizer.task.domain.query.repository.TaskOccurrenceProducedByTaskProjectionRepositoryInterface

@Component
class TaskOccurrenceProducedByTaskProjectionRepository(
    @Autowired private val jpaRepository: TaskOccurrenceProducedByTaskProjectionJpaRepository
) : TaskOccurrenceProducedByTaskProjectionRepositoryInterface {
    override fun save(projection: TaskOccurrenceProducedByTaskProjection) {
        jpaRepository.saveAndFlush(projection)
    }
}
