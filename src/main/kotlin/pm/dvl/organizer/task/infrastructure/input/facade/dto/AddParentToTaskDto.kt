package pm.dvl.organizer.task.infrastructure.input.facade.dto

import pm.dvl.organizer.task.domain.model.TaskId

data class AddParentToTaskDto(
    val taskId: TaskId,
    val parentTaskId: TaskId,
)
