package pm.dvl.organizer.task.infrastructure.input.eventlistener

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import pm.dvl.organizer.calendar.infrastructure.output.event.EventOccurrenceOccurredEvent
import pm.dvl.organizer.task.domain.command.ProduceTaskOccurrenceCommand
import pm.dvl.organizer.task.domain.command.StartTaskOccurrenceCommand
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.query.repository.TaskOccurrenceProjectionRepositoryInterface
import pm.dvl.organizer.task.domain.query.repository.TaskProjectionRepositoryInterface

@Component
class EventOccurrenceOccurredEventListener(
    @Autowired val commandGateway: CommandGateway,
    @Autowired val taskProjectionRepository: TaskProjectionRepositoryInterface,
    @Autowired val taskOccurrenceProjectionRepository: TaskOccurrenceProjectionRepositoryInterface,
) : ApplicationListener<EventOccurrenceOccurredEvent> {
    override fun onApplicationEvent(event: EventOccurrenceOccurredEvent) {
        maybeProduceTask(event)
        maybeStartTask(event)
    }

    // TODO add some attribute to event.occurrence to define which operation is needed
    private fun maybeProduceTask(event: EventOccurrenceOccurredEvent) {
        val taskId = TaskId(event.occurrence.calendarId.value)

        if (taskProjectionRepository.findOneOrNullById(taskId) == null) return

        commandGateway.send<Unit>(
            ProduceTaskOccurrenceCommand(
                taskId,
                TaskOccurrenceId(event.occurrence.eventId.value),
                event.occurrence.startAt
            )
        )
    }

    // TODO add some attribute to event.occurrence to define which operation is needed
    private fun maybeStartTask(event: EventOccurrenceOccurredEvent) {
        val taskOccurrenceId = TaskOccurrenceId(event.occurrence.calendarId.value)

        if (taskOccurrenceProjectionRepository.findOneOrNullById(taskOccurrenceId) == null) return

        commandGateway.send<Unit>(
            StartTaskOccurrenceCommand(
                taskOccurrenceId,
            )
        )
    }
}
