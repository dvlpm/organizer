package pm.dvl.organizer.task.infrastructure.adapter.domain.service

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.calendar.domain.model.CalendarId
import pm.dvl.organizer.calendar.infrastructure.input.facade.CalendarFacade
import pm.dvl.organizer.calendar.infrastructure.input.dto.StartCalendarDto
import pm.dvl.organizer.calendar.infrastructure.input.dto.DisableCalendarDto
import pm.dvl.organizer.calendar.infrastructure.input.dto.EnableCalendarDto
import pm.dvl.organizer.task.domain.command.ProduceTaskOccurrenceCommand
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.service.*
import java.time.ZonedDateTime
import java.util.UUID

@Component
class TaskOccurrenceManager(
    @Autowired val commandGateway: CommandGateway,
    @Autowired private val calendarFacade: CalendarFacade
) : TaskOccurrenceManagerInterface {
    override fun produce(payload: ProduceTaskOccurrencePayload) {
        commandGateway.send<Unit>(ProduceTaskOccurrenceCommand(
            payload.taskId,
            payload.taskOccurrenceId,
            payload.taskStartAt,
            payload.taskOccurrenceParentIds,
        ))
    }

    override fun scheduleProduction(payload: ScheduleTaskOccurrencesProductionPayload) {
        if (payload.schedule === null) {
            commandGateway.send<Unit>(ProduceTaskOccurrenceCommand(
                payload.taskId,
                TaskOccurrenceId(UUID.randomUUID()),
                ZonedDateTime.now()
            ))

            return
        }

        calendarFacade.start(StartCalendarDto(payload.taskId.toCalendarId(), payload.schedule.calendar))
    }

    override fun disable(payload: DisableTaskOccurrencesPayload) {
        calendarFacade.disable(DisableCalendarDto(payload.taskId.toCalendarId()))
    }

    override fun enable(payload: EnableTaskOccurrencesPayload) {
        calendarFacade.enable(EnableCalendarDto(payload.taskId.toCalendarId()))
    }

    fun TaskId.toCalendarId(): CalendarId {
        return CalendarId(value)
    }
}
