package pm.dvl.organizer.task.infrastructure.input.facade

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.command.ChangeTaskOccurrenceStateCommand
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.query.projection.TaskOccurrenceProjection
import pm.dvl.organizer.task.domain.query.repository.TaskOccurrenceProjectionRepositoryInterface
import pm.dvl.organizer.task.infrastructure.input.facade.dto.ChangeTaskOccurrenceStateDto
import pm.dvl.organizer.user.domain.model.UserId

@Component
class TaskOccurrenceFacade(
    @Autowired private val commandGateway: CommandGateway,
    @Autowired private val taskProjectionRepository: TaskOccurrenceProjectionRepositoryInterface
) {
    fun changeState(dto: ChangeTaskOccurrenceStateDto) {
        commandGateway.send<Unit>(
            ChangeTaskOccurrenceStateCommand(
                dto.taskOccurrenceId,
                dto.taskOccurrenceState
            )
        )
    }

    fun findOneOrNullById(id: TaskOccurrenceId): TaskOccurrenceProjection? {
        return this.taskProjectionRepository.findOneOrNullById(id)
    }

    fun findAllByReporterUserId(reporterUserId: UserId): List<TaskOccurrenceProjection> {
        return this.taskProjectionRepository.findAllByReporterId(reporterUserId)
    }

    fun findAll(): List<TaskOccurrenceProjection> {
        return this.taskProjectionRepository.findAll()
    }
}
