package pm.dvl.organizer.task.infrastructure.output.eventproducer

import org.axonframework.eventhandling.EventHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.event.TaskOccurrenceStartedEvent
import pm.dvl.organizer.task.domain.event.TaskOccurrenceStateChangedEvent

@Component
class TaskOccurrenceEventListener(
    @Autowired private val applicationEventPublisher: ApplicationEventPublisher,
) {
    @EventHandler
    fun on(event: TaskOccurrenceStartedEvent) {
        applicationEventPublisher.publishEvent(
            pm.dvl.organizer.task.infrastructure.output.eventproducer.event.TaskOccurrenceStartedEvent(
                event.taskOccurrenceId,
                event.taskData
            )
        )
    }

    @EventHandler
    fun on(event: TaskOccurrenceStateChangedEvent) {
        applicationEventPublisher.publishEvent(
            pm.dvl.organizer.task.infrastructure.output.eventproducer.event.TaskOccurrenceStateChangedEvent(
                event.taskOccurrenceId,
                event.state
            )
        )
    }
}
