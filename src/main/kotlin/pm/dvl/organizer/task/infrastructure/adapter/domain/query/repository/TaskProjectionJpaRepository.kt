package pm.dvl.organizer.task.infrastructure.adapter.domain.query.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.query.projection.TaskProjection

@Repository
interface TaskProjectionJpaRepository : JpaRepository<TaskProjection, TaskId>
