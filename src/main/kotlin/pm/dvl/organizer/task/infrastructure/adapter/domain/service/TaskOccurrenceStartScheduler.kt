package pm.dvl.organizer.task.infrastructure.adapter.domain.service

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.calendar.domain.model.CalendarId
import pm.dvl.organizer.calendar.infrastructure.input.dto.StartCalendarDto
import pm.dvl.organizer.calendar.infrastructure.input.facade.CalendarFacade
import pm.dvl.organizer.common.extensions.toCalendar
import pm.dvl.organizer.task.domain.command.StartTaskOccurrenceCommand
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.service.ScheduleTaskOccurrenceStartPayload
import pm.dvl.organizer.task.domain.service.TaskOccurrenceStartSchedulerInterface
import java.time.ZonedDateTime

@Component
class TaskOccurrenceStartScheduler(
    @Autowired val commandGateway: CommandGateway,
    @Autowired private val calendarFacade: CalendarFacade
) : TaskOccurrenceStartSchedulerInterface {
    override fun schedule(payload: ScheduleTaskOccurrenceStartPayload) {
        if (payload.startAt === null || payload.startAt <= ZonedDateTime.now()) {
            commandGateway.send<StartTaskOccurrenceCommand>(StartTaskOccurrenceCommand(payload.taskOccurrenceId))

            return
        }

        calendarFacade.start(StartCalendarDto(payload.taskOccurrenceId.toCalendarId(), payload.startAt.toCalendar()))
    }

    private fun TaskOccurrenceId.toCalendarId(): CalendarId {
        return CalendarId(value)
    }
}
