package pm.dvl.organizer.task.infrastructure.input.controller.request

data class ChangeTaskStateRequest(
    val state: String? = null,
)
