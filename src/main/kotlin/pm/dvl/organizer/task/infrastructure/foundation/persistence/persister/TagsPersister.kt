package pm.dvl.organizer.task.infrastructure.foundation.persistence.persister

import org.hibernate.cache.spi.access.CollectionDataAccess
import org.hibernate.collection.spi.PersistentCollection
import org.hibernate.engine.spi.SharedSessionContractImplementor
import org.hibernate.mapping.Collection
import org.hibernate.persister.collection.BasicCollectionPersister
import org.hibernate.persister.entity.EntityPersister
import org.hibernate.persister.spi.PersisterCreationContext
import java.io.Serializable

class TagsPersister(
    collectionBinding: Collection,
    cacheAccessStrategy: CollectionDataAccess?,
    creationContext: PersisterCreationContext,
): BasicCollectionPersister(collectionBinding, cacheAccessStrategy, creationContext) {
    override fun getRole(): String {
        return super.getRole()
    }

    override fun getElementPersister(): EntityPersister {
        return super.getElementPersister()
    }

    override fun insertRows(
        collection: PersistentCollection?,
        id: Serializable?,
        session: SharedSessionContractImplementor?
    ) {
        super.insertRows(collection, id, session)
    }
}
