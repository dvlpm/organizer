package pm.dvl.organizer.task.infrastructure.input.controller.request

data class RemoveParentFromTaskRequest(
    val parentTaskId: String? = null,
)
