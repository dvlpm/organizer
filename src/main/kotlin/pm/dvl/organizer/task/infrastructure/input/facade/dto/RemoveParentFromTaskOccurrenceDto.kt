package pm.dvl.organizer.task.infrastructure.input.facade.dto

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId

data class RemoveParentFromTaskOccurrenceDto(
    val taskOccurrenceId: TaskOccurrenceId,
    val parentTaskOccurrenceId: TaskOccurrenceId,
)
