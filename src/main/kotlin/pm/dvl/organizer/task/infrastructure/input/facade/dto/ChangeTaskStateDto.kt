package pm.dvl.organizer.task.infrastructure.input.facade.dto

import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskState

data class ChangeTaskStateDto(
    val taskId: TaskId,
    val state: TaskState
)
