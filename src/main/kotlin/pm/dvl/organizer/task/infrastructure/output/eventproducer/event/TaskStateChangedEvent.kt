package pm.dvl.organizer.task.infrastructure.output.eventproducer.event

import org.springframework.context.ApplicationEvent
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskState

class TaskStateChangedEvent(
    val taskId: TaskId,
    val state: TaskState,
) : ApplicationEvent(taskId)
