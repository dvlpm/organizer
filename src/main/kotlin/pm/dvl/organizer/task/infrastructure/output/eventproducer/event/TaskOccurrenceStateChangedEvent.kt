package pm.dvl.organizer.task.infrastructure.output.eventproducer.event

import org.springframework.context.ApplicationEvent
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskOccurrenceState

class TaskOccurrenceStateChangedEvent(
    val taskOccurrenceId: TaskOccurrenceId,
    val state: TaskOccurrenceState,
) : ApplicationEvent(taskOccurrenceId)