package pm.dvl.organizer.task.infrastructure.foundation.configuration

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration

@Configuration
@EntityScan("pm.dvl.organizer.task.domain.query.projection")
class JpaTaskOccurrenceConfiguration {}
