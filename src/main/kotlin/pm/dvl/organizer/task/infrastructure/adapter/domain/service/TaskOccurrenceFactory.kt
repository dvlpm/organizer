package pm.dvl.organizer.task.infrastructure.adapter.domain.service

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.command.CreateTaskOccurrenceCommand
import pm.dvl.organizer.task.domain.service.CreateTaskOccurrencePayload
import pm.dvl.organizer.task.domain.service.TaskOccurrenceFactoryInterface

@Component
class TaskOccurrenceFactory(@Autowired val commandGateway: CommandGateway) : TaskOccurrenceFactoryInterface {
    override fun create(payload: CreateTaskOccurrencePayload) {
        commandGateway.send<CreateTaskOccurrenceCommand>(
            CreateTaskOccurrenceCommand(
                payload.taskOccurrenceId,
                payload.taskData,
                payload.taskOccurrenceParentIds
            )
        )
    }
}
