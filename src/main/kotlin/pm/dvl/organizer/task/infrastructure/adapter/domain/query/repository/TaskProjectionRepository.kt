package pm.dvl.organizer.task.infrastructure.adapter.domain.query.repository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.query.projection.TaskProjection
import pm.dvl.organizer.task.domain.query.repository.TaskProjectionRepositoryInterface

@Component
class TaskProjectionRepository(
    @Autowired private val jpaRepository: TaskProjectionJpaRepository
) : TaskProjectionRepositoryInterface {
    override val attempts: Int
        get() = 10
    override val delay: Long
        get() = 1000L

    override fun save(projection: TaskProjection) {
        jpaRepository.saveAndFlush(projection)
    }

    override fun findOneOrNullById(id: TaskId): TaskProjection? {
        return jpaRepository.findByIdOrNull(id)
    }

    override suspend fun waitForFindOneOrNullById(id: TaskId): TaskProjection? {
        return waitFor { findOneOrNullById(id) }
    }

    override fun findAll(): List<TaskProjection> {
        return jpaRepository.findAll()
    }
}
