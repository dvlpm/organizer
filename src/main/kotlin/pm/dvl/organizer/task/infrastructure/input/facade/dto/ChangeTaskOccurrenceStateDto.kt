package pm.dvl.organizer.task.infrastructure.input.facade.dto

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskOccurrenceState

data class ChangeTaskOccurrenceStateDto(
    val taskOccurrenceId: TaskOccurrenceId,
    val taskOccurrenceState: TaskOccurrenceState
)
