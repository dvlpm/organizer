package pm.dvl.organizer.task.infrastructure.input.facade.dto

import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId

data class CreateTaskOccurrenceDto(
    val taskOccurrenceId: TaskOccurrenceId,
    val taskData: TaskData,
)
