package pm.dvl.organizer.task.infrastructure.output.eventproducer.event

import org.springframework.context.ApplicationEvent
import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskState

class TaskCreatedEvent(
    val taskId: TaskId,
    val taskData: TaskData,
    val taskState: TaskState,
    val taskSchedule: Schedule?
) : ApplicationEvent(taskId)
