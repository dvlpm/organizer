package pm.dvl.organizer.task.infrastructure.input.facade.dto

import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.tag.domain.model.RankedTag
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskId

data class CreateTaskDto(
    val taskId: TaskId,
    val taskData: TaskData,
    val schedule: Schedule? = null,
    val parents: MutableList<TaskId> = mutableListOf(),
    val tags: MutableList<RankedTag> = mutableListOf(),
)
