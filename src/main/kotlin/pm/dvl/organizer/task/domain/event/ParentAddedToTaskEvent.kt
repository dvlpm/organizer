package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskId

data class ParentAddedToTaskEvent(val taskId: TaskId, val parentTaskId: TaskId)
