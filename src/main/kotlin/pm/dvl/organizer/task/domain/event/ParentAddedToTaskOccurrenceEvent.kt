package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId

data class ParentAddedToTaskOccurrenceEvent(val taskOccurrenceId: TaskOccurrenceId, val parentTaskOccurrenceId: TaskOccurrenceId)
