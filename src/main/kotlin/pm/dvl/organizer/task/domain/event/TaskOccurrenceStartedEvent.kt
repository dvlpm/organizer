package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId

data class TaskOccurrenceStartedEvent(
    val taskOccurrenceId: TaskOccurrenceId,
    val taskData: TaskData
)
