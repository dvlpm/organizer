package pm.dvl.organizer.task.domain.query.projector

import org.axonframework.eventhandling.EventHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.event.TaskOccurrenceProducedByTaskEvent
import pm.dvl.organizer.task.domain.query.projection.TaskOccurrenceProducedByTaskProjection
import pm.dvl.organizer.task.domain.query.repository.TaskOccurrenceProducedByTaskProjectionRepositoryInterface

@Component
class TaskOccurrenceProducedByTaskProjector(
    @Autowired private val repository: TaskOccurrenceProducedByTaskProjectionRepositoryInterface
) {
    @EventHandler
    fun on(event: TaskOccurrenceProducedByTaskEvent) {
        repository.save(
            TaskOccurrenceProducedByTaskProjection(
                event.taskOccurrenceId,
                event.taskId,
            )
        )
    }
}
