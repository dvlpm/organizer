package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskState

data class TaskStateChangedEvent(
    val taskId: TaskId,
    val taskState: TaskState,
)
