package pm.dvl.organizer.task.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId

data class CreateTaskOccurrenceCommand(
    @TargetAggregateIdentifier
    val taskOccurrenceId: TaskOccurrenceId,
    val taskData: TaskData,
    val taskOccurrenceParentIds: MutableList<TaskOccurrenceId> = mutableListOf(),
)
