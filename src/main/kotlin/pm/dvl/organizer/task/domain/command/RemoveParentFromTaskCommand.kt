package pm.dvl.organizer.task.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskId

data class RemoveParentFromTaskCommand(
    @TargetAggregateIdentifier
    val taskId: TaskId,
    val parentTaskId: TaskId
)
