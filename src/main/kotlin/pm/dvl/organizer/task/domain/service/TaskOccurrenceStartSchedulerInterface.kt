package pm.dvl.organizer.task.domain.service

interface TaskOccurrenceStartSchedulerInterface {
    fun schedule(payload: ScheduleTaskOccurrenceStartPayload)
}
