package pm.dvl.organizer.task.domain.query.repository

import pm.dvl.organizer.common.RepositoryWithWaitForInterface
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.query.projection.TaskOccurrenceProjection
import pm.dvl.organizer.user.domain.model.UserId

interface TaskOccurrenceProjectionRepositoryInterface : RepositoryWithWaitForInterface {
    fun save(projection: TaskOccurrenceProjection)
    fun findOneOrNullById(id: TaskOccurrenceId): TaskOccurrenceProjection?
    suspend fun waitForFindOneOrNullById(id: TaskOccurrenceId): TaskOccurrenceProjection?
    fun findAllByReporterId(reporterUserId: UserId): List<TaskOccurrenceProjection>
    fun findAll(): List<TaskOccurrenceProjection>
}
