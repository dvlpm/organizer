package pm.dvl.organizer.task.domain.service

interface TaskOccurrenceManagerInterface {
    fun produce(payload: ProduceTaskOccurrencePayload)
    fun scheduleProduction(payload: ScheduleTaskOccurrencesProductionPayload)
    fun disable(payload: DisableTaskOccurrencesPayload)
    fun enable(payload: EnableTaskOccurrencesPayload)
}
