package pm.dvl.organizer.task.domain.query.repository

import pm.dvl.organizer.task.domain.query.projection.TaskOccurrenceProducedByTaskProjection

interface TaskOccurrenceProducedByTaskProjectionRepositoryInterface {
    fun save(projection: TaskOccurrenceProducedByTaskProjection)
}
