package pm.dvl.organizer.task.domain.model

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.extensions.kotlin.applyEvent
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import org.springframework.beans.factory.annotation.Autowired
import pm.dvl.organizer.task.domain.command.*
import pm.dvl.organizer.task.domain.event.*
import pm.dvl.organizer.task.domain.service.ScheduleTaskOccurrenceStartPayload
import pm.dvl.organizer.task.domain.service.TaskOccurrenceStartSchedulerInterface

@Aggregate(snapshotTriggerDefinition = "aggregateSnapshotTriggerDefinition")
class TaskOccurrence() {
    @AggregateIdentifier
    private lateinit var id: TaskOccurrenceId
    private lateinit var data: TaskData
    private lateinit var parents: MutableList<TaskOccurrenceId>
    private lateinit var state: TaskOccurrenceState

    @CommandHandler
    constructor(command: CreateTaskOccurrenceCommand, @Autowired taskOccurrenceStartScheduler: TaskOccurrenceStartSchedulerInterface) : this() {
        taskOccurrenceStartScheduler.schedule(
            ScheduleTaskOccurrenceStartPayload(
                command.taskOccurrenceId,
                command.taskData.startAt
            )
        )

        applyEvent(
            TaskOccurrenceCreatedEvent(
                command.taskOccurrenceId,
                command.taskData,
                command.taskOccurrenceParentIds,
                TaskOccurrenceState.PENDING,
            )
        )
    }

    @EventSourcingHandler
    fun on(event: TaskOccurrenceCreatedEvent) {
        id = event.taskOccurrenceId
        data = event.taskData
        state = event.taskOccurrenceState
        parents = event.taskOccurrenceParentIds
    }

    @CommandHandler
    fun start(command: StartTaskOccurrenceCommand) {
        applyEvent(TaskOccurrenceStartedEvent(id, data))
    }

    @EventSourcingHandler
    fun on(event: TaskOccurrenceStartedEvent) {
        applyEvent(TaskOccurrenceStateChangedEvent(id, TaskOccurrenceState.IN_PROGRESS))
    }

    @CommandHandler
    fun changeState(command: ChangeTaskOccurrenceStateCommand) {
        if (command.state == state) return

        applyEvent(TaskOccurrenceStateChangedEvent(id, command.state))
    }

    @EventSourcingHandler
    fun on(event: TaskOccurrenceStateChangedEvent) {
        state = event.state
    }

    @CommandHandler
    fun addParent(command: AddParentToTaskOccurrenceCommand) {
        if (id === command.parentTaskOccurrenceId) return
        if (parents.contains(command.parentTaskOccurrenceId)) return

        applyEvent(ParentAddedToTaskOccurrenceEvent(id, command.parentTaskOccurrenceId))
    }

    @EventSourcingHandler
    fun on(event: ParentAddedToTaskOccurrenceEvent) {
        parents.add(event.parentTaskOccurrenceId)
    }

    @CommandHandler
    fun removeParent(command: RemoveParentFromTaskOccurrenceCommand) {
        if (!parents.contains(command.parentTaskOccurrenceId)) return

        applyEvent(ParentRemovedFromTaskOccurrenceEvent(id, command.parentTaskOccurrenceId))
    }

    @EventSourcingHandler
    fun on(event: ParentRemovedFromTaskOccurrenceEvent) {
        parents.remove(event.parentTaskOccurrenceId)
    }
}
