package pm.dvl.organizer.task.domain.model

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.extensions.kotlin.applyEvent
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import org.springframework.beans.factory.annotation.Autowired
import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.task.domain.command.*
import pm.dvl.organizer.task.domain.event.*
import pm.dvl.organizer.task.domain.service.*
import java.util.*

@Aggregate(snapshotTriggerDefinition = "aggregateSnapshotTriggerDefinition")
class Task() {
    @AggregateIdentifier
    private lateinit var id: TaskId
    private lateinit var taskData: TaskData
    private lateinit var state: TaskState
    private val taskOccurrences: MutableList<TaskOccurrenceId> = mutableListOf()
    private val parents: MutableList<TaskId> = mutableListOf()
    private val children: MutableList<TaskId> = mutableListOf()
    private var schedule: Schedule? = null

    @CommandHandler
    constructor(
        command: CreateTaskCommand,
        @Autowired taskOccurrenceManager: TaskOccurrenceManagerInterface,
    ) : this() {
        taskOccurrenceManager.scheduleProduction(
            ScheduleTaskOccurrencesProductionPayload(
                command.taskId,
                command.schedule
            )
        )

        applyEvent(
            TaskCreatedEvent(
                command.taskId,
                command.taskData,
                TaskState.ENABLED,
                command.schedule,
            )
        )
    }

    @EventSourcingHandler
    fun on(event: TaskCreatedEvent) {
        id = event.taskId
        taskData = event.taskData
        state = event.taskState
        schedule = event.taskSchedule
    }

    @CommandHandler
    fun addParent(command: AddParentToTaskCommand) {
        if (id === command.parentTaskId) return
        if (parents.contains(command.parentTaskId)) return

        applyEvent(ParentAddedToTaskEvent(
            id,
            command.parentTaskId
        ))
    }

    @EventSourcingHandler
    fun on(event: ParentAddedToTaskEvent) {
        parents.add(event.parentTaskId)
    }

    @CommandHandler
    fun removeParent(command: RemoveParentFromTaskCommand) {
        if (!parents.contains(command.parentTaskId)) return

        applyEvent(ParentRemovedFromTaskEvent(id, command.parentTaskId))
    }

    @EventSourcingHandler
    fun on(event: ParentRemovedFromTaskEvent) {
        parents.remove(event.parentTaskId)
    }

    @CommandHandler
    fun addChild(command: AddChildToTaskCommand) {
        if (id === command.childTaskId) return
        if (children.contains(command.childTaskId)) return

        applyEvent(ChildAddedToTaskEvent(
            id,
            command.childTaskId
        ))
    }

    @EventSourcingHandler
    fun on(event: ChildAddedToTaskEvent) {
        children.add(event.childTaskId)
    }

    @CommandHandler
    fun removeChild(command: RemoveChildFromTaskCommand) {
        if (!children.contains(command.childTaskId)) return

        applyEvent(ChildRemovedFromTaskEvent(id, command.childTaskId))
    }

    @EventSourcingHandler
    fun on(event: ChildRemovedFromTaskEvent) {
        children.remove(event.childTaskId)
    }

    @CommandHandler
    fun produceTaskOccurrence(
        command: ProduceTaskOccurrenceCommand,
        @Autowired taskFactory: TaskOccurrenceFactoryInterface,
        @Autowired taskOccurrenceManager: TaskOccurrenceManagerInterface
    ) {
        if (taskOccurrences.contains(command.taskOccurrenceId)) return

        taskFactory.create(
            CreateTaskOccurrencePayload(
                command.taskOccurrenceId,
                taskData.withStartAt(command.taskStartAt),
                command.taskOccurrenceParentIds
            )
        )

        children.forEach { childTaskId ->
            taskOccurrenceManager.produce(
                ProduceTaskOccurrencePayload(
                    childTaskId,
                    TaskOccurrenceId(UUID.randomUUID()),
                    command.taskStartAt,
                    mutableListOf(command.taskOccurrenceId)
                )
            )
        }

        applyEvent(TaskOccurrenceProducedByTaskEvent(id, command.taskOccurrenceId))
    }

    @EventSourcingHandler
    fun on(event: TaskOccurrenceProducedByTaskEvent) {
        taskOccurrences.add(event.taskOccurrenceId)
    }

    @CommandHandler
    fun changeState(
        command: ChangeTaskStateCommand,
        @Autowired taskOccurrenceManager: TaskOccurrenceManagerInterface,
    ) {
        if (state == command.state) return

        when (command.state) {
            TaskState.ENABLED -> taskOccurrenceManager.enable(EnableTaskOccurrencesPayload(id))
            TaskState.DISABLED -> taskOccurrenceManager.disable(DisableTaskOccurrencesPayload(id))
        }

        applyEvent(
            TaskStateChangedEvent(
                id,
                command.state
            )
        )
    }

    @EventSourcingHandler
    fun on(event: TaskStateChangedEvent) {
        state = event.taskState
    }
}
