package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskId

data class ParentRemovedFromTaskEvent(val taskId: TaskId, val parentTaskId: TaskId)
