package pm.dvl.organizer.task.domain.query.projection

import com.fasterxml.jackson.annotation.JsonIgnore
import pm.dvl.organizer.common.annotations.Persistable
import pm.dvl.organizer.tag.domain.model.RankedTag
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskOccurrenceState
import pm.dvl.organizer.user.domain.model.UserId

@Persistable
class TaskOccurrenceProjection(
    var id: TaskOccurrenceId,
    var summary: String,
    var reporterUserId: UserId,
    var assigneeUserId: UserId? = null,
    var state: TaskOccurrenceState,
    @JsonIgnore
    var _parents: MutableSet<TaskOccurrenceId>? = mutableSetOf(),
    @JsonIgnore
    var _children: MutableSet<TaskOccurrenceId>? = mutableSetOf(),
    var _tags: Set<RankedTag>? = setOf(),
) {
    val parents: MutableSet<TaskOccurrenceId>
        get() = _parents ?: mutableSetOf()

    val children: MutableSet<TaskOccurrenceId>
        get() = _children ?: mutableSetOf()

    val tags: Set<RankedTag>
        get() = _tags ?: setOf()

    fun updateState(state: TaskOccurrenceState): TaskOccurrenceProjection {
        return apply {
            this.state = state
        }
    }

    fun addParent(taskOccurrenceProjection: TaskOccurrenceProjection): TaskOccurrenceProjection {
        return apply {
            _parents = parents.apply { add(taskOccurrenceProjection.id) }
            taskOccurrenceProjection._children = taskOccurrenceProjection.children.apply { add(this@TaskOccurrenceProjection.id) }
        }
    }

    fun removeParent(taskOccurrenceProjection: TaskOccurrenceProjection): TaskOccurrenceProjection {
        return apply {
            _parents = parents.apply { remove(taskOccurrenceProjection.id) }
            taskOccurrenceProjection._children = taskOccurrenceProjection.children.apply { remove(this@TaskOccurrenceProjection.id) }
        }
    }
}
