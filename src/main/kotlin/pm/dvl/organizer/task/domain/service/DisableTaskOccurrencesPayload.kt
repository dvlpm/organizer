package pm.dvl.organizer.task.domain.service

import pm.dvl.organizer.task.domain.model.TaskId

data class DisableTaskOccurrencesPayload(val taskId: TaskId)
