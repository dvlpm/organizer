package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskId

data class ChildRemovedFromTaskEvent(val taskId: TaskId, val childTaskId: TaskId)
