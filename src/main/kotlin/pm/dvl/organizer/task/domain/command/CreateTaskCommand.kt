package pm.dvl.organizer.task.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskId

data class CreateTaskCommand(
    @TargetAggregateIdentifier
    val taskId: TaskId,
    val taskData: TaskData,
    val schedule: Schedule? = null,
)
