package pm.dvl.organizer.task.domain.service

interface TaskOccurrenceFactoryInterface {
    fun create(payload: CreateTaskOccurrencePayload)
}