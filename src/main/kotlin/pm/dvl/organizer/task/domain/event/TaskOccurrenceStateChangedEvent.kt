package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskOccurrenceState

data class TaskOccurrenceStateChangedEvent(
    val taskOccurrenceId: TaskOccurrenceId,
    val state: TaskOccurrenceState,
)
