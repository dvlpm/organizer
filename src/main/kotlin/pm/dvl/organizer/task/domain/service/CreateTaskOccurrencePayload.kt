package pm.dvl.organizer.task.domain.service

import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId

data class CreateTaskOccurrencePayload(
    val taskOccurrenceId: TaskOccurrenceId,
    val taskData: TaskData,
    val taskOccurrenceParentIds: MutableList<TaskOccurrenceId> = mutableListOf(),
)
