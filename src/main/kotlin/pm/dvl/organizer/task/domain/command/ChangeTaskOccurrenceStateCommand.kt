package pm.dvl.organizer.task.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskOccurrenceState

data class ChangeTaskOccurrenceStateCommand(
    @TargetAggregateIdentifier
    val taskOccurrenceId: TaskOccurrenceId,
    val state: TaskOccurrenceState
)
