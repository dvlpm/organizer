package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId

data class ParentRemovedFromTaskOccurrenceEvent(val taskOccurrenceId: TaskOccurrenceId, val parentTaskOccurrenceId: TaskOccurrenceId)
