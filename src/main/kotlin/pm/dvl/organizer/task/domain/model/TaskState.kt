package pm.dvl.organizer.task.domain.model

enum class TaskState {
    ENABLED,
    DISABLED,
}

fun String.toTaskState(): TaskState
{
    return TaskState.valueOf(this.uppercase())
}
