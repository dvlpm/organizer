package pm.dvl.organizer.task.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskId
import java.time.ZonedDateTime

data class ProduceTaskOccurrenceCommand(
    @TargetAggregateIdentifier
    val taskId: TaskId,
    val taskOccurrenceId: TaskOccurrenceId,
    val taskStartAt: ZonedDateTime,
    val taskOccurrenceParentIds: MutableList<TaskOccurrenceId> = mutableListOf(),
)
