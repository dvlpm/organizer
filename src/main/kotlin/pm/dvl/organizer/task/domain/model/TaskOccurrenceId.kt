package pm.dvl.organizer.task.domain.model

import pm.dvl.organizer.common.annotations.Persistable
import java.util.*
import java.io.Serializable

@Persistable
data class TaskOccurrenceId(var value: UUID) : Serializable
