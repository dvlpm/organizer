package pm.dvl.organizer.task.domain.query.projection

import com.fasterxml.jackson.annotation.JsonIgnore
import pm.dvl.organizer.common.annotations.Persistable
import pm.dvl.organizer.tag.domain.model.RankedTag
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskState
import pm.dvl.organizer.user.domain.model.UserId

@Persistable
data class TaskProjection(
    var id: TaskId,
    var state: TaskState,
    var summary: String,
    var reporterUserId: UserId,
    var assigneeUserId: UserId? = null,
    @JsonIgnore
    var _parents: MutableSet<TaskId>? = mutableSetOf(),
    @JsonIgnore
    var _children: MutableSet<TaskId>? = mutableSetOf(),
    var _tags: Set<RankedTag>? = setOf(),
) {
    val parents: MutableSet<TaskId>
        get() = _parents ?: mutableSetOf()

    val children: MutableSet<TaskId>
        get() = _children ?: mutableSetOf()

    val tags: Set<RankedTag>
        get() = _tags ?: setOf()

    fun updateState(state: TaskState): TaskProjection {
        return apply {
            this.state = state
        }
    }

    fun addParent(taskProjection: TaskProjection): TaskProjection {
        return apply {
            _parents = parents.apply { add(taskProjection.id) }
            taskProjection._children = taskProjection.children.apply { add(this@TaskProjection.id) }
        }
    }

    fun removeParent(taskProjection: TaskProjection): TaskProjection {
        return apply {
            _parents = parents.apply { remove(taskProjection.id) }
            taskProjection._children = taskProjection.children.apply { remove(this@TaskProjection.id) }
        }
    }
}
