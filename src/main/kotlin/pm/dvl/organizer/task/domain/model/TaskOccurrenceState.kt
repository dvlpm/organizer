package pm.dvl.organizer.task.domain.model

enum class TaskOccurrenceState {
    PENDING,
    IN_PROGRESS,
    DONE,
    CANCELED
}
