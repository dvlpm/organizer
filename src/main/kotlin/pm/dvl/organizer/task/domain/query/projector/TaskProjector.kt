package pm.dvl.organizer.task.domain.query.projector

import org.axonframework.eventhandling.EventHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.event.ParentAddedToTaskEvent
import pm.dvl.organizer.task.domain.event.ParentRemovedFromTaskEvent
import pm.dvl.organizer.task.domain.event.TaskCreatedEvent
import pm.dvl.organizer.task.domain.event.TaskStateChangedEvent
import pm.dvl.organizer.task.domain.query.projection.TaskProjection
import pm.dvl.organizer.task.domain.query.repository.TaskProjectionRepositoryInterface

@Component
class TaskProjector(
    @Autowired private val repository: TaskProjectionRepositoryInterface
) {
    @EventHandler
    fun on(event: TaskCreatedEvent) {
        repository.save(
            TaskProjection(
                event.taskId,
                event.taskState,
                event.taskData.summary,
                event.taskData.reporterUserId,
                event.taskData.assigneeUserId,
                _tags = event.taskData.tags
            )
        )
    }

    @EventHandler
    fun on(event: TaskStateChangedEvent) {
        val projection = repository.findOneOrNullById(event.taskId) ?: return

        projection.updateState(event.taskState)

        repository.save(projection)
    }

    @EventHandler
    fun on(event: ParentAddedToTaskEvent) {
        val projection = repository.findOneOrNullById(event.taskId) ?: return
        val parentProjection = repository.findOneOrNullById(event.parentTaskId) ?: return

        projection.addParent(parentProjection)

        repository.save(projection)
    }

    @EventHandler
    fun on(event: ParentRemovedFromTaskEvent) {
        val projection = repository.findOneOrNullById(event.taskId) ?: return
        val parentProjection = repository.findOneOrNullById(event.parentTaskId) ?: return

        projection.removeParent(parentProjection)

        repository.save(projection)
    }
}
