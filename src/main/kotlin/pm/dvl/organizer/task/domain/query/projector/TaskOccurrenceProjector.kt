package pm.dvl.organizer.task.domain.query.projector

import org.axonframework.eventhandling.EventHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.event.ParentAddedToTaskOccurrenceEvent
import pm.dvl.organizer.task.domain.event.ParentRemovedFromTaskOccurrenceEvent
import pm.dvl.organizer.task.domain.event.TaskOccurrenceCreatedEvent
import pm.dvl.organizer.task.domain.event.TaskOccurrenceStateChangedEvent
import pm.dvl.organizer.task.domain.query.projection.TaskOccurrenceProjection
import pm.dvl.organizer.task.domain.query.repository.TaskOccurrenceProjectionRepositoryInterface

@Component
class TaskOccurrenceProjector(
    @Autowired private val repository: TaskOccurrenceProjectionRepositoryInterface
) {
    @EventHandler
    fun on(event: TaskOccurrenceCreatedEvent) {
        val projection = TaskOccurrenceProjection(
            event.taskOccurrenceId,
            event.taskData.summary,
            event.taskData.reporterUserId,
            event.taskData.assigneeUserId,
            event.taskOccurrenceState,
            _tags = event.taskData.tags
        )

        event.taskOccurrenceParentIds.forEach { parentTaskId ->
            val parentProjection = repository.findOneOrNullById(parentTaskId) ?: return

            projection.addParent(parentProjection)
        }

        repository.save(projection)
    }

    @EventHandler
    fun on(event: TaskOccurrenceStateChangedEvent) {
        val projection = repository.findOneOrNullById(event.taskOccurrenceId) ?: return

        projection.updateState(event.state)

        repository.save(projection)
    }

    @EventHandler
    fun on(event: ParentAddedToTaskOccurrenceEvent) {
        val projection = repository.findOneOrNullById(event.taskOccurrenceId) ?: return
        val parentProjection = repository.findOneOrNullById(event.parentTaskOccurrenceId) ?: return

        projection.addParent(parentProjection)

        repository.save(projection)
    }

    @EventHandler
    fun on(event: ParentRemovedFromTaskOccurrenceEvent) {
        val projection = repository.findOneOrNullById(event.taskOccurrenceId) ?: return
        val parentProjection = repository.findOneOrNullById(event.parentTaskOccurrenceId) ?: return

        projection.removeParent(parentProjection)

        repository.save(projection)
    }
}
