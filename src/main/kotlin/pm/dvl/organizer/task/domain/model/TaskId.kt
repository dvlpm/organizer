package pm.dvl.organizer.task.domain.model

import pm.dvl.organizer.common.annotations.Persistable
import java.io.Serializable
import java.util.*

@Persistable
data class TaskId(var value: UUID) : Serializable
