package pm.dvl.organizer.task.domain.service

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import java.time.ZonedDateTime

data class ScheduleTaskOccurrenceStartPayload(val taskOccurrenceId: TaskOccurrenceId, val startAt: ZonedDateTime? = null)
