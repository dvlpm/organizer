package pm.dvl.organizer.task.domain.model

import pm.dvl.organizer.tag.domain.model.RankedTag
import pm.dvl.organizer.user.domain.model.UserId
import java.time.Duration
import java.time.ZonedDateTime

data class TaskData(
    var summary: String,
    var reporterUserId: UserId,
    var assigneeUserId: UserId? = null,
    var startAt: ZonedDateTime? = null,
    var dueDateAt: ZonedDateTime? = null,
    var tags: Set<RankedTag>? = setOf(),
    var duration: Duration? = null,
) {
    fun withStartAt(startAt: ZonedDateTime): TaskData {
        return this.copy().apply {
            this.startAt = startAt
        }
    }

    fun withTags(tags: MutableSet<RankedTag>): TaskData {
        return this.copy().apply {
            this.tags = tags
        }
    }
}
