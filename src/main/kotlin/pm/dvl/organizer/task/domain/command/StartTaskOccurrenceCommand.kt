package pm.dvl.organizer.task.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId

data class StartTaskOccurrenceCommand(
    @TargetAggregateIdentifier
    val taskOccurrenceId: TaskOccurrenceId,
)
