package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskOccurrenceState

data class TaskOccurrenceCreatedEvent(
    val taskOccurrenceId: TaskOccurrenceId,
    val taskData: TaskData,
    val taskOccurrenceParentIds: MutableList<TaskOccurrenceId>,
    val taskOccurrenceState: TaskOccurrenceState,
)
