package pm.dvl.organizer.task.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskState

data class ChangeTaskStateCommand(
    @TargetAggregateIdentifier
    val taskId: TaskId,
    val state: TaskState
)
