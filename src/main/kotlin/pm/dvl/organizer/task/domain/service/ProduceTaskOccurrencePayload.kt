package pm.dvl.organizer.task.domain.service

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskId
import java.time.ZonedDateTime

data class ProduceTaskOccurrencePayload(
    val taskId: TaskId,
    val taskOccurrenceId: TaskOccurrenceId,
    val taskStartAt: ZonedDateTime,
    val taskOccurrenceParentIds: MutableList<TaskOccurrenceId> = mutableListOf(),
)
