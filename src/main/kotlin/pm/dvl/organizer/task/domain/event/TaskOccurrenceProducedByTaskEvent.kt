package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskId

data class TaskOccurrenceProducedByTaskEvent(
    val taskId: TaskId,
    val taskOccurrenceId: TaskOccurrenceId
)
