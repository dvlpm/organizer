package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskState

data class TaskCreatedEvent(
    val taskId: TaskId,
    val taskData: TaskData,
    val taskState: TaskState,
    val taskSchedule: Schedule?
)
