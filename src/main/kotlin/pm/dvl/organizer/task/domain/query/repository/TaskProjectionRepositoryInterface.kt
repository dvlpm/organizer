package pm.dvl.organizer.task.domain.query.repository

import pm.dvl.organizer.common.RepositoryWithWaitForInterface
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.query.projection.TaskProjection

interface TaskProjectionRepositoryInterface : RepositoryWithWaitForInterface {
    fun save(projection: TaskProjection)
    fun findOneOrNullById(id: TaskId): TaskProjection?
    suspend fun waitForFindOneOrNullById(id: TaskId): TaskProjection?
    fun findAll(): List<TaskProjection>
}
