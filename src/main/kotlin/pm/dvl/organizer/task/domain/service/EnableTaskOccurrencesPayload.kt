package pm.dvl.organizer.task.domain.service

import pm.dvl.organizer.task.domain.model.TaskId

data class EnableTaskOccurrencesPayload(val taskId: TaskId)
