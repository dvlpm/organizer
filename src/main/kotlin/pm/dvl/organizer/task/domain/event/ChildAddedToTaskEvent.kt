package pm.dvl.organizer.task.domain.event

import pm.dvl.organizer.task.domain.model.TaskId

data class ChildAddedToTaskEvent(val taskId: TaskId, val childTaskId: TaskId)
