package pm.dvl.organizer.task.domain.service

import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.task.domain.model.TaskId

data class ScheduleTaskOccurrencesProductionPayload(
    val taskId: TaskId,
    val schedule: Schedule? = null
)
