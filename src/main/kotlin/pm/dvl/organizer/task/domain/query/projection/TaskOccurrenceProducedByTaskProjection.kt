package pm.dvl.organizer.task.domain.query.projection

import pm.dvl.organizer.common.annotations.Persistable
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskId

@Persistable
data class TaskOccurrenceProducedByTaskProjection(
    var taskOccurrenceId: TaskOccurrenceId,
    var taskId: TaskId
)
