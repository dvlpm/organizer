package pm.dvl.organizer.task.domain.model

import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.modelling.saga.SagaEventHandler
import org.axonframework.modelling.saga.StartSaga
import org.axonframework.spring.stereotype.Saga
import org.springframework.beans.factory.annotation.Autowired
import pm.dvl.organizer.task.domain.command.AddChildToTaskCommand
import pm.dvl.organizer.task.domain.command.AddParentToTaskCommand
import pm.dvl.organizer.task.domain.command.RemoveChildFromTaskCommand
import pm.dvl.organizer.task.domain.command.RemoveParentFromTaskCommand
import pm.dvl.organizer.task.domain.event.ChildAddedToTaskEvent
import pm.dvl.organizer.task.domain.event.ChildRemovedFromTaskEvent
import pm.dvl.organizer.task.domain.event.ParentAddedToTaskEvent
import pm.dvl.organizer.task.domain.event.ParentRemovedFromTaskEvent

@Saga
class TaskHierarchySaga {
    @Autowired
    @Transient
    private lateinit var commandGateway: CommandGateway

    private lateinit var taskId: TaskId

    @StartSaga
    @SagaEventHandler(associationProperty = "taskId")
    fun on(event: ParentAddedToTaskEvent) {
        taskId = event.taskId

        commandGateway.send<Unit>(AddChildToTaskCommand(
            event.parentTaskId,
            event.taskId,
        ))
    }

    @StartSaga
    @SagaEventHandler(associationProperty = "taskId")
    fun on(event: ParentRemovedFromTaskEvent) {
        taskId = event.taskId

        commandGateway.send<Unit>(RemoveChildFromTaskCommand(
            event.parentTaskId,
            event.taskId,
        ))
    }

    @StartSaga
    @SagaEventHandler(associationProperty = "taskId")
    fun on(event: ChildAddedToTaskEvent) {
        taskId = event.taskId

        commandGateway.send<Unit>(AddParentToTaskCommand(
            event.childTaskId,
            event.taskId,
        ))
    }

    @StartSaga
    @SagaEventHandler(associationProperty = "taskId")
    fun on(event: ChildRemovedFromTaskEvent) {
        taskId = event.taskId

        commandGateway.send<Unit>(RemoveParentFromTaskCommand(
            event.childTaskId,
            event.taskId,
        ))
    }
}
