package pm.dvl.organizer.task.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskId

data class RemoveChildFromTaskCommand(
    @TargetAggregateIdentifier
    val taskId: TaskId,
    val childTaskId: TaskId
)
