package pm.dvl.organizer.calendar.infrastructure.input.dto

import net.fortuna.ical4j.model.Calendar
import pm.dvl.organizer.calendar.domain.model.CalendarId

data class StartCalendarDto(val calendarId: CalendarId, val calendar: Calendar)
