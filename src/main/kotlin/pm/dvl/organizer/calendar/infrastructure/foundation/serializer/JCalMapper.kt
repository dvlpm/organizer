package pm.dvl.organizer.calendar.infrastructure.foundation.serializer

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import net.fortuna.ical4j.data.DefaultParameterFactorySupplier
import net.fortuna.ical4j.data.DefaultPropertyFactorySupplier
import net.fortuna.ical4j.model.*
import net.fortuna.ical4j.model.component.*
import net.fortuna.ical4j.model.parameter.Value
import org.mnode.ical4j.serializer.JCalDecoder
import org.mnode.ical4j.serializer.JsonMapper
import java.io.IOException
import java.net.URISyntaxException
import java.text.ParseException

class JCalMapper(vc: Class<*>?) : StdDeserializer<Calendar?>(vc), JsonMapper {
    private val parameterFactories: List<ParameterFactory<*>> = DefaultParameterFactorySupplier().get()
    private val propertyFactories: List<PropertyFactory<*>> = DefaultPropertyFactorySupplier().get()
    private val componentFactories: List<ComponentFactory<*>> = listOf<ComponentFactory<*>>(
        Available.Factory(),
        Daylight.Factory(),
        Standard.Factory(),
        VAlarm.Factory(),
        VAvailability.Factory(),
        VEvent.Factory(),
        VFreeBusy.Factory(),
        VJournal.Factory(),
        VTimeZone.Factory(),
        VToDo.Factory(),
        VVenue.Factory()
    )

    @Throws(IOException::class)
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Calendar {
        val calendar = Calendar()
        assertTextValue(p, "vcalendar")
        // calendar properties..
        assertNextToken(p, JsonToken.START_ARRAY)
        while (JsonToken.END_ARRAY != p.nextToken()) {
            try {
                calendar.properties.add(parseProperty(p))
            } catch (e: URISyntaxException) {
                throw IllegalArgumentException(e)
            } catch (e: ParseException) {
                throw IllegalArgumentException(e)
            }
        }
        // calendar components..
        assertNextToken(p, JsonToken.START_ARRAY)
        while (JsonToken.END_ARRAY != p.nextToken()) {
            try {
                calendar.components.add(parseComponent(p) as CalendarComponent)
            } catch (e: URISyntaxException) {
                throw IllegalArgumentException(e)
            } catch (e: ParseException) {
                throw IllegalArgumentException(e)
            }
        }
        return calendar
    }

    @Throws(IOException::class, URISyntaxException::class, ParseException::class)
    private fun parseComponent(p: JsonParser): Component {
        assertCurrentToken(p, JsonToken.START_ARRAY)
        val componentBuilder: ComponentBuilder<*> = ComponentBuilder<Component>(componentFactories)
        componentBuilder.name(p.nextTextValue())
        // component properties..
        assertNextToken(p, JsonToken.START_ARRAY)
        while (JsonToken.END_ARRAY != p.nextToken()) {
            componentBuilder.property(parseProperty(p))
        }
        // sub-components..
        assertNextToken(p, JsonToken.START_ARRAY)
        while (JsonToken.END_ARRAY != p.nextToken()) {
            componentBuilder.subComponent(parseComponent(p))
        }

        assertNextToken(p, JsonToken.END_ARRAY)
        return componentBuilder.build()
    }

    @Throws(IOException::class, URISyntaxException::class, ParseException::class)
    private fun parseProperty(p: JsonParser): Property {
        assertCurrentToken(p, JsonToken.START_ARRAY)
        val propertyBuilder = PropertyBuilder(propertyFactories)
        propertyBuilder.name(p.nextTextValue())
        // property params..
        assertNextToken(p, JsonToken.START_OBJECT)
        while (JsonToken.END_OBJECT != p.nextToken()) {
            try {
                propertyBuilder.parameter(parseParameter(p))
            } catch (e: URISyntaxException) {
                throw IllegalArgumentException(e)
            } catch (e: IOException) {
                throw IllegalArgumentException(e)
            }
        }

        // propertyType
        val propertyType = p.nextTextValue()
        when (propertyType) {
            "date" -> {
                propertyBuilder.parameter(Value.DATE)
                propertyBuilder.value(JCalDecoder.DATE.decode(p.nextTextValue()))
            }
            "date-time" -> {
                propertyBuilder.parameter(Value.DATE_TIME)
                propertyBuilder.value(JCalDecoder.DATE_TIME.decode(p.nextTextValue()))
            }
            "time" -> {
                propertyBuilder.parameter(Value.TIME)
                propertyBuilder.value(JCalDecoder.TIME.decode(p.nextTextValue()))
            }
            "utc-offset" -> {
                propertyBuilder.parameter(Value.UTC_OFFSET)
                propertyBuilder.value(JCalDecoder.UTCOFFSET.decode(p.nextTextValue()))
            }
            "binary" -> {
                propertyBuilder.parameter(Value.BINARY)
                propertyBuilder.parameter(Value.DURATION)
                propertyBuilder.parameter(Value.PERIOD)
                propertyBuilder.parameter(Value.URI)
                propertyBuilder.value(p.nextTextValue())
            }
            "duration" -> {
                propertyBuilder.parameter(Value.DURATION)
                propertyBuilder.parameter(Value.PERIOD)
                propertyBuilder.parameter(Value.URI)
                propertyBuilder.value(p.nextTextValue())
            }
            "period" -> {
                propertyBuilder.parameter(Value.PERIOD)
                propertyBuilder.parameter(Value.URI)
                propertyBuilder.value(p.nextTextValue())
            }
            "uri" -> {
                propertyBuilder.parameter(Value.URI)
                propertyBuilder.value(p.nextTextValue())
            }
            else -> propertyBuilder.value(p.nextTextValue())
        }
        assertNextToken(p, JsonToken.END_ARRAY)
        return propertyBuilder.build()
    }

    @Throws(IOException::class, URISyntaxException::class)
    private fun parseParameter(p: JsonParser): Parameter {
        return ParameterBuilder(parameterFactories).name(p.currentName()).value(p.nextTextValue()).build()
    }
}
