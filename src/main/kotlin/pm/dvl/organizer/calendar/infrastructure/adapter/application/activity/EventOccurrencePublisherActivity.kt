package pm.dvl.organizer.calendar.infrastructure.adapter.application.activity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import pm.dvl.organizer.calendar.application.activity.EventOccurrencePublisherActivityInterface
import pm.dvl.organizer.calendar.domain.model.EventOccurrence
import pm.dvl.organizer.calendar.infrastructure.output.event.EventOccurrenceOccurredEvent

@Service
class EventOccurrencePublisherActivity(
    @Autowired private val applicationEventPublisher: ApplicationEventPublisher
) : EventOccurrencePublisherActivityInterface {
    override fun publish(occurrence: EventOccurrence) {
        applicationEventPublisher.publishEvent(EventOccurrenceOccurredEvent(occurrence))
    }
}
