package pm.dvl.organizer.calendar.infrastructure.adapter.application.activity

import ai.applica.spring.boot.starter.temporal.WorkflowFactory
import io.temporal.client.WorkflowClient
import io.temporal.client.WorkflowExecutionAlreadyStarted
import io.temporal.client.WorkflowNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pm.dvl.organizer.calendar.application.activity.EventOccurrenceSchedulerActivityInterface
import pm.dvl.organizer.calendar.application.workflow.EventWorkflow
import pm.dvl.organizer.calendar.application.workflow.EventWorkflowInterface
import pm.dvl.organizer.calendar.domain.model.EventOccurrence

@Service
class EventOccurrenceSchedulerActivity(
    @Autowired private val workflowFactory: WorkflowFactory,
    @Autowired private val workflowClient: WorkflowClient
) : EventOccurrenceSchedulerActivityInterface {
    override fun schedule(eventOccurrence: EventOccurrence) {
        try {
            WorkflowClient.start {
                eventOccurrence.toWorkflow().schedule(eventOccurrence)
            }
        } catch (_: WorkflowExecutionAlreadyStarted) {
        }
    }

    fun EventOccurrence.toWorkflow(): EventWorkflowInterface {
        return workflowFactory.makeStub(
            EventWorkflowInterface::class.java,
            workflowFactory.defaultOptionsBuilder(EventWorkflow::class.java)
                .setWorkflowId(toWorkflowId())
        )
    }

    override fun disable(eventOccurrence: EventOccurrence) {
        try {
            eventOccurrence.toStartedWorkflow().disable()
        } catch (_: WorkflowNotFoundException) {
        }
    }

    override fun enable(eventOccurrence: EventOccurrence) {
        try {
            eventOccurrence.toStartedWorkflow().enable()
        } catch (_: WorkflowNotFoundException) {
        }
    }

    fun EventOccurrence.toStartedWorkflow(): EventWorkflowInterface {
        return workflowClient.newWorkflowStub(
            EventWorkflowInterface::class.java,
            toWorkflowId()
        )
    }

    fun EventOccurrence.toWorkflowId(): String {
        return "${calendarId}_${eventId}_${startAt}"
    }
}
