package pm.dvl.organizer.calendar.infrastructure.input.facade

import ai.applica.spring.boot.starter.temporal.WorkflowFactory
import io.temporal.client.WorkflowClient
import io.temporal.client.WorkflowExecutionAlreadyStarted
import io.temporal.client.WorkflowNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.calendar.application.workflow.CalendarWorkflow
import pm.dvl.organizer.calendar.application.workflow.CalendarWorkflowInterface
import pm.dvl.organizer.calendar.application.workflow.properties.CalendarWorkflowProperties
import pm.dvl.organizer.calendar.domain.model.CalendarId
import pm.dvl.organizer.calendar.infrastructure.input.dto.DisableCalendarDto
import pm.dvl.organizer.calendar.infrastructure.input.dto.EnableCalendarDto
import pm.dvl.organizer.calendar.infrastructure.input.dto.StartCalendarDto

@Component
class CalendarFacade(
    @Autowired private val workflowFactory: WorkflowFactory,
    @Autowired private val workflowClient: WorkflowClient,
    @Autowired private val calendarWorkflowProperties: CalendarWorkflowProperties
) {
    fun start(payload: StartCalendarDto) {
        try {
            WorkflowClient.start {
                payload.calendarId.toWorkflow().start(
                    pm.dvl.organizer.calendar.domain.model.Calendar(
                        payload.calendarId, payload.calendar
                    ), calendarWorkflowProperties
                )
            }
        } catch (_: WorkflowExecutionAlreadyStarted) {
        }
    }

    fun CalendarId.toWorkflow(): CalendarWorkflowInterface {
        return workflowFactory.makeStub(
            CalendarWorkflowInterface::class.java,
            workflowFactory.defaultOptionsBuilder(CalendarWorkflow::class.java)
                .setWorkflowId(toString())
        )
    }

    fun disable(payload: DisableCalendarDto) {
        try {
            payload.calendarId.toStartedWorkflow().disable()
        } catch (_: WorkflowNotFoundException) {
        }
    }

    fun enable(payload: EnableCalendarDto) {
        try {
            payload.calendarId.toStartedWorkflow().enable()
        } catch (_: WorkflowNotFoundException) {
        }
    }

    fun CalendarId.toStartedWorkflow(): CalendarWorkflowInterface {
        return workflowClient.newWorkflowStub(
            CalendarWorkflowInterface::class.java,
            toString()
        )
    }
}
