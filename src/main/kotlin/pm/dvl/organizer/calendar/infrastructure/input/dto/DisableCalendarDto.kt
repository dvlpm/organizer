package pm.dvl.organizer.calendar.infrastructure.input.dto

import pm.dvl.organizer.calendar.domain.model.CalendarId

data class DisableCalendarDto(val calendarId: CalendarId)
