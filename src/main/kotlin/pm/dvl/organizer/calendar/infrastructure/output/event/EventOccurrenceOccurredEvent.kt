package pm.dvl.organizer.calendar.infrastructure.output.event

import org.springframework.context.ApplicationEvent
import pm.dvl.organizer.calendar.domain.model.EventOccurrence

class EventOccurrenceOccurredEvent(val occurrence: EventOccurrence) : ApplicationEvent(occurrence)
