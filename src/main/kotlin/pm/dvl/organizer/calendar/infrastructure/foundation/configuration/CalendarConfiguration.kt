package pm.dvl.organizer.calendar.infrastructure.foundation.configuration

import ai.applica.spring.boot.starter.temporal.config.DefaultTemporalOptionsConfiguration
import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import io.temporal.client.WorkflowClientOptions
import io.temporal.common.converter.*
import net.fortuna.ical4j.model.Calendar
import org.mnode.ical4j.serializer.JCalSerializer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import pm.dvl.organizer.calendar.infrastructure.foundation.serializer.JCalMapper

@Configuration
class CalendarConfiguration {
    @Component
    class CustomTemporalOptionsConfiguration(@Autowired val objectMapper: ObjectMapper) :
        DefaultTemporalOptionsConfiguration() {
        override fun modifyClientOptions(newBuilder: WorkflowClientOptions.Builder?): WorkflowClientOptions.Builder {
            val defaultBuilder = super.modifyClientOptions(newBuilder)

            objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

            return defaultBuilder.setDataConverter(
                DefaultDataConverter(
                    NullPayloadConverter(),
                    ByteArrayPayloadConverter(),
                    ProtobufJsonPayloadConverter(),
                    JacksonJsonPayloadConverter(objectMapper)
                )
            )
        }
    }

    @Bean
    fun jcalModule(): Module {
        return SimpleModule("org.mnode.ical4j.serializer")
            .addSerializer(JCalSerializer(Calendar::class.java))
            .addDeserializer(Calendar::class.java, JCalMapper(Calendar::class.java))
    }
}
