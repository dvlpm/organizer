package pm.dvl.organizer.calendar.application.activity

import io.temporal.activity.ActivityInterface
import io.temporal.activity.ActivityMethod
import pm.dvl.organizer.calendar.domain.model.EventOccurrence
import pm.dvl.organizer.calendar.domain.service.EventOccurrencePublisherInterface

@ActivityInterface
interface EventOccurrencePublisherActivityInterface : EventOccurrencePublisherInterface {
    @ActivityMethod
    override fun publish(occurrence: EventOccurrence)
}
