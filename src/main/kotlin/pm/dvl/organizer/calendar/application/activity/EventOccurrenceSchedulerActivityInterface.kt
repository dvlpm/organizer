package pm.dvl.organizer.calendar.application.activity

import io.temporal.activity.ActivityInterface
import io.temporal.activity.ActivityMethod
import pm.dvl.organizer.calendar.domain.model.EventOccurrence
import pm.dvl.organizer.calendar.domain.service.EventOccurrenceSchedulerInterface

@ActivityInterface
interface EventOccurrenceSchedulerActivityInterface: EventOccurrenceSchedulerInterface {
    @ActivityMethod
    override fun schedule(eventOccurrence: EventOccurrence)
}
