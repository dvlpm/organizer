package pm.dvl.organizer.calendar.application.workflow

import ai.applica.spring.boot.starter.temporal.annotations.ActivityStub
import ai.applica.spring.boot.starter.temporal.annotations.TemporalWorkflow
import net.fortuna.ical4j.model.Period
import org.springframework.stereotype.Component
import pm.dvl.organizer.calendar.application.activity.EventOccurrenceSchedulerActivityInterface
import pm.dvl.organizer.calendar.application.workflow.helper.WorkflowHelper
import pm.dvl.organizer.calendar.application.workflow.properties.CalendarWorkflowProperties
import pm.dvl.organizer.calendar.domain.model.Calendar
import pm.dvl.organizer.common.extensions.toPeriodTo
import java.time.ZonedDateTime

@Component
@TemporalWorkflow("Calendar")
class CalendarWorkflow : CalendarWorkflowInterface {
    @ActivityStub
    private lateinit var eventOccurrenceSchedulerActivity: EventOccurrenceSchedulerActivityInterface

    private lateinit var calendar: Calendar
    private lateinit var properties: CalendarWorkflowProperties

    override fun start(calendar: Calendar, properties: CalendarWorkflowProperties) {
        init(calendar, properties)

        val periodBag = calculatePeriodBag()

        while (!calendar.isCompleted) {
            calendar.scheduleEventOccurrences(
                eventOccurrenceSchedulerActivity,
                periodBag.period
            )

            if (!calendar.isCompleted) {
                WorkflowHelper.sleepToDate(periodBag.end)
            }
        }
    }

    private fun init(calendar: Calendar, properties: CalendarWorkflowProperties) {
        this.calendar = calendar
        this.properties = properties
    }

    override fun disable() {
        calendar.disableEventOccurrences(
            eventOccurrenceSchedulerActivity,
            calculatePeriodBag().period
        )
    }

    override fun enable() {
        calendar.enableEventOccurrences(
            eventOccurrenceSchedulerActivity,
            calculatePeriodBag().period
        )
    }

    private fun calculatePeriodBag(): PeriodBag {
        val now = WorkflowHelper.now()
        val periodEnd = now.plus(properties.periodDuration)

        return PeriodBag(now.toPeriodTo(periodEnd), periodEnd)
    }

    data class PeriodBag(val period: Period, val end: ZonedDateTime)
}
