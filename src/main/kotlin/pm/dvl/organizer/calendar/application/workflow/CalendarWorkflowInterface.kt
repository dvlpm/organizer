package pm.dvl.organizer.calendar.application.workflow

import io.temporal.workflow.SignalMethod
import io.temporal.workflow.WorkflowInterface
import io.temporal.workflow.WorkflowMethod
import pm.dvl.organizer.calendar.application.workflow.properties.CalendarWorkflowProperties
import pm.dvl.organizer.calendar.domain.model.Calendar

@WorkflowInterface
interface CalendarWorkflowInterface {
    @WorkflowMethod
    fun start(calendar: Calendar, properties: CalendarWorkflowProperties)

    @SignalMethod
    fun disable()

    @SignalMethod
    fun enable()
}
