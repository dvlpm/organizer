package pm.dvl.organizer.calendar.application.workflow

import io.temporal.workflow.SignalMethod
import io.temporal.workflow.WorkflowInterface
import io.temporal.workflow.WorkflowMethod
import pm.dvl.organizer.calendar.domain.model.EventOccurrence

@WorkflowInterface
interface EventWorkflowInterface {
    @WorkflowMethod
    fun schedule(eventOccurrence: EventOccurrence)

    @SignalMethod
    fun disable()

    @SignalMethod
    fun enable()
}
