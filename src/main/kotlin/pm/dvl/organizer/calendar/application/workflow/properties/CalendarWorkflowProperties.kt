package pm.dvl.organizer.calendar.application.workflow.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import java.time.Duration

@ConfigurationProperties("calendar")
class CalendarWorkflowProperties {
    lateinit var periodDuration: Duration
}
