package pm.dvl.organizer.calendar.application.workflow

import ai.applica.spring.boot.starter.temporal.annotations.ActivityStub
import ai.applica.spring.boot.starter.temporal.annotations.TemporalWorkflow
import org.springframework.stereotype.Component
import pm.dvl.organizer.calendar.application.activity.EventOccurrencePublisherActivityInterface
import pm.dvl.organizer.calendar.domain.model.EventOccurrence
import pm.dvl.organizer.calendar.application.workflow.helper.WorkflowHelper

@Component
@TemporalWorkflow("Event")
class EventWorkflow : EventWorkflowInterface {
    @ActivityStub
    private lateinit var eventOccurrencePublisherActivity: EventOccurrencePublisherActivityInterface

    private lateinit var eventOccurrence: EventOccurrence

    override fun schedule(eventOccurrence: EventOccurrence) {
        init(eventOccurrence)

        WorkflowHelper.sleepToDate(eventOccurrence.startAt)

        eventOccurrence.occur(eventOccurrencePublisherActivity)
    }

    private fun init(eventOccurrence: EventOccurrence) {
        this.eventOccurrence = eventOccurrence
    }

    override fun disable() {
        eventOccurrence.disable()
    }

    override fun enable() {
        eventOccurrence.enable()
    }
}
