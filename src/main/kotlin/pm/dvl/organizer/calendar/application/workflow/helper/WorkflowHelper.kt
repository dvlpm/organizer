package pm.dvl.organizer.calendar.application.workflow.helper

import io.temporal.workflow.Workflow
import pm.dvl.organizer.common.extensions.toZonedDateTime
import java.time.Duration
import java.time.ZonedDateTime

class WorkflowHelper {
    companion object {
        fun sleepToDate(date: ZonedDateTime) {
            val sleepMillis = date.toInstant().toEpochMilli() - Workflow.currentTimeMillis()

            if (sleepMillis > 0) {
                Workflow.sleep(Duration.ofMillis(sleepMillis))
            }
        }

        fun now(): ZonedDateTime {
            return Workflow.currentTimeMillis().toZonedDateTime()
        }
    }
}
