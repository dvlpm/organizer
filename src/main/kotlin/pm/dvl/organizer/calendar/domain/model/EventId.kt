package pm.dvl.organizer.calendar.domain.model

import java.util.*

data class EventId(val value: UUID)
