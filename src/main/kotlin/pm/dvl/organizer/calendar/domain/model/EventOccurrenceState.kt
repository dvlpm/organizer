package pm.dvl.organizer.calendar.domain.model

enum class EventOccurrenceState {
    ENABLED,
    DISABLED
}
