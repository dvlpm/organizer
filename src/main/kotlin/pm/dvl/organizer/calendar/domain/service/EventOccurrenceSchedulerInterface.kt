package pm.dvl.organizer.calendar.domain.service

import pm.dvl.organizer.calendar.domain.model.EventOccurrence

interface EventOccurrenceSchedulerInterface {
    fun schedule(eventOccurrence: EventOccurrence)
    fun disable(eventOccurrence: EventOccurrence)
    fun enable(eventOccurrence: EventOccurrence)
}
