package pm.dvl.organizer.calendar.domain.model

enum class CalendarState {
    ENABLED,
    DISABLED,
    COMPLETED
}
