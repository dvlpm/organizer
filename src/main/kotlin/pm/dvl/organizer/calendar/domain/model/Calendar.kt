package pm.dvl.organizer.calendar.domain.model

import net.fortuna.ical4j.model.Calendar
import net.fortuna.ical4j.model.Period
import net.fortuna.ical4j.model.Property
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.property.Uid
import pm.dvl.organizer.calendar.domain.model.CalendarState.*
import pm.dvl.organizer.calendar.domain.service.EventOccurrenceSchedulerInterface
import pm.dvl.organizer.common.extensions.*
import java.time.ZoneId
import java.time.ZoneOffset
import java.util.*

class Calendar(
    val id: CalendarId,
    private val calendar: Calendar
) {
    private var state: CalendarState = ENABLED

    init {
        events.forEach {
            if (it.properties.getProperty<Uid?>(Property.UID) == null) {
                it.properties.add(Uid(UUID.randomUUID().toString()))
            }
        }
    }

    fun scheduleEventOccurrences(
        eventOccurrenceScheduler: EventOccurrenceSchedulerInterface,
        period: Period
    ) {
        if (state == DISABLED) return

        var needToScheduleEventsOccurrences = false

        events.forEach { event ->
            needToScheduleEventsOccurrences = needToScheduleEventsOccurrences || event.isRecurrent
            event.calculateRecurrenceSet(period).forEach { schedule ->
                eventOccurrenceScheduler.schedule(createEventOccurrence(event, schedule))
            }
        }

        if (!needToScheduleEventsOccurrences) {
            state = COMPLETED
        }
    }

    private fun Uid.toEventId(): EventId {
        return EventId(UUID.fromString(value))
    }

    fun disableEventOccurrences(
        eventOccurrenceScheduler: EventOccurrenceSchedulerInterface,
        period: Period
    ) {
        events.forEach { event ->
            event.calculateRecurrenceSet(period).forEach { schedule ->
                eventOccurrenceScheduler.disable(createEventOccurrence(event, schedule))
            }
        }

        state = DISABLED
    }

    fun enableEventOccurrences(
        eventOccurrenceScheduler: EventOccurrenceSchedulerInterface,
        period: Period
    ) {
        events.forEach { event ->
            event.calculateRecurrenceSet(period).forEach { schedule ->
                eventOccurrenceScheduler.enable(createEventOccurrence(event, schedule))
            }
        }

        state = ENABLED
    }

    private fun createEventOccurrence(event: VEvent, period: Period): EventOccurrence {
        return EventOccurrence(
            id,
            event.uid.toEventId(),
            period.start.shiftFromOffset(zoneOffset).toZonedDateTime()
        )
    }

    val isCompleted: Boolean
        get() = state == COMPLETED

    private val events: List<VEvent>
        get() = calendar.events

    private val zoneOffset: ZoneOffset
        get() = calendar.zoneOffset ?: ZoneId.systemDefault().toZoneOffset()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is pm.dvl.organizer.calendar.domain.model.Calendar) return false

        if (id != other.id) return false
        if (events.size != other.events.size) return false
        if (zoneOffset != other.zoneOffset) return false

        return true
    }

    override fun hashCode(): Int {
        return id.toString().hashCode()
    }
}
