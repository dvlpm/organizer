package pm.dvl.organizer.calendar.domain.model

import pm.dvl.organizer.calendar.domain.service.EventOccurrencePublisherInterface
import java.time.ZonedDateTime
import pm.dvl.organizer.calendar.domain.model.EventOccurrenceState.*

class EventOccurrence(
    val calendarId: CalendarId,
    val eventId: EventId,
    val startAt: ZonedDateTime
) {
    private var state = ENABLED

    fun occur(eventOccurrencePublisher: EventOccurrencePublisherInterface) {
        if (state != ENABLED) return

        eventOccurrencePublisher.publish(this)
    }

    fun disable() {
        state = DISABLED
    }

    fun enable() {
        state = ENABLED
    }
}
