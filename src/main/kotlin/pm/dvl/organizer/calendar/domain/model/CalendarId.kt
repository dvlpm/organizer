package pm.dvl.organizer.calendar.domain.model

import java.util.*

data class CalendarId(val value: UUID)
