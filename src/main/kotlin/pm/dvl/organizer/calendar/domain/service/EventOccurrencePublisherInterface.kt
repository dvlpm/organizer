package pm.dvl.organizer.calendar.domain.service

import pm.dvl.organizer.calendar.domain.model.EventOccurrence

interface EventOccurrencePublisherInterface {
    fun publish(occurrence: EventOccurrence)
}
