package pm.dvl.organizer.calendar.domain.model

data class ScheduleEventsOccurrencesForPeriodResult(
    val needToScheduleEventsOccurrences: Boolean
)
