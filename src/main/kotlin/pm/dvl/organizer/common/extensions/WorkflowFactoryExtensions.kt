package pm.dvl.organizer.common.extensions

import ai.applica.spring.boot.starter.temporal.WorkflowFactory
import kotlin.reflect.KClass

fun <T: Any, C: T> WorkflowFactory.makeStubWithWorkflowId(workflowInterface: KClass<T>, workflowClas: KClass<C>, workflowId: String): T {
    return makeStub(
        workflowInterface.java,
        defaultOptionsBuilder(workflowClas.java)
            .setWorkflowId(workflowId)
    )
}
