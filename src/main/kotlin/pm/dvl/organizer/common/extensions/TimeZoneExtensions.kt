package pm.dvl.organizer.common.extensions

import net.fortuna.ical4j.model.Property
import net.fortuna.ical4j.model.PropertyList
import net.fortuna.ical4j.model.component.VTimeZone
import net.fortuna.ical4j.model.property.TzId
import net.fortuna.ical4j.model.property.TzName
import net.fortuna.ical4j.model.property.TzOffsetTo
import java.time.ZoneOffset
import java.util.*

fun TimeZone.toVTimeZone(): VTimeZone {
    val properties = PropertyList<Property>(3)
    properties.add(TzOffsetTo(toZoneOffset()))
    properties.add(TzName(displayName ?: "UTC"))
    properties.add(TzId(id ?: "UTC"))

    return VTimeZone(properties)
}

fun TimeZone.toZoneOffset(): ZoneOffset {
    return ZoneOffset.ofTotalSeconds(rawOffset / 1000)
}
