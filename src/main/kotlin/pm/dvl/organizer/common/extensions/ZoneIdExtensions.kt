package pm.dvl.organizer.common.extensions

import java.time.ZoneId
import java.time.ZoneOffset
import java.util.*

fun ZoneId.toZoneOffset(): ZoneOffset {
    return TimeZone.getTimeZone(this).toZoneOffset()
}
