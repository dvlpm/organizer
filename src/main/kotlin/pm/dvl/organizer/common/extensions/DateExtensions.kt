package pm.dvl.organizer.common.extensions

import net.fortuna.ical4j.model.DateTime
import java.util.*


fun Date.toiCalDateTime(timeZone: TimeZone? = null): DateTime {
    return if (timeZone == null) DateTime(this) else DateTime(
        this, net.fortuna.ical4j.model.TimeZone(timeZone.toVTimeZone())
    )
}
