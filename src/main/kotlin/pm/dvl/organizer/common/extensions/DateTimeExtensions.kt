package pm.dvl.organizer.common.extensions

import net.fortuna.ical4j.model.DateTime
import java.time.ZoneOffset
import java.time.ZonedDateTime

fun DateTime.toZonedDateTime(): ZonedDateTime {
    return time.toZonedDateTime()
}

fun DateTime.shiftFromOffset(zoneOffset: ZoneOffset): DateTime {
    return DateTime(time - zoneOffset.totalSeconds * 1000)
}

fun DateTime.shiftToOffset(zoneOffset: ZoneOffset): DateTime {
    return DateTime(time + zoneOffset.totalSeconds * 1000)
}

fun DateTime.shiftToDays(days: Int): DateTime {
    return DateTime(time + days * 24 * 60 * 60 * 1000)
}
