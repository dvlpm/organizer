package pm.dvl.organizer.common.extensions

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun LocalDateTime.format(pattern: String): String {
    return format(DateTimeFormatter.ofPattern(pattern))
}
