package pm.dvl.organizer.common.extensions

import net.fortuna.ical4j.model.Calendar
import net.fortuna.ical4j.model.Component
import net.fortuna.ical4j.model.Property
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.component.VTimeZone
import net.fortuna.ical4j.model.property.TzOffsetTo
import java.time.ZoneOffset

val Calendar.events: List<VEvent>
    get() = getComponents(Component.VEVENT)

val Calendar.zoneOffset: ZoneOffset?
    get() = getComponent<VTimeZone>(Component.VTIMEZONE).getProperty<TzOffsetTo>(Property.TZOFFSETTO)?.offset
