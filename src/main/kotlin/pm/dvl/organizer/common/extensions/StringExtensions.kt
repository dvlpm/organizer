package pm.dvl.organizer.common.extensions

import net.fortuna.ical4j.model.DateTime
import java.text.SimpleDateFormat

fun String.toDateTime(): DateTime {
    return DateTime(SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this))
}
