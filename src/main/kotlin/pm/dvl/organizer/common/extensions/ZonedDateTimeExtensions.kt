package pm.dvl.organizer.common.extensions

import net.fortuna.ical4j.model.Calendar
import net.fortuna.ical4j.model.DateTime
import net.fortuna.ical4j.model.Period
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.property.DtStart
import java.time.ZonedDateTime

fun ZonedDateTime.toPeriodTo(end: ZonedDateTime): Period {
    return Period(this.toiCalDateTime(), end.toiCalDateTime())
}

private fun ZonedDateTime.toiCalDateTime(): DateTime {
    return DateTime(this.toInstant().toEpochMilli())
}

fun ZonedDateTime.toCalendar(): Calendar {
    val event = VEvent(false)

    event.properties.add(DtStart(toiCalDateTime()))

    return event.toCalendar()
}
