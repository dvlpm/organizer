package pm.dvl.organizer.common.extensions

import net.fortuna.ical4j.model.Calendar
import net.fortuna.ical4j.model.ComponentList
import net.fortuna.ical4j.model.Property
import net.fortuna.ical4j.model.component.CalendarComponent
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.property.RRule
import java.util.*

fun VEvent.toCalendar(timeZone: TimeZone? = null): Calendar {
    val componentList: ComponentList<CalendarComponent> = ComponentList(listOf())
    componentList.add(this)
    if (timeZone != null) componentList.add(timeZone.toVTimeZone())

    return Calendar(componentList)
}

val VEvent.isRecurrent: Boolean
    get() = rrule != null

val VEvent.rrule: RRule?
    get() = getProperty(Property.RRULE)