package pm.dvl.organizer.common

import kotlinx.coroutines.delay

interface RepositoryWithWaitForInterface {
    val attempts: Int
    val delay: Long

    suspend fun <R> waitFor(block: () -> R): R? {
        for (i in 1..attempts) {
            val model = block()
            if (model != null) return model
            delay(delay)
        }

        return null
    }
}
