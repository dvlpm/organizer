package pm.dvl.organizer.common.infrastructure.foundation.configuration

import org.axonframework.common.jdbc.PersistenceExceptionResolver
import org.axonframework.common.jpa.EntityManagerProvider
import org.axonframework.common.transaction.TransactionManager
import org.axonframework.eventsourcing.EventCountSnapshotTriggerDefinition
import org.axonframework.eventsourcing.SnapshotTriggerDefinition
import org.axonframework.eventsourcing.Snapshotter
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore
import org.axonframework.eventsourcing.eventstore.EventStorageEngine
import org.axonframework.eventsourcing.eventstore.EventStore
import org.axonframework.eventsourcing.eventstore.jpa.JpaEventStorageEngine
import org.axonframework.serialization.Serializer
import org.axonframework.spring.config.AxonConfiguration
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class AxonConfiguration {

    @Bean
    fun eventStore(storageEngine: EventStorageEngine?, configuration: AxonConfiguration): EmbeddedEventStore? {
        return EmbeddedEventStore.builder()
            .storageEngine(storageEngine)
            .messageMonitor(configuration.messageMonitor(EventStore::class.java, "eventStore"))
            .build()
    }

    @Bean
    fun storageEngine(
        defaultSerializer: Serializer?,
        persistenceExceptionResolver: PersistenceExceptionResolver?,
        @Qualifier("eventSerializer") eventSerializer: Serializer?,
        configuration: AxonConfiguration,
        entityManagerProvider: EntityManagerProvider?,
        transactionManager: TransactionManager?
    ): EventStorageEngine? {
        return JpaEventStorageEngine.builder()
            .snapshotSerializer(defaultSerializer)
            .upcasterChain(configuration.upcasterChain())
            .persistenceExceptionResolver(persistenceExceptionResolver)
            .eventSerializer(eventSerializer)
            .entityManagerProvider(entityManagerProvider)
            .transactionManager(transactionManager)
            .build()
    }

    @Bean
    fun aggregateSnapshotTriggerDefinition(
        snapshotter: Snapshotter,
        @Value("\${axon.aggregate.snapshot-threshold:250}") threshold: String
    ): SnapshotTriggerDefinition {
        return EventCountSnapshotTriggerDefinition(snapshotter, threshold.toInt())
    }
}
