package pm.dvl.organizer.common.infrastructure.foundation.configuration

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration

@Configuration
@EntityScan(
    "org.axonframework.eventsourcing.eventstore.jpa",
    "org.axonframework.modelling.saga.repository.jpa",
    "org.axonframework.eventhandling.tokenstore.jpa"
)
class JpaCommonConfiguration {}
