package pm.dvl.organizer.telegram.application.activity

import io.temporal.activity.ActivityInterface
import io.temporal.activity.ActivityMethod
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.query.projection.ChatProjection

@ActivityInterface
interface ChatProjectionProviderActivityInterface {
    @ActivityMethod
    fun provideById(id: ChatId): ChatProjection?
}
