package pm.dvl.organizer.telegram.application.activity

import io.temporal.activity.ActivityInterface
import io.temporal.activity.ActivityMethod
import pm.dvl.organizer.telegram.domain.command.*

@ActivityInterface
interface CommandGatewayActivityInterface {
    /*
     * Activity can have neither overloaded methods nor generic types nor "Any" type.
     */
    @ActivityMethod
    fun sendCreateChatCommand(command: CreateChatCommand)
    @ActivityMethod
    fun sendSetupChatTimeZoneCommand(command: SetupChatTimeZoneCommand)
    @ActivityMethod
    fun sendCreateTaskViaChatCommand(command: CreateTaskOccurrenceViaChatCommand)
    @ActivityMethod
    fun sendPressButtonCommand(command: PressButtonCommand)
    @ActivityMethod
    fun sendHelpCommand(command: HelpCommand)
    @ActivityMethod
    fun sendReactAccordingToModeCommand(command: ReactAccordingToModeCommand)
}
