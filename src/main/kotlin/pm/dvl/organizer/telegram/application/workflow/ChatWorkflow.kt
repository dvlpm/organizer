package pm.dvl.organizer.telegram.application.workflow

import ai.applica.spring.boot.starter.temporal.annotations.ActivityStub
import ai.applica.spring.boot.starter.temporal.annotations.TemporalWorkflow
import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.model.Update
import org.springframework.stereotype.Component
import pm.dvl.organizer.telegram.application.activity.ChatProjectionProviderActivityInterface
import pm.dvl.organizer.telegram.application.activity.CommandGatewayActivityInterface
import pm.dvl.organizer.telegram.domain.command.*
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.query.projection.ChatProjection
import pm.dvl.organizer.telegram.extensions.isOneOfCommand
import pm.dvl.organizer.user.domain.model.UserId
import java.util.*

@Component
@TemporalWorkflow("Chat")
class ChatWorkflow : ChatWorkflowInterface {
    @ActivityStub
    private lateinit var chatProjectionProviderActivity: ChatProjectionProviderActivityInterface

    @ActivityStub
    private lateinit var commandGatewayActivity: CommandGatewayActivityInterface

    override fun start(message: Message, update: Update) {
        doHandle(message, update)
    }

    override fun handle(message: Message, update: Update) {
        doHandle(message, update)
    }

    private fun doHandle(message: Message, update: Update) {
        val chatProjection = provideChatProjectionByChatId(ChatId(message.chat().id()))

        handleMessage(message, chatProjection.id)
    }

    private fun provideChatProjectionByChatId(chatId: ChatId): ChatProjection {
        var chatProjection = chatProjectionProviderActivity.provideById(chatId)

        if (chatProjection == null) {
            commandGatewayActivity.sendCreateChatCommand(CreateChatCommand(chatId, UserId(UUID.randomUUID())))
        }

        while (true) {
            chatProjection = chatProjectionProviderActivity.provideById(chatId)

            if (chatProjection != null) {
                return chatProjection
            }
        }
    }

    private fun handleMessage(message: Message, chatId: ChatId) {
        when {
            message.isOneOfCommand("start", "help") -> {
                commandGatewayActivity.sendHelpCommand(HelpCommand(chatId))
            }
            message.isOneOfCommand("timezone") -> {
                commandGatewayActivity.sendSetupChatTimeZoneCommand(
                    SetupChatTimeZoneCommand(
                        chatId,
                        message.text().withoutCommands("timezone")
                    )
                )
            }
            message.isOneOfCommand("task") -> {
                commandGatewayActivity.sendCreateTaskViaChatCommand(
                    CreateTaskOccurrenceViaChatCommand(
                        chatId,
                        message.text().withoutCommands("task")
                    )
                )
            }
            else -> {
                commandGatewayActivity.sendReactAccordingToModeCommand(
                    ReactAccordingToModeCommand(
                        chatId,
                        message.text()
                    )
                )
            }
        }
    }

    private fun String.withoutCommands(vararg commands: String): String {
        var withoutCommands = this

        commands.onEach { withoutCommands = withoutCommands.withoutCommand(it) }

        return withoutCommands
    }

    private fun String.withoutCommand(command: String): String {
        return replaceFirst(if (startsWith("/")) "/$command " else "$command ", "").trim()
    }
}
