package pm.dvl.organizer.telegram.application.workflow

import com.pengrad.telegrambot.model.Message
import com.pengrad.telegrambot.model.Update
import io.temporal.workflow.SignalMethod
import io.temporal.workflow.WorkflowInterface
import io.temporal.workflow.WorkflowMethod

@WorkflowInterface
interface ChatWorkflowInterface {
    @WorkflowMethod
    fun start(message: Message, update: Update)
    @SignalMethod
    fun handle(message: Message, update: Update)
}
