package pm.dvl.organizer.telegram.application.workflow

import com.pengrad.telegrambot.model.CallbackQuery
import com.pengrad.telegrambot.model.Update
import io.temporal.workflow.WorkflowInterface
import io.temporal.workflow.WorkflowMethod

@WorkflowInterface
interface CallbackQueryWorkflowInterface {
    @WorkflowMethod
    fun start(callbackQuery: CallbackQuery, update: Update)
}
