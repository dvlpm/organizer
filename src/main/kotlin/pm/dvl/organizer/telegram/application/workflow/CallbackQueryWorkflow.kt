package pm.dvl.organizer.telegram.application.workflow

import ai.applica.spring.boot.starter.temporal.annotations.ActivityStub
import ai.applica.spring.boot.starter.temporal.annotations.TemporalWorkflow
import com.pengrad.telegrambot.model.CallbackQuery
import com.pengrad.telegrambot.model.Update
import org.springframework.stereotype.Component
import pm.dvl.organizer.telegram.application.activity.CommandGatewayActivityInterface
import pm.dvl.organizer.telegram.domain.command.PressButtonCommand
import pm.dvl.organizer.telegram.domain.model.ButtonId
import java.util.*

@Component
@TemporalWorkflow("CallbackQuery")
class CallbackQueryWorkflow : CallbackQueryWorkflowInterface {
    @ActivityStub
    private lateinit var commandGatewayActivity: CommandGatewayActivityInterface

    override fun start(callbackQuery: CallbackQuery, update: Update) {
        commandGatewayActivity.sendPressButtonCommand(
            PressButtonCommand(
                ButtonId(UUID.fromString(callbackQuery.data()))
            )
        )
    }
}
