package pm.dvl.organizer.telegram.domain.event

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.telegram.domain.model.*

data class TaskOccurrenceMessageCreatedEvent(
    val taskOccurrenceMessageId: TaskOccurrenceMessageId,
    val taskOccurrenceId: TaskOccurrenceId,
    val chatId: ChatId,
    val messageId: MessageId,
    val buttonPayloads: Map<TaskOccurrenceMessageButtonType, ButtonPayload>
)
