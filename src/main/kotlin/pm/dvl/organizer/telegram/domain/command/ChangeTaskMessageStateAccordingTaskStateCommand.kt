package pm.dvl.organizer.telegram.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskState
import pm.dvl.organizer.telegram.domain.model.TaskMessageId

data class ChangeTaskMessageStateAccordingTaskStateCommand(
    @TargetAggregateIdentifier
    val taskMessageId: TaskMessageId,
    val taskState: TaskState
)
