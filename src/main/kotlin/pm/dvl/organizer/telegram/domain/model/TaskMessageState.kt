package pm.dvl.organizer.telegram.domain.model

enum class TaskMessageState {
    DRAFTED, ENABLED, DISABLED;

    fun inverse() = when (this) {
        ENABLED -> DISABLED
        DISABLED -> ENABLED
        DRAFTED -> DRAFTED
    }
}
