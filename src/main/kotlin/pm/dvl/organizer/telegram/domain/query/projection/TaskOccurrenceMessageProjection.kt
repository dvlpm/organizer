package pm.dvl.organizer.telegram.domain.query.projection

import pm.dvl.organizer.common.annotations.Persistable
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageId

@Persistable
data class TaskOccurrenceMessageProjection(
    var id: TaskOccurrenceMessageId,
    var taskOccurrenceId: TaskOccurrenceId,
)
