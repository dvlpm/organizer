package pm.dvl.organizer.telegram.domain.query.projection

import pm.dvl.organizer.common.annotations.Persistable
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.telegram.domain.model.TaskMessageId

@Persistable
data class TaskMessageProjection(
    var id: TaskMessageId,
    var taskId: TaskId,
)
