package pm.dvl.organizer.telegram.domain.model

enum class TaskOccurrenceMessageButtonType {
    DONE,
    CANCELED
}
