package pm.dvl.organizer.telegram.domain.model

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.extensions.kotlin.applyEvent
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import pm.dvl.organizer.telegram.domain.command.CreateButtonCommand
import pm.dvl.organizer.telegram.domain.command.PressButtonCommand
import pm.dvl.organizer.telegram.domain.event.ButtonCreatedEvent
import pm.dvl.organizer.telegram.domain.event.ButtonPressedEvent

@Aggregate(snapshotTriggerDefinition = "aggregateSnapshotTriggerDefinition")
class Button() {
    @AggregateIdentifier
    private lateinit var id: ButtonId

    @CommandHandler
    constructor(command: CreateButtonCommand) : this() {
        applyEvent(
            ButtonCreatedEvent(
                command.buttonId,
            )
        )
    }

    @EventSourcingHandler
    fun on(event: ButtonCreatedEvent) {
        id = event.buttonId
    }

    @CommandHandler
    fun press(command: PressButtonCommand) {
        applyEvent(ButtonPressedEvent(id))
    }
}
