package pm.dvl.organizer.telegram.domain.query.repository

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageId
import pm.dvl.organizer.telegram.domain.query.projection.TaskOccurrenceMessageProjection

interface TaskOccurrenceMessageProjectionRepositoryInterface {
    fun save(projection: TaskOccurrenceMessageProjection)
    fun findOneOrNullById(id: TaskOccurrenceMessageId): TaskOccurrenceMessageProjection?
    fun findAllByTaskId(taskOccurrenceId: TaskOccurrenceId): List<TaskOccurrenceMessageProjection>
}
