package pm.dvl.organizer.telegram.domain.model

import pm.dvl.organizer.common.annotations.Persistable
import java.util.UUID

@Persistable
data class TaskMessageId(var value: UUID) : java.io.Serializable
