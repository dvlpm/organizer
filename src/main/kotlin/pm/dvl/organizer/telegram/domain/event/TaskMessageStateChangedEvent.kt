package pm.dvl.organizer.telegram.domain.event

import pm.dvl.organizer.telegram.domain.model.ButtonPayload
import pm.dvl.organizer.telegram.domain.model.TaskMessageButtonType
import pm.dvl.organizer.telegram.domain.model.TaskMessageId
import pm.dvl.organizer.telegram.domain.model.TaskMessageState

data class TaskMessageStateChangedEvent(
    val taskMessageId: TaskMessageId,
    val state: TaskMessageState,
    val buttonPayloads: Map<TaskMessageButtonType, ButtonPayload>
)
