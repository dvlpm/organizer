package pm.dvl.organizer.telegram.domain.event

import pm.dvl.organizer.telegram.domain.model.ChatId
import java.util.TimeZone

data class ChatTimeZoneChangedEvent(
    val chatId: ChatId,
    val timeZone: TimeZone
)
