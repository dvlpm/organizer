package pm.dvl.organizer.telegram.domain.query.repository

import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.telegram.domain.query.projection.TaskMessageProjection

interface TaskMessageProjectionRepositoryInterface {
    fun save(projection: TaskMessageProjection)
    fun findAllByTaskId(taskId: TaskId): List<TaskMessageProjection>
}
