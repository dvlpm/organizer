package pm.dvl.organizer.telegram.domain.query.projector

import org.axonframework.eventhandling.EventHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.telegram.domain.event.TaskMessageCreatedEvent
import pm.dvl.organizer.telegram.domain.query.projection.TaskMessageProjection
import pm.dvl.organizer.telegram.domain.query.repository.TaskMessageProjectionRepositoryInterface

@Component
class TaskMessageProjector(
    @Autowired private val repository: TaskMessageProjectionRepositoryInterface
) {
    @EventHandler
    fun on(event: TaskMessageCreatedEvent) {
        repository.save(TaskMessageProjection(event.taskMessageId, event.taskId))
    }
}
