package pm.dvl.organizer.telegram.domain.query.projector

import org.axonframework.eventhandling.EventHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.telegram.domain.event.ChatCreatedEvent
import pm.dvl.organizer.telegram.domain.event.ChatTimeZoneChangedEvent
import pm.dvl.organizer.telegram.domain.query.projection.ChatProjection
import pm.dvl.organizer.telegram.domain.query.repository.ChatProjectionRepositoryInterface

@Component
class ChatProjector(
    @Autowired private val repository: ChatProjectionRepositoryInterface
) {
    @EventHandler
    fun on(event: ChatCreatedEvent) {
        repository.save(ChatProjection(event.chatId, event.globalUserId))
    }

    @EventHandler
    fun on(event: ChatTimeZoneChangedEvent) {
        val chatProjection = repository.findOneOrNullById(event.chatId) ?: return

        chatProjection.updateTimeZone(event.timeZone)

        repository.save(chatProjection)
    }
}
