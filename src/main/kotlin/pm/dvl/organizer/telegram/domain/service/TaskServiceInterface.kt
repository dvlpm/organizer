package pm.dvl.organizer.telegram.domain.service

import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskState

interface TaskServiceInterface {
    fun create(taskId: TaskId, taskData: TaskData, schedule: Schedule? = null)
    fun changeState(taskId: TaskId, taskState: TaskState)
}
