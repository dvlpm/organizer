package pm.dvl.organizer.telegram.domain.event

import pm.dvl.organizer.telegram.domain.model.ButtonPayload
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageButtonType
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageId
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageState

data class TaskOccurrenceMessageStateChangedEvent(
    val taskOccurrenceMessageId: TaskOccurrenceMessageId,
    val state: TaskOccurrenceMessageState,
    val buttonPayloads: Map<TaskOccurrenceMessageButtonType, ButtonPayload>
)
