package pm.dvl.organizer.telegram.domain.payload

import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.model.TaskMessageId

data class CreateTaskMessagePayload(
    val taskMessageId: TaskMessageId,
    val taskId: TaskId,
    val chatId: ChatId,
)
