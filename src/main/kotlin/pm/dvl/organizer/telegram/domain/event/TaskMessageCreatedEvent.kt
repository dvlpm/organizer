package pm.dvl.organizer.telegram.domain.event

import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.telegram.domain.model.*

data class TaskMessageCreatedEvent(
    val taskMessageId: TaskMessageId,
    val taskId: TaskId,
    val chatId: ChatId,
    val messageId: MessageId,
    val state: TaskMessageState,
    val buttonPayloads: Map<TaskMessageButtonType, ButtonPayload>,
)
