package pm.dvl.organizer.telegram.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.telegram.domain.model.ChatId

data class CreateChatCommand(
    @TargetAggregateIdentifier
    val chatId: ChatId,
    val globalUserId: pm.dvl.organizer.user.domain.model.UserId,
)
