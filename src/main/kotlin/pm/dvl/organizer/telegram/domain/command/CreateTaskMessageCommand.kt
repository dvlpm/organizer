package pm.dvl.organizer.telegram.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.model.TaskMessageId

data class CreateTaskMessageCommand(
    @TargetAggregateIdentifier
    val taskMessageId: TaskMessageId,
    val taskId: TaskId,
    val chatId: ChatId,
)
