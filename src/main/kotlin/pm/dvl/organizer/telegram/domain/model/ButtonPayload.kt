package pm.dvl.organizer.telegram.domain.model

import java.util.UUID

data class ButtonPayload(
    val text: String,
    val buttonId: ButtonId,
) {
    constructor(text: String) : this(text, ButtonId(UUID.randomUUID()))

    fun withText(text: String): ButtonPayload {
        return ButtonPayload(
            text,
            buttonId
        )
    }
}
