package pm.dvl.organizer.telegram.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.telegram.domain.model.ButtonId

data class CreateButtonCommand(
    @TargetAggregateIdentifier
    val buttonId: ButtonId,
)
