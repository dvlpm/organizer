package pm.dvl.organizer.telegram.domain.model

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.extensions.kotlin.applyEvent
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import org.springframework.beans.factory.annotation.Autowired
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskState
import pm.dvl.organizer.task.domain.query.repository.TaskProjectionRepositoryInterface
import pm.dvl.organizer.telegram.domain.command.ChangeTaskMessageStateAccordingTaskStateCommand
import pm.dvl.organizer.telegram.domain.command.CreateTaskMessageCommand
import pm.dvl.organizer.telegram.domain.command.PressTaskMessageButtonCommand
import pm.dvl.organizer.telegram.domain.event.TaskMessageCreatedEvent
import pm.dvl.organizer.telegram.domain.event.TaskMessageStateChangedEvent
import pm.dvl.organizer.telegram.domain.service.BotInterface
import pm.dvl.organizer.telegram.domain.service.TaskServiceInterface
import pm.dvl.organizer.telegram.extensions.display

@Aggregate(snapshotTriggerDefinition = "aggregateSnapshotTriggerDefinition")
class TaskMessage() {
    @AggregateIdentifier
    private lateinit var id: TaskMessageId
    private lateinit var taskId: TaskId
    private lateinit var chatId: ChatId
    private lateinit var state: TaskMessageState
    private var buttonPayloads: Map<TaskMessageButtonType, ButtonPayload> = mapOf()
    private lateinit var messageId: MessageId

    @CommandHandler
    constructor(
        command: CreateTaskMessageCommand,
        @Autowired bot: BotInterface,
        @Autowired taskProjectionRepository: TaskProjectionRepositoryInterface
    ) : this() {
        val taskProjection = taskProjectionRepository.findOneOrNullById(command.taskId) ?: return
        val state = TaskMessageState.ENABLED
        val buttonPayloads = state.toButtonPayloads()

        val messageId = bot.send(
            command.chatId, "\uD83D\uDDD3 ${taskProjection.summary} ${taskProjection.tags.display()} #schedule",
            buttonPayloads.values.toList()
        )

        applyEvent(
            TaskMessageCreatedEvent(
                command.taskMessageId,
                command.taskId,
                command.chatId,
                messageId,
                state,
                buttonPayloads
            )
        )
    }

    @EventSourcingHandler
    fun on(event: TaskMessageCreatedEvent) {
        id = event.taskMessageId
        taskId = event.taskId
        chatId = event.chatId
        state = event.state
        messageId = event.messageId
        buttonPayloads = event.buttonPayloads
    }

    @CommandHandler
    fun pressButton(
        command: PressTaskMessageButtonCommand,
        @Autowired taskService: TaskServiceInterface,
    ) {
        val targetState = when (state.inverse()) {
            TaskMessageState.ENABLED -> TaskState.ENABLED
            TaskMessageState.DISABLED -> TaskState.DISABLED
            else -> null
        } ?: return

        taskService.changeState(
            taskId,
            targetState
        )
    }

    @CommandHandler
    fun changeStateAccordingTaskState(
        command: ChangeTaskMessageStateAccordingTaskStateCommand,
        @Autowired bot: BotInterface
    ) {
        val targetState = command.taskState.toMessageState()

        if (targetState == state) return

        val buttonPayloads = targetState.toButtonPayloads()

        bot.updateButtons(chatId, messageId, buttonPayloads.values.toList())

        applyEvent(
            TaskMessageStateChangedEvent(
                id,
                targetState,
                buttonPayloads
            )
        )
    }

    private fun TaskState.toMessageState(): TaskMessageState {
        return when (this) {
            TaskState.DISABLED -> TaskMessageState.DISABLED
            TaskState.ENABLED -> TaskMessageState.ENABLED
        }
    }

    private final fun TaskMessageState.toButtonPayloads(): Map<TaskMessageButtonType, ButtonPayload> {
        val changeStateButton = this@TaskMessage.buttonPayloads[TaskMessageButtonType.CHANGE_STATE]
            ?: ButtonPayload(DISABLE_BUTTON_TEXT)

        return mapOf(
            TaskMessageButtonType.CHANGE_STATE to when (this) {
                TaskMessageState.ENABLED -> changeStateButton.withText(DISABLE_BUTTON_TEXT)
                TaskMessageState.DISABLED -> changeStateButton.withText(ENABLE_BUTTON_TEXT)
                TaskMessageState.DRAFTED -> changeStateButton.withText(ENABLE_BUTTON_TEXT)
            }
        )
    }

    @EventSourcingHandler
    fun on(event: TaskMessageStateChangedEvent) {
        state = event.state
        buttonPayloads = event.buttonPayloads
    }

    companion object {
        const val ENABLE_BUTTON_TEXT = "\uD83D\uDD34 Disabled"
        const val DISABLE_BUTTON_TEXT = "\uD83D\uDFE2 Enabled"
    }
}
