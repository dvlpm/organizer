package pm.dvl.organizer.telegram.domain.event

import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.telegram.domain.model.ChatId

data class TaskOccurrenceCreatedViaChatEvent(
    val chatId: ChatId,
    val taskId: TaskId,
)
