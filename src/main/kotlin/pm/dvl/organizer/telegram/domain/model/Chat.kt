package pm.dvl.organizer.telegram.domain.model

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.extensions.kotlin.applyEvent
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import org.springframework.beans.factory.annotation.Autowired
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.telegram.domain.command.*
import pm.dvl.organizer.telegram.domain.event.ChatCreatedEvent
import pm.dvl.organizer.telegram.domain.event.ChatTimeZoneChangedEvent
import pm.dvl.organizer.telegram.domain.event.TaskOccurrenceCreatedViaChatEvent
import pm.dvl.organizer.telegram.domain.service.BotInterface
import pm.dvl.organizer.telegram.domain.service.CreateTaskOccurrenceServiceBag
import java.util.*

@Aggregate(snapshotTriggerDefinition = "aggregateSnapshotTriggerDefinition")
class Chat() {
    @AggregateIdentifier
    private lateinit var id: ChatId
    private lateinit var globalUserId: pm.dvl.organizer.user.domain.model.UserId
    private lateinit var mode: ChatMode
    private var timeZone: TimeZone? = null

    @CommandHandler
    constructor(command: CreateChatCommand) : this() {
        applyEvent(ChatCreatedEvent(command.chatId, command.globalUserId, ChatMode.TASK_CREATION))
    }

    @EventSourcingHandler
    fun on(event: ChatCreatedEvent) {
        id = event.chatId
        globalUserId = event.globalUserId
        mode = event.chatMode
    }

    @CommandHandler
    fun setupTimeZone(
        command: SetupChatTimeZoneCommand,
        @Autowired bot: BotInterface
    ) {
        val timeZone = TimeZone.getTimeZone(command.textTimeZone)

        bot.send(id, "⌚ ${timeZone.displayName}")

        applyEvent(ChatTimeZoneChangedEvent(id, timeZone))
    }

    @EventSourcingHandler
    fun on(event: ChatTimeZoneChangedEvent) {
        timeZone = event.timeZone
    }

    @CommandHandler
    fun createTask(
        command: CreateTaskOccurrenceViaChatCommand,
        @Autowired createTaskOccurrenceServiceBag: CreateTaskOccurrenceServiceBag,
    ) {
        createTask(command.text, createTaskOccurrenceServiceBag)
    }

    @CommandHandler
    fun reactAccordingToMode(
        command: ReactAccordingToModeCommand,
        @Autowired createTaskOccurrenceServiceBag: CreateTaskOccurrenceServiceBag
    ) {
        when (mode) {
            ChatMode.TASK_CREATION -> createTask(command.text, createTaskOccurrenceServiceBag)
        }
    }

    private fun createTask(text: String, createTaskOccurrenceServiceBag: CreateTaskOccurrenceServiceBag) {
        ensureTimeZoneSetup(createTaskOccurrenceServiceBag.bot)

        val nlpResult = timeZone?.run {
            createTaskOccurrenceServiceBag.naturalLanguageParser.parse(
                text,
                this
            )
        } ?: return

        val taskId = TaskId(UUID.randomUUID())

        createTaskOccurrenceServiceBag.taskService.create(
            taskId,
            TaskData(
                nlpResult.text,
                globalUserId,
                globalUserId,
                tags = createTaskOccurrenceServiceBag.tagProvider.provideByOwnerIdAndRankedTagNames(
                    globalUserId,
                    *nlpResult.tags.toTypedArray()
                ).toMutableSet()
            ),
            nlpResult.schedule,
        )

        applyEvent(
            TaskOccurrenceCreatedViaChatEvent(
                id,
                taskId
            )
        )
    }

    private fun ensureTimeZoneSetup(bot: BotInterface) {
        if (timeZone == null) {
            bot.send(id, "Please setup timezone with `/timezone GMT+3` command")
        }
    }

    @CommandHandler
    fun help(
        command: HelpCommand,
        @Autowired bot: BotInterface,
    ) {
        bot.send(
            id,
            """
            I can help you create task schedule like this: 
            `go to gym everyday at 6:00am`
            or simple tasks like this:
            `meet Darya at 7:30 pm`
            Supported schedule rules:
            `everyday`, `every weekday`, `on mondays/tuesdays` etc
            Set your timezone with:
            `/timezone GMT+3` - set GMT+3 timezone.
            """.trimIndent()
        )
    }
}
