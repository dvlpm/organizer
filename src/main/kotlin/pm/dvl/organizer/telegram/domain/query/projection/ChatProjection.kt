package pm.dvl.organizer.telegram.domain.query.projection

import pm.dvl.organizer.common.annotations.Persistable
import pm.dvl.organizer.telegram.domain.model.ChatId
import java.util.*

@Persistable
data class ChatProjection(
    var id: ChatId,
    var globalUserId: pm.dvl.organizer.user.domain.model.UserId,
    var timeZone: TimeZone? = null
) {
    fun updateTimeZone(timeZone: TimeZone): ChatProjection {
        this.timeZone = timeZone

        return this
    }
}
