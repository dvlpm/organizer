package pm.dvl.organizer.telegram.domain.service

import pm.dvl.organizer.tag.domain.model.RankedTag
import pm.dvl.organizer.tag.domain.model.RankedTagName
import pm.dvl.organizer.user.domain.model.UserId

interface RankedTagProviderInterface {
    fun provideByOwnerIdAndRankedTagNames(ownerId: UserId, vararg rankedTagNames: RankedTagName): List<RankedTag>
}
