package pm.dvl.organizer.telegram.domain.model

enum class TaskOccurrenceMessageState {
    PENDING,
    CANCELED,
    DONE
}
