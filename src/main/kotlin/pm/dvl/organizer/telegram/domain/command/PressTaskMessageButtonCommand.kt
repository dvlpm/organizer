package pm.dvl.organizer.telegram.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.telegram.domain.model.ButtonId
import pm.dvl.organizer.telegram.domain.model.TaskMessageId

data class PressTaskMessageButtonCommand(
    @TargetAggregateIdentifier
    val taskMessageId: TaskMessageId,
    val buttonId: ButtonId
)
