package pm.dvl.organizer.telegram.domain.model

data class MessageId(val value: Int)

