package pm.dvl.organizer.telegram.domain.query.projector

import org.axonframework.eventhandling.EventHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.telegram.domain.event.TaskOccurrenceMessageCreatedEvent
import pm.dvl.organizer.telegram.domain.query.projection.TaskOccurrenceMessageProjection
import pm.dvl.organizer.telegram.domain.query.repository.TaskOccurrenceMessageProjectionRepositoryInterface

@Component
class TaskOccurrenceMessageProjector(
    @Autowired private val repository: TaskOccurrenceMessageProjectionRepositoryInterface
) {
    @EventHandler
    fun on(event: TaskOccurrenceMessageCreatedEvent) {
        repository.save(TaskOccurrenceMessageProjection(event.taskOccurrenceMessageId, event.taskOccurrenceId))
    }
}
