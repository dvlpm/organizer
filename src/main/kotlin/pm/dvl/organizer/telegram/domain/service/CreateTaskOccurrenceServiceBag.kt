package pm.dvl.organizer.telegram.domain.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.nlp.domain.service.NaturalLanguageParserInterface

@Component
class CreateTaskOccurrenceServiceBag(
    @Autowired val bot: BotInterface,
    @Autowired val naturalLanguageParser: NaturalLanguageParserInterface,
    @Autowired val taskService: TaskServiceInterface,
    @Autowired val tagProvider: RankedTagProviderInterface,
)
