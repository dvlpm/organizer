package pm.dvl.organizer.telegram.domain.event

import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.model.ChatMode

data class ChatCreatedEvent(
    val chatId: ChatId,
    val globalUserId: pm.dvl.organizer.user.domain.model.UserId,
    val chatMode: ChatMode,
)
