package pm.dvl.organizer.telegram.domain.model

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.extensions.kotlin.applyEvent
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import org.springframework.beans.factory.annotation.Autowired
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskOccurrenceState
import pm.dvl.organizer.task.domain.query.repository.TaskOccurrenceProjectionRepositoryInterface
import pm.dvl.organizer.telegram.domain.command.ChangeTaskOccurrenceMessageStateAccordingTaskOccurrenceStateCommand
import pm.dvl.organizer.telegram.domain.command.CreateTaskOccurrenceMessageCommand
import pm.dvl.organizer.telegram.domain.command.PressTaskOccurrenceMessageButtonCommand
import pm.dvl.organizer.telegram.domain.event.TaskOccurrenceMessageCreatedEvent
import pm.dvl.organizer.telegram.domain.event.TaskOccurrenceMessageStateChangedEvent
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageState.*
import pm.dvl.organizer.telegram.domain.service.BotInterface
import pm.dvl.organizer.telegram.domain.service.TaskOccurrenceServiceInterface
import pm.dvl.organizer.telegram.extensions.display

@Aggregate(snapshotTriggerDefinition = "aggregateSnapshotTriggerDefinition")
class TaskOccurrenceMessage() {
    @AggregateIdentifier
    private lateinit var id: TaskOccurrenceMessageId
    private lateinit var taskOccurrenceId: TaskOccurrenceId
    private lateinit var chatId: ChatId
    private lateinit var messageId: MessageId
    private lateinit var state: TaskOccurrenceMessageState
    private lateinit var buttonPayloads: Map<TaskOccurrenceMessageButtonType, ButtonPayload>

    @CommandHandler
    constructor(
        command: CreateTaskOccurrenceMessageCommand,
        @Autowired taskProjectionRepository: TaskOccurrenceProjectionRepositoryInterface,
        @Autowired bot: BotInterface,
    ) : this() {
        val taskProjection =
            taskProjectionRepository.findOneOrNullById(command.taskOccurrenceId) ?: return
        val buttonPayloads = mapOf(
            TaskOccurrenceMessageButtonType.DONE to ButtonPayload(DONE_BUTTON_TEXT),
            TaskOccurrenceMessageButtonType.CANCELED to ButtonPayload(CANCELED_BUTTON_TEXT)
        )

        val messageId = bot.send(
            command.chatId, "\uD83D\uDD18 ${taskProjection.summary} ${taskProjection.tags.display()} #task",
            buttonPayloads.values.toList()
        )

        applyEvent(
            TaskOccurrenceMessageCreatedEvent(
                command.taskOccurrenceMessageId, command.taskOccurrenceId, command.chatId, messageId, buttonPayloads
            )
        )
    }

    @EventSourcingHandler
    fun on(event: TaskOccurrenceMessageCreatedEvent) {
        id = event.taskOccurrenceMessageId
        taskOccurrenceId = event.taskOccurrenceId
        chatId = event.chatId
        messageId = event.messageId
        state = PENDING
        buttonPayloads = event.buttonPayloads
    }

    @CommandHandler
    fun pressButton(
        command: PressTaskOccurrenceMessageButtonCommand,
        @Autowired taskService: TaskOccurrenceServiceInterface,
    ) {
        val buttonPayloadPair = buttonPayloads.entries.find { it.value.buttonId == command.buttonId } ?: return

        taskService.changeState(taskOccurrenceId, buttonPayloadPair.toState().toTaskState())
    }

    private fun Map.Entry<TaskOccurrenceMessageButtonType, ButtonPayload>.toState(): TaskOccurrenceMessageState {
        // TODO migrate to sealed class for TaskOccurrenceMessageState
        return when {
            key == TaskOccurrenceMessageButtonType.DONE && this@TaskOccurrenceMessage.state != DONE -> DONE
            key == TaskOccurrenceMessageButtonType.DONE && this@TaskOccurrenceMessage.state == DONE -> PENDING
            key == TaskOccurrenceMessageButtonType.CANCELED && this@TaskOccurrenceMessage.state != CANCELED -> CANCELED
            key == TaskOccurrenceMessageButtonType.CANCELED && this@TaskOccurrenceMessage.state == CANCELED -> PENDING
            else -> PENDING
        }
    }

    private fun TaskOccurrenceMessageState.toTaskState(): TaskOccurrenceState {
        return when (this) {
            PENDING -> TaskOccurrenceState.PENDING
            DONE -> TaskOccurrenceState.DONE
            CANCELED -> TaskOccurrenceState.CANCELED
        }
    }

    @CommandHandler
    fun changeStateAccordingTaskState(
        command: ChangeTaskOccurrenceMessageStateAccordingTaskOccurrenceStateCommand,
        @Autowired bot: BotInterface
    ) {
        val targetState = command.taskOccurrenceState.toMessageState()

        if (targetState == this.state) return

        val buttonPayloads = targetState.toButtonPayloads()

        bot.updateButtons(chatId, messageId, buttonPayloads.values.toList())

        applyEvent(
            TaskOccurrenceMessageStateChangedEvent(
                id, targetState, buttonPayloads
            )
        )
    }

    private fun TaskOccurrenceState.toMessageState(): TaskOccurrenceMessageState {
        return when (this) {
            TaskOccurrenceState.DONE -> DONE
            TaskOccurrenceState.CANCELED -> CANCELED
            TaskOccurrenceState.PENDING, TaskOccurrenceState.IN_PROGRESS -> PENDING
        }
    }

    private fun TaskOccurrenceMessageState.toButtonPayloads(): Map<TaskOccurrenceMessageButtonType, ButtonPayload> {
        val doneButtonPayload = this@TaskOccurrenceMessage.buttonPayloads[TaskOccurrenceMessageButtonType.DONE]!!
        val canceledButtonPayload = this@TaskOccurrenceMessage.buttonPayloads[TaskOccurrenceMessageButtonType.CANCELED]!!

        return when (this) {
            DONE -> mapOf(
                TaskOccurrenceMessageButtonType.DONE to doneButtonPayload.withText(PRESSED_DONE_BUTTON_TEXT),
                TaskOccurrenceMessageButtonType.CANCELED to canceledButtonPayload.withText(CANCELED_BUTTON_TEXT)
            )
            CANCELED -> mapOf(
                TaskOccurrenceMessageButtonType.DONE to doneButtonPayload.withText(DONE_BUTTON_TEXT),
                TaskOccurrenceMessageButtonType.CANCELED to canceledButtonPayload.withText(PRESSED_CANCELED_BUTTON_TEXT)
            )
            PENDING -> mapOf(
                TaskOccurrenceMessageButtonType.DONE to doneButtonPayload.withText(DONE_BUTTON_TEXT),
                TaskOccurrenceMessageButtonType.CANCELED to canceledButtonPayload.withText(CANCELED_BUTTON_TEXT)
            )
        }
    }

    @EventSourcingHandler
    fun on(event: TaskOccurrenceMessageStateChangedEvent) {
        state = event.state
        buttonPayloads = event.buttonPayloads
    }

    companion object {
        const val DONE_BUTTON_TEXT = "Done"
        const val PRESSED_DONE_BUTTON_TEXT = "Done ✅"
        const val CANCELED_BUTTON_TEXT = "Canceled"
        const val PRESSED_CANCELED_BUTTON_TEXT = "Canceled ❌"
    }
}
