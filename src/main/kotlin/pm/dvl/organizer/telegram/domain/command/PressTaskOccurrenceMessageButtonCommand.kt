package pm.dvl.organizer.telegram.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.telegram.domain.model.ButtonId
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageId

data class PressTaskOccurrenceMessageButtonCommand(
    @TargetAggregateIdentifier
    val taskOccurrenceMessageId: TaskOccurrenceMessageId,
    val buttonId: ButtonId
)
