package pm.dvl.organizer.telegram.domain.model

import java.util.UUID

data class ButtonId(val value: UUID)