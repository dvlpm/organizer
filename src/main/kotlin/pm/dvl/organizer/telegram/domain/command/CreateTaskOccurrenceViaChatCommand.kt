package pm.dvl.organizer.telegram.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.telegram.domain.model.ChatId

data class CreateTaskOccurrenceViaChatCommand(
    @TargetAggregateIdentifier
    val chatId: ChatId,
    val text: String
)
