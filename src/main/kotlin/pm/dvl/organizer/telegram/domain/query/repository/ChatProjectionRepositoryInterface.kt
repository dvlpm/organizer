package pm.dvl.organizer.telegram.domain.query.repository

import pm.dvl.organizer.common.RepositoryWithWaitForInterface
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.query.projection.ChatProjection
import pm.dvl.organizer.user.domain.model.UserId

interface ChatProjectionRepositoryInterface : RepositoryWithWaitForInterface {
    fun save(projection: ChatProjection)
    fun findOneOrNullById(id: ChatId): ChatProjection?
    suspend fun waitForFindOneOrNullByGlobalUserId(userId: UserId): ChatProjection? {
        return waitFor { findOneOrNullByGlobalUserId(userId) }
    }
    fun findOneOrNullByGlobalUserId(globalUserId: UserId): ChatProjection?
}
