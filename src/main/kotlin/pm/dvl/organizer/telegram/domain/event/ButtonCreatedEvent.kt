package pm.dvl.organizer.telegram.domain.event

import pm.dvl.organizer.telegram.domain.model.ButtonId

data class ButtonCreatedEvent(val buttonId: ButtonId)
