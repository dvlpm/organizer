package pm.dvl.organizer.telegram.domain.model

import pm.dvl.organizer.common.annotations.Persistable
import java.io.Serializable

@Persistable
data class ChatId(
    var value: Long
) : Serializable
