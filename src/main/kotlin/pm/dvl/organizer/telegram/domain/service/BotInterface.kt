package pm.dvl.organizer.telegram.domain.service

import pm.dvl.organizer.telegram.domain.model.ButtonPayload
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.model.MessageId

interface BotInterface {
    fun send(chatId: ChatId, text: String, buttonPayloads: List<ButtonPayload> = listOf()): MessageId
    fun updateButtons(chatId: ChatId, messageId: MessageId, buttonPayloads: List<ButtonPayload>)
}
