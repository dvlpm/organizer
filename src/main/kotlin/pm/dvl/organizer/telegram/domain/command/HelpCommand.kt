package pm.dvl.organizer.telegram.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.telegram.domain.model.ChatId

data class HelpCommand(
    @TargetAggregateIdentifier
    val chatId: ChatId,
)
