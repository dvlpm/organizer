package pm.dvl.organizer.telegram.domain.model

import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.modelling.saga.SagaEventHandler
import org.axonframework.modelling.saga.SagaLifecycle
import org.axonframework.modelling.saga.StartSaga
import org.axonframework.spring.stereotype.Saga
import org.springframework.beans.factory.annotation.Autowired
import pm.dvl.organizer.telegram.domain.command.CreateButtonCommand
import pm.dvl.organizer.telegram.domain.command.PressTaskMessageButtonCommand
import pm.dvl.organizer.telegram.domain.event.ButtonPressedEvent
import pm.dvl.organizer.telegram.domain.event.TaskMessageCreatedEvent

@Saga
class TaskMessageButtonsSaga {
    @Autowired
    @Transient
    private lateinit var commandGateway: CommandGateway

    private lateinit var taskMessageId: TaskMessageId

    @StartSaga
    @SagaEventHandler(associationProperty = "taskMessageId")
    fun on(event: TaskMessageCreatedEvent) {
        taskMessageId = event.taskMessageId

        event.buttonPayloads.map { (_, buttonPayload) ->
            SagaLifecycle.associateWith("buttonId", buttonPayload.buttonId.toString())

            commandGateway.send<Unit>(CreateButtonCommand(buttonPayload.buttonId))
        }
    }

    @SagaEventHandler(associationProperty = "buttonId")
    fun on(event: ButtonPressedEvent) {
        commandGateway.send<Unit>(
            PressTaskMessageButtonCommand(
                taskMessageId,
                event.buttonId
            )
        )
    }
}
