package pm.dvl.organizer.telegram.domain.model

import pm.dvl.organizer.common.annotations.Persistable
import java.util.*

@Persistable
data class TaskOccurrenceMessageId(var value: UUID) : java.io.Serializable
