package pm.dvl.organizer.telegram.domain.service

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskOccurrenceState

interface TaskOccurrenceServiceInterface {
    fun changeState(taskOccurrenceId: TaskOccurrenceId, taskOccurrenceState: TaskOccurrenceState)
}
