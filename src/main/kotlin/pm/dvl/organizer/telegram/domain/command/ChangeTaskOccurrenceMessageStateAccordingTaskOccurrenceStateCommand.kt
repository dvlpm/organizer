package pm.dvl.organizer.telegram.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskOccurrenceState
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageId

data class ChangeTaskOccurrenceMessageStateAccordingTaskOccurrenceStateCommand(
    @TargetAggregateIdentifier
    val taskOccurrenceMessageId: TaskOccurrenceMessageId,
    val taskOccurrenceState: TaskOccurrenceState
)
