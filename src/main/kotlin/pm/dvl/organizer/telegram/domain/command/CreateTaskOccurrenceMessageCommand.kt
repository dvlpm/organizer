package pm.dvl.organizer.telegram.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageId

data class CreateTaskOccurrenceMessageCommand(
    @TargetAggregateIdentifier
    val taskOccurrenceMessageId: TaskOccurrenceMessageId,
    val taskOccurrenceId: TaskOccurrenceId,
    val chatId: ChatId,
)
