package pm.dvl.organizer.telegram.infrastructure.input.dto

import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageId
import pm.dvl.organizer.user.domain.model.UserId

data class CreateTaskOccurrenceMessageDto(
    val assigneeUserId: UserId,
    val taskOccurrenceMessageId: TaskOccurrenceMessageId,
    val taskOccurrenceId: TaskOccurrenceId
)
