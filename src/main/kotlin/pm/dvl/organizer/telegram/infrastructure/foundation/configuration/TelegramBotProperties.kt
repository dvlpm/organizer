package pm.dvl.organizer.telegram.infrastructure.foundation.configuration

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("telegram.bot")
class TelegramBotProperties {
    lateinit var token: String
    lateinit var webhookHost: String

    val webhookUrl: String
        get() = "$webhookHost$WEBHOOK_PATH/$token"

    companion object {
        const val WEBHOOK_PATH = "/api/telegram/webhook"
    }
}
