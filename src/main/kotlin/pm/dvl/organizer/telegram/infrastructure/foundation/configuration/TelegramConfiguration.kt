package pm.dvl.organizer.telegram.infrastructure.foundation.configuration

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.BotCommand
import com.pengrad.telegrambot.request.SetMyCommands
import com.pengrad.telegrambot.request.SetWebhook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class TelegramConfiguration {
    @Autowired
    private lateinit var telegramBotProperties: TelegramBotProperties
    @Autowired
    private lateinit var telegramWebAppProperties: TelegramWebAppProperties

    @Bean
    fun telegramBot(): TelegramBot {
        return TelegramBot(telegramBotProperties.token).apply {
            execute(SetWebhook().url(telegramBotProperties.webhookUrl))
            execute(SetMyCommands(
                BotCommand("help", "Get help"),
            ))
        }
    }
}
