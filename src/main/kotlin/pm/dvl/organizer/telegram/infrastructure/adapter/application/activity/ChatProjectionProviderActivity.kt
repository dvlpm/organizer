package pm.dvl.organizer.telegram.infrastructure.adapter.application.activity

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pm.dvl.organizer.telegram.application.activity.ChatProjectionProviderActivityInterface
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.query.projection.ChatProjection
import pm.dvl.organizer.telegram.domain.query.repository.ChatProjectionRepositoryInterface

@Service
class ChatProjectionProviderActivity(
    @Autowired private val chatProjectionRepository: ChatProjectionRepositoryInterface
): ChatProjectionProviderActivityInterface {
    override fun provideById(id: ChatId): ChatProjection? {
        return chatProjectionRepository.findOneOrNullById(id)
    }
}
