package pm.dvl.organizer.telegram.infrastructure.input.eventlistener

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.infrastructure.output.eventproducer.event.TaskCreatedEvent
import pm.dvl.organizer.telegram.domain.command.CreateTaskMessageCommand
import pm.dvl.organizer.telegram.domain.model.TaskMessageId
import pm.dvl.organizer.telegram.domain.query.repository.ChatProjectionRepositoryInterface

@Component("pm.dvl.organizer.telegram.infrastructure.input.eventlistener.TaskCreatedEventListener")
class TaskCreatedEventListener(
    @Autowired private val chatProjectionRepository: ChatProjectionRepositoryInterface,
    @Autowired private val commandGateway: CommandGateway
) : ApplicationListener<TaskCreatedEvent> {
    override fun onApplicationEvent(event: TaskCreatedEvent) {
        if (event.taskSchedule === null) return

        val chatProjection =
            chatProjectionRepository.findOneOrNullByGlobalUserId(event.taskData.reporterUserId) ?: return

        commandGateway.send<Unit>(
            CreateTaskMessageCommand(
                TaskMessageId(event.taskId.value),
                event.taskId,
                chatProjection.id
            )
        )
    }
}
