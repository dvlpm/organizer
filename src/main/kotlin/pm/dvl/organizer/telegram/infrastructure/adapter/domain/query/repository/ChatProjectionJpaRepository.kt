package pm.dvl.organizer.telegram.infrastructure.adapter.domain.query.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.query.projection.ChatProjection
import pm.dvl.organizer.user.domain.model.UserId

@Repository
interface ChatProjectionJpaRepository
    : JpaRepository<ChatProjection, ChatId>
{
    fun findFirstByGlobalUserId(globalUserId: UserId): ChatProjection?
}
