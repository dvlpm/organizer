package pm.dvl.organizer.telegram.infrastructure.input.facade

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.telegram.domain.command.CreateTaskOccurrenceMessageCommand
import pm.dvl.organizer.telegram.domain.query.repository.ChatProjectionRepositoryInterface
import pm.dvl.organizer.telegram.domain.query.repository.TaskOccurrenceMessageProjectionRepositoryInterface
import pm.dvl.organizer.telegram.infrastructure.input.dto.CreateTaskOccurrenceMessageDto

@Component
class TaskOccurrenceMessageFacade(
    @Autowired private val chatProjectionRepository: ChatProjectionRepositoryInterface,
    @Autowired private val taskMessageProjectionRepository: TaskOccurrenceMessageProjectionRepositoryInterface,
    @Autowired private val commandGateway: CommandGateway
) {
    fun create(dto: CreateTaskOccurrenceMessageDto) {
        if (taskMessageProjectionRepository.findOneOrNullById(dto.taskOccurrenceMessageId) != null) return

        runBlocking {
            launch {
                createTaskMessage(dto)
            }
        }
    }

    suspend fun createTaskMessage(dto: CreateTaskOccurrenceMessageDto) {
        val chatProjection =
            chatProjectionRepository.waitForFindOneOrNullByGlobalUserId(dto.assigneeUserId) ?: return

        commandGateway.send<Unit>(
            CreateTaskOccurrenceMessageCommand(
                dto.taskOccurrenceMessageId,
                dto.taskOccurrenceId,
                chatProjection.id,
            )
        )
    }
}
