package pm.dvl.organizer.telegram.infrastructure.adapter.domain.query.repository

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageId
import pm.dvl.organizer.telegram.domain.query.projection.TaskOccurrenceMessageProjection
import pm.dvl.organizer.telegram.domain.query.repository.TaskOccurrenceMessageProjectionRepositoryInterface

@Component
class TaskOccurrenceMessageProjectionRepository(
    val jpaRepository: TaskOccurrenceMessageProjectionJpaRepository
) : TaskOccurrenceMessageProjectionRepositoryInterface {
    override fun save(projection: TaskOccurrenceMessageProjection) {
        jpaRepository.saveAndFlush(projection)
    }

    override fun findOneOrNullById(id: TaskOccurrenceMessageId): TaskOccurrenceMessageProjection? {
        return jpaRepository.findByIdOrNull(id)
    }

    override fun findAllByTaskId(taskOccurrenceId: TaskOccurrenceId): List<TaskOccurrenceMessageProjection> {
        return jpaRepository.findAllByTaskOccurrenceId(taskOccurrenceId)
    }
}
