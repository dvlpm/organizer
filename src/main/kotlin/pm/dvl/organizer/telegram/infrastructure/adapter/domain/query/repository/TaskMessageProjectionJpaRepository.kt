package pm.dvl.organizer.telegram.infrastructure.adapter.domain.query.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.telegram.domain.model.TaskMessageId
import pm.dvl.organizer.telegram.domain.query.projection.TaskMessageProjection

@Repository
interface TaskMessageProjectionJpaRepository : JpaRepository<TaskMessageProjection, TaskMessageId> {
    fun findAllByTaskId(taskId: TaskId): List<TaskMessageProjection>
}
