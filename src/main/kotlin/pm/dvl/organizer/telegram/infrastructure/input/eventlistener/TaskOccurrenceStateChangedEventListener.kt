package pm.dvl.organizer.telegram.infrastructure.input.eventlistener

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.infrastructure.output.eventproducer.event.TaskOccurrenceStateChangedEvent
import pm.dvl.organizer.telegram.domain.command.ChangeTaskOccurrenceMessageStateAccordingTaskOccurrenceStateCommand
import pm.dvl.organizer.telegram.domain.query.repository.TaskOccurrenceMessageProjectionRepositoryInterface

@Component("pm.dvl.organizer.telegram.infrastructure.input.eventlistener.TaskOccurrenceStateChangedEventListener")
class TaskOccurrenceStateChangedEventListener(
    @Autowired private val taskMessageProjectionRepository: TaskOccurrenceMessageProjectionRepositoryInterface,
    @Autowired private val commandGateway: CommandGateway
) : ApplicationListener<TaskOccurrenceStateChangedEvent> {
    override fun onApplicationEvent(event: TaskOccurrenceStateChangedEvent) {
        taskMessageProjectionRepository.findAllByTaskId(event.taskOccurrenceId).map { taskMessageProjection ->
            commandGateway.send<Unit>(
                ChangeTaskOccurrenceMessageStateAccordingTaskOccurrenceStateCommand(
                    taskMessageProjection.id, event.state
                )
            )
        }
    }
}
