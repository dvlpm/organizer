package pm.dvl.organizer.telegram.infrastructure.adapter.domain.query.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageId
import pm.dvl.organizer.telegram.domain.query.projection.TaskOccurrenceMessageProjection

@Repository
interface TaskOccurrenceMessageProjectionJpaRepository : JpaRepository<TaskOccurrenceMessageProjection, TaskOccurrenceMessageId> {
    fun findAllByTaskOccurrenceId(taskOccurrenceId: TaskOccurrenceId): List<TaskOccurrenceMessageProjection>
}
