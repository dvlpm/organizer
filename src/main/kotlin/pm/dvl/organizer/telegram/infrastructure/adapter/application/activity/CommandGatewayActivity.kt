package pm.dvl.organizer.telegram.infrastructure.adapter.application.activity

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pm.dvl.organizer.telegram.application.activity.CommandGatewayActivityInterface
import pm.dvl.organizer.telegram.domain.command.*

@Service
class CommandGatewayActivity(
    @Autowired val commandGateway: CommandGateway
) : CommandGatewayActivityInterface {
    override fun sendCreateChatCommand(command: CreateChatCommand) {
        commandGateway.send<Unit>(command)
    }

    override fun sendSetupChatTimeZoneCommand(command: SetupChatTimeZoneCommand) {
        commandGateway.send<Unit>(command)
    }

    override fun sendCreateTaskViaChatCommand(command: CreateTaskOccurrenceViaChatCommand) {
        commandGateway.send<Unit>(command)
    }

    override fun sendPressButtonCommand(command: PressButtonCommand) {
        commandGateway.send<Unit>(command)
    }

    override fun sendHelpCommand(command: HelpCommand) {
        commandGateway.send<Unit>(command)
    }

    override fun sendReactAccordingToModeCommand(command: ReactAccordingToModeCommand) {
        commandGateway.send<Unit>(command)
    }
}
