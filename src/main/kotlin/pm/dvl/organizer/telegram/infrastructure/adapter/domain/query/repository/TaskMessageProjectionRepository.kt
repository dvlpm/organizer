package pm.dvl.organizer.telegram.infrastructure.adapter.domain.query.repository

import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.telegram.domain.query.projection.TaskMessageProjection
import pm.dvl.organizer.telegram.domain.query.repository.TaskMessageProjectionRepositoryInterface

@Component
class TaskMessageProjectionRepository(
    val jpaRepository: TaskMessageProjectionJpaRepository
) : TaskMessageProjectionRepositoryInterface {
    override fun save(projection: TaskMessageProjection) {
        jpaRepository.saveAndFlush(projection)
    }

    override fun findAllByTaskId(taskId: TaskId): List<TaskMessageProjection> {
        return jpaRepository.findAllByTaskId(taskId)
    }
}
