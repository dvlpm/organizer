package pm.dvl.organizer.telegram.infrastructure.adapter.domain.query.repository

import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.query.projection.ChatProjection
import pm.dvl.organizer.telegram.domain.query.repository.ChatProjectionRepositoryInterface
import pm.dvl.organizer.user.domain.model.UserId

@Component
class ChatProjectionRepository(
    val jpaRepository: ChatProjectionJpaRepository
) : ChatProjectionRepositoryInterface {
    override val attempts: Int
        get() = 10
    override val delay: Long
        get() = 1000L

    override fun save(projection: ChatProjection) {
        jpaRepository.saveAndFlush(projection)
    }

    override fun findOneOrNullById(id: ChatId): ChatProjection? {
        return jpaRepository.findByIdOrNull(id)
    }

    override fun findOneOrNullByGlobalUserId(globalUserId: UserId): ChatProjection? {
        return jpaRepository.findFirstByGlobalUserId(globalUserId)
    }
}
