package pm.dvl.organizer.telegram.infrastructure.input.eventlistener

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.infrastructure.output.eventproducer.event.TaskStateChangedEvent
import pm.dvl.organizer.telegram.domain.command.ChangeTaskMessageStateAccordingTaskStateCommand
import pm.dvl.organizer.telegram.domain.query.repository.TaskMessageProjectionRepositoryInterface

@Component("pm.dvl.organizer.telegram.infrastructure.input.eventlistener.TaskStateChangedEventListener")
class TaskStateChangedEventListener(
    @Autowired private val taskMessageProjectionRepository: TaskMessageProjectionRepositoryInterface,
    @Autowired private val commandGateway: CommandGateway
) : ApplicationListener<TaskStateChangedEvent> {
    override fun onApplicationEvent(event: TaskStateChangedEvent) {
        taskMessageProjectionRepository.findAllByTaskId(event.taskId).map { taskMessageProjection ->
            commandGateway.send<Unit>(
                ChangeTaskMessageStateAccordingTaskStateCommand(
                    taskMessageProjection.id,
                    event.state
                )
            )
        }
    }
}
