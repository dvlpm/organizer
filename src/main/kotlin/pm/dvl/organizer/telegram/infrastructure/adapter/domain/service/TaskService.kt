package pm.dvl.organizer.telegram.infrastructure.adapter.domain.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskState
import pm.dvl.organizer.task.infrastructure.input.facade.dto.ChangeTaskStateDto
import pm.dvl.organizer.task.infrastructure.input.facade.dto.CreateTaskDto
import pm.dvl.organizer.task.infrastructure.input.facade.TaskFacade
import pm.dvl.organizer.telegram.domain.service.TaskServiceInterface

@Component("pm.dvl.organizer.telegram.infrastructure.adapter.domain.service.TaskService")
class TaskService(
    @Autowired private val taskFacade: TaskFacade
) : TaskServiceInterface {
    override fun create(taskId: TaskId, taskData: TaskData, schedule: Schedule?) {
        taskFacade.create(
            CreateTaskDto(
                taskId,
                taskData,
                schedule,
            )
        )
    }

    override fun changeState(taskId: TaskId, taskState: TaskState) {
        taskFacade.changeState(
            ChangeTaskStateDto(
                taskId, taskState
            )
        )
    }
}
