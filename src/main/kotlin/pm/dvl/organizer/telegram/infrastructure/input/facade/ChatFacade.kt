package pm.dvl.organizer.telegram.infrastructure.input.facade

import ai.applica.spring.boot.starter.temporal.WorkflowFactory
import com.pengrad.telegrambot.model.Update
import io.temporal.client.WorkflowClient
import io.temporal.client.WorkflowExecutionAlreadyStarted
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.common.extensions.makeStubWithWorkflowId
import pm.dvl.organizer.telegram.application.workflow.CallbackQueryWorkflow
import pm.dvl.organizer.telegram.application.workflow.CallbackQueryWorkflowInterface
import pm.dvl.organizer.telegram.application.workflow.ChatWorkflow
import pm.dvl.organizer.telegram.application.workflow.ChatWorkflowInterface
import pm.dvl.organizer.telegram.extensions.chatId
import pm.dvl.organizer.telegram.infrastructure.adapter.domain.service.Bot

@Component
class ChatFacade(
    @Autowired private val bot: Bot,
    @Autowired private val workflowFactory: WorkflowFactory,
    @Autowired private val workflowClient: WorkflowClient,
) {
    fun handle(update: Update, token: String?) {
        if (!bot.validateToken(token)) {
            return
        }

        val callbackQuery = update.callbackQuery()
        val message = update.message()

        when {
            callbackQuery != null -> {
                WorkflowClient.start {
                    workflowFactory.makeStubWithWorkflowId(
                        CallbackQueryWorkflowInterface::class,
                        CallbackQueryWorkflow::class,
                        update.callbackQueryWorkflowId
                    ).start(callbackQuery, update)
                }
            }
            message != null -> {
                try {
                    WorkflowClient.start {
                        workflowFactory.makeStubWithWorkflowId(
                            ChatWorkflowInterface::class,
                            ChatWorkflow::class,
                            update.chatWorkflowId
                        ).start(message, update)
                    }
                } catch (workflowExecutionAlreadyStarted: WorkflowExecutionAlreadyStarted) {
                    workflowClient.newWorkflowStub(
                        ChatWorkflowInterface::class.java,
                        update.chatWorkflowId
                    ).handle(message, update)
                }
            }
        }
    }

    val Update.chatWorkflowId: String
        get() = chatId.toString()

    val Update.callbackQueryWorkflowId: String
        get() = callbackQuery().id()
}
