package pm.dvl.organizer.telegram.infrastructure.foundation.configuration

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration

@Configuration
@EntityScan("pm.dvl.organizer.telegram.domain.query.projection")
class JpaTelegramConfiguration {}
