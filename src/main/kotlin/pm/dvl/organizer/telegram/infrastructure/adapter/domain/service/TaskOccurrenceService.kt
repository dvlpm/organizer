package pm.dvl.organizer.telegram.infrastructure.adapter.domain.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.model.TaskOccurrenceState
import pm.dvl.organizer.task.infrastructure.input.facade.TaskOccurrenceFacade
import pm.dvl.organizer.task.infrastructure.input.facade.dto.ChangeTaskOccurrenceStateDto
import pm.dvl.organizer.telegram.domain.service.TaskOccurrenceServiceInterface

@Component
class TaskOccurrenceService(
    @Autowired val taskOccurrenceFacade: TaskOccurrenceFacade
) : TaskOccurrenceServiceInterface {
    override fun changeState(taskOccurrenceId: TaskOccurrenceId, taskOccurrenceState: TaskOccurrenceState) {
        taskOccurrenceFacade.changeState(
            ChangeTaskOccurrenceStateDto(
                taskOccurrenceId,
                taskOccurrenceState
            )
        )
    }
}
