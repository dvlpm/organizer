package pm.dvl.organizer.telegram.infrastructure.input.eventlistener

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import pm.dvl.organizer.task.infrastructure.output.eventproducer.event.TaskOccurrenceStartedEvent
import pm.dvl.organizer.telegram.domain.model.TaskOccurrenceMessageId
import pm.dvl.organizer.telegram.infrastructure.input.dto.CreateTaskOccurrenceMessageDto
import pm.dvl.organizer.telegram.infrastructure.input.facade.TaskOccurrenceMessageFacade
import java.util.*

@Component("pm.dvl.organizer.telegram.infrastructure.input.eventlistener.TaskStartedEventListener")
class TaskOccurrenceStartedEventListener(
    @Autowired private val taskOccurrenceMessageFacade: TaskOccurrenceMessageFacade
) : ApplicationListener<TaskOccurrenceStartedEvent> {
    override fun onApplicationEvent(event: TaskOccurrenceStartedEvent) {
        taskOccurrenceMessageFacade.create(
            CreateTaskOccurrenceMessageDto(
                event.taskData.assigneeUserId ?: return,
                TaskOccurrenceMessageId(UUID.randomUUID()),
                event.taskOccurrenceId
            )
        )
    }
}
