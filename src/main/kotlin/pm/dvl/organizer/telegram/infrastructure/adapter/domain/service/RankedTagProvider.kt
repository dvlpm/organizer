package pm.dvl.organizer.telegram.infrastructure.adapter.domain.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.tag.domain.model.RankedTag
import pm.dvl.organizer.tag.domain.model.RankedTagName
import pm.dvl.organizer.tag.infrastructure.input.facade.TagFacade
import pm.dvl.organizer.tag.infrastructure.input.facade.payload.ProvideRankedTagByRankedTagNamePayload
import pm.dvl.organizer.telegram.domain.service.RankedTagProviderInterface
import pm.dvl.organizer.user.domain.model.UserId

@Component
class RankedTagProvider(
    @Autowired private val tagFacade: TagFacade
) : RankedTagProviderInterface {
    override fun provideByOwnerIdAndRankedTagNames(
        ownerId: UserId,
        vararg rankedTagNames: RankedTagName
    ): List<RankedTag> {
        return tagFacade.provideRankedTagByRankedTagName(ProvideRankedTagByRankedTagNamePayload(
            ownerId,
            rankedTagNames.toList()
        ))
    }
}
