package pm.dvl.organizer.telegram.infrastructure.input.controller

import com.pengrad.telegrambot.BotUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import pm.dvl.organizer.telegram.infrastructure.input.facade.ChatFacade
import pm.dvl.organizer.telegram.infrastructure.foundation.configuration.TelegramBotProperties

@RestController
class TelegramBotController(
    @Autowired private val chatFacade: ChatFacade
) {
    @PostMapping("${TelegramBotProperties.WEBHOOK_PATH}/{token}")
    fun webhook(
        @PathVariable("token")
        token: String?,
        @RequestBody
        request: String
    ): String {
        chatFacade.handle(BotUtils.parseUpdate(request), token)

        return "ok"
    }
}
