package pm.dvl.organizer.telegram.infrastructure.foundation.configuration

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("telegram.web-app")
class TelegramWebAppProperties {
    lateinit var token: String
    lateinit var webAppHost: String

    val webAppUrl: String
        get() = "$webAppHost${WEP_APP_PATH}"

    companion object {
        const val API_PATH = "/api/telegram/web-app"
        const val WEP_APP_PATH = "/telegram/web-app"
    }
}
