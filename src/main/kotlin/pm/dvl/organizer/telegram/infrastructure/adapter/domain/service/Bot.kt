package pm.dvl.organizer.telegram.infrastructure.adapter.domain.service

import com.pengrad.telegrambot.TelegramBot
import com.pengrad.telegrambot.model.WebAppInfo
import com.pengrad.telegrambot.model.request.*
import com.pengrad.telegrambot.request.EditMessageReplyMarkup
import com.pengrad.telegrambot.request.SendMessage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pm.dvl.organizer.telegram.domain.model.ButtonPayload
import pm.dvl.organizer.telegram.domain.model.ChatId
import pm.dvl.organizer.telegram.domain.model.MessageId
import pm.dvl.organizer.telegram.domain.service.BotInterface
import pm.dvl.organizer.telegram.infrastructure.foundation.configuration.TelegramWebAppProperties

@Service
class Bot(
    @Autowired private val properties: TelegramWebAppProperties,
    @Autowired private val telegramBot: TelegramBot
) : BotInterface {
    fun validateToken(token: String?): Boolean {
        return telegramBot.token.equals(token)
    }

    override fun send(chatId: ChatId, text: String, buttonPayloads: List<ButtonPayload>): MessageId {
        return telegramBot.send(chatId, text, buttonPayloads)
    }

    override fun updateButtons(chatId: ChatId, messageId: MessageId, buttonPayloads: List<ButtonPayload>) {
        telegramBot.updateButtons(chatId, messageId, buttonPayloads)
    }

    fun TelegramBot.send(
        chatId: ChatId,
        text: String,
        buttonPayloads: List<ButtonPayload> = listOf(),
    ): MessageId {
        val response = execute(SendMessage(chatId.value, text.escape()).apply {
            if (buttonPayloads.isNotEmpty()) {
                replyMarkup(InlineKeyboardMarkup(buttonPayloads.map {
                    InlineKeyboardButton(it.text).callbackData(
                        it.buttonId.value.toString()
                    )
                }.toTypedArray()))
            } else {
                replyMarkup(
                    ReplyKeyboardMarkup(
                        KeyboardButton("Open").webAppInfo(
                            WebAppInfo(this@Bot.properties.webAppUrl)
                        )
                    )
                )
            }
            parseMode(ParseMode.MarkdownV2)
        })

        return MessageId(response.message().messageId())
    }

    fun TelegramBot.updateButtons(chatId: ChatId, messageId: MessageId, buttonPayloads: List<ButtonPayload>) {
        execute(
            EditMessageReplyMarkup(
                chatId.value,
                messageId.value
            ).replyMarkup(InlineKeyboardMarkup(buttonPayloads.map {
                InlineKeyboardButton(it.text).callbackData(
                    it.buttonId.value.toString()
                )
            }.toTypedArray()))
        )
    }

    private fun String.escape(): String {
        return replace("(?<!\\\\)[_\\[\\]\\.(\\)\\~\\>\\#\\+\\-=\\|{\\}!]".toRegex()) { "\\${it.value}" }
    }
}
