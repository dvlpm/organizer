package pm.dvl.organizer.telegram.extensions

import com.pengrad.telegrambot.model.Update

val Update.chatId: Long
    get() = if (message() != null) {
        message().chat().id()
    } else {
        callbackQuery().message().chat().id()
    }
