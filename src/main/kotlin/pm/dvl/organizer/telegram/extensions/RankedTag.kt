package pm.dvl.organizer.telegram.extensions

import pm.dvl.organizer.tag.domain.model.RankedTag

fun Set<RankedTag>.display(): String {
    return joinToString(" ") { "#${it.tagName.value}" }
}
