package pm.dvl.organizer.telegram.extensions

import com.pengrad.telegrambot.model.Message

fun Message?.isOneOfCommand(vararg commands: String): Boolean {
    return commands.any { isCommand(it) }
}

private fun Message?.isCommand(command: String): Boolean {
    return this != null
            && text() != null
            && (text().startsWith("/$command") || text().startsWith(command))
}
