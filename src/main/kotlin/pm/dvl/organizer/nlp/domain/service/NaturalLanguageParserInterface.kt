package pm.dvl.organizer.nlp.domain.service

import java.util.TimeZone

interface NaturalLanguageParserInterface {
    fun parse(naturalLanguageString: String, timeZone: TimeZone): NaturalLanguageParserResult
}
