package pm.dvl.organizer.nlp.domain.service

import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.tag.domain.model.RankedTagName

data class NaturalLanguageParserResult(
    val text: String,
    val schedule: Schedule? = null,
    val tags: MutableList<RankedTagName> = mutableListOf()
)
