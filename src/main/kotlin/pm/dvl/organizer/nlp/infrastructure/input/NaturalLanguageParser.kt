package pm.dvl.organizer.nlp.infrastructure.input

import net.fortuna.ical4j.model.DateTime
import net.fortuna.ical4j.model.Recur
import net.fortuna.ical4j.model.component.VEvent
import net.fortuna.ical4j.model.property.DtStart
import net.fortuna.ical4j.model.property.RRule
import org.apache.commons.io.IOUtils
import org.springframework.stereotype.Service
import pm.dvl.organizer.common.extensions.*
import pm.dvl.organizer.nlp.domain.service.NaturalLanguageParserInterface
import pm.dvl.organizer.nlp.domain.service.NaturalLanguageParserResult
import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.tag.domain.model.RankedTagName
import pm.dvl.organizer.tag.domain.model.TagName
import java.nio.charset.StandardCharsets
import java.util.*

@Service
class NaturalLanguageParser : NaturalLanguageParserInterface {
    override fun parse(naturalLanguageString: String, timeZone: TimeZone): NaturalLanguageParserResult {
        val normalizedString = normalizeString(naturalLanguageString)

        return NaturalLanguageParserResult(
            normalizedString,
            parseSchedule(normalizedString, timeZone),
            parseTags(naturalLanguageString)
        )
    }

    fun normalizeString(naturalLanguageString: String): String {
        return "#(\\S+)".toRegex().replace(naturalLanguageString, "").trim()
    }

    fun parseTags(naturalLanguageString: String): MutableList<RankedTagName> {
        val tags = mutableListOf<RankedTagName>()

        "#(\\S+)".toRegex().findAll(naturalLanguageString).forEachIndexed { index, it ->
            RankedTagName(TagName(it.value.replace("#", "")), index.toString()).apply {
                if (tags.find { tag -> this.tagName == tag.tagName } !== null) return@apply

                tags.add(this)
            }
        }

        return tags
    }

    fun parseSchedule(naturalLanguageString: String, timeZone: TimeZone): Schedule? {
        val pwd = ProcessBuilder("pwd").exec()?.trim()
        val recurrentEventResult = ProcessBuilder(
            "python3",
            "${pwd}/src/main/python/RecurrentVEventParser.py",
            naturalLanguageString
        ).exec().toParserResult(naturalLanguageString, timeZone)

        if (recurrentEventResult.start == null) {
            return null
        }

        val event = VEvent(false)

        event.properties.add(DtStart(recurrentEventResult.start))

        if (recurrentEventResult.recur != null) {
            event.properties.add(RRule(recurrentEventResult.recur))
        }

        return Schedule(event.toCalendar(timeZone))
    }

    fun ProcessBuilder.exec(): String? {
        return try {
            redirectErrorStream(true)
            IOUtils.toString(this.start().inputStream, StandardCharsets.UTF_8)
        } catch (exception: Exception) {
            null
        }
    }

    fun String?.toParserResult(naturalLanguageString: String, timeZone: TimeZone): RecurrentEventParserResult {
        if (this == null || contains("None\n")) return RecurrentEventParserResult(null, null)

        var start = DateTime().shiftToOffset(timeZone.toZoneOffset())
        var recur: Recur? = null

        split("\\n").map {
            if (contains("RRULE:")) {
                recur = Recur(it.replace("RRULE:", "").trim())
            } else {
                start = it.trim().toDateTime()
                start = if (!naturalLanguageString.hasExactTime()) {
                    start.shiftToOffset(timeZone.toZoneOffset())
                } else {
                    start.normalizeDateInTimeZone(timeZone)
                }
            }
        }

        return RecurrentEventParserResult(start, recur)
    }

    private fun String.hasExactTime(): Boolean {
        return contains(NUMBERED_WITH_MINUTES_TIME_PATTERN.toRegex())
                || contains("${NUMBERED_TIME_PATTERN}${ANTE_OR_POST_MERIDIEM_PATTERN}".toRegex())
                || contains("$NUMBERED_TIME_PATTERN $ANTE_OR_POST_MERIDIEM_PATTERN".toRegex())
                || contains("$AT_PATTERN $NUMBERED_TIME_PATTERN".toRegex())
    }

    private fun DateTime.normalizeDateInTimeZone(timeZone: TimeZone): DateTime {
        return shiftToDays(DateTime().shiftToOffset(timeZone.toZoneOffset()).date - date)
    }

    data class RecurrentEventParserResult(val start: DateTime?, val recur: Recur? = null)

    companion object {
        const val NUMBERED_WITH_MINUTES_TIME_PATTERN = "\\d:\\d\\d"
        const val NUMBERED_TIME_PATTERN = "\\d"
        const val AT_PATTERN = "at"
        const val ANTE_OR_POST_MERIDIEM_PATTERN = "pm|am"
    }
}
