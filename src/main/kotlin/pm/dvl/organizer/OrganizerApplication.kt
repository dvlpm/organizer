package pm.dvl.organizer

import ai.applica.spring.boot.starter.temporal.annotations.EnableTemporal
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import pm.dvl.organizer.calendar.application.workflow.properties.CalendarWorkflowProperties
import pm.dvl.organizer.telegram.infrastructure.foundation.configuration.TelegramBotProperties
import pm.dvl.organizer.telegram.infrastructure.foundation.configuration.TelegramWebAppProperties
import java.util.TimeZone
import javax.annotation.PostConstruct

@EnableTemporal
@SpringBootApplication
@EnableConfigurationProperties(CalendarWorkflowProperties::class, TelegramBotProperties::class, TelegramWebAppProperties::class)
class OrganizerApplication

fun main(args: Array<String>) {
    runApplication<OrganizerApplication>(*args)
}

@PostConstruct
fun init() {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
}
