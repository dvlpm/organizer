package pm.dvl.organizer.tag.infrastructure.input.facade.result

import pm.dvl.organizer.tag.domain.model.TagId
import pm.dvl.organizer.tag.domain.model.TagName

data class FindAllOrCreateTagResult(
    val tagNamesToIds: Map<TagName, TagId>
)
