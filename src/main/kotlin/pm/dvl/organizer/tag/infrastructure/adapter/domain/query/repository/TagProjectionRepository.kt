package pm.dvl.organizer.tag.infrastructure.adapter.domain.query.repository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import pm.dvl.organizer.tag.domain.model.TagId
import pm.dvl.organizer.tag.domain.model.TagName
import pm.dvl.organizer.tag.domain.query.projection.TagProjection
import pm.dvl.organizer.tag.domain.query.repository.TagProjectionRepositoryInterface
import pm.dvl.organizer.user.domain.model.UserId

@Component
class TagProjectionRepository(
    @Autowired private val jpaRepository: TagProjectionJpaRepository
) : TagProjectionRepositoryInterface {
    override fun save(projection: TagProjection) {
        jpaRepository.saveAndFlush(projection)
    }

    override fun findOneOrNullById(id: TagId): TagProjection? {
        return jpaRepository.findByIdOrNull(id)
    }

    override fun findAllByOwnerId(ownerId: UserId): List<TagProjection> {
        return jpaRepository.findAllByOwnerId(ownerId)
    }

    override fun findAllByIds(vararg tagIds: TagId): List<TagProjection> {
        return jpaRepository.findAllById(tagIds.toList())
    }

    override fun findAllByOwnerIdAndNames(ownerId: UserId, vararg tagNames: TagName): List<TagProjection> {
        return jpaRepository.findAllByOwnerIdAndNameIn(ownerId, tagNames.toList())
    }
}
