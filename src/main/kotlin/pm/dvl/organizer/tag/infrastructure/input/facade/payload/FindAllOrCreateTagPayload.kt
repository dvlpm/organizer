package pm.dvl.organizer.tag.infrastructure.input.facade.payload

import pm.dvl.organizer.tag.domain.model.TagName
import pm.dvl.organizer.user.domain.model.UserId

data class FindAllOrCreateTagPayload(
    val ownerId: UserId,
    val tagNames: List<TagName>
)
