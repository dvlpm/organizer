package pm.dvl.organizer.tag.infrastructure.input.facade

import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.tag.domain.command.CreateTagCommand
import pm.dvl.organizer.tag.domain.model.RankedTag
import pm.dvl.organizer.tag.domain.model.TagId
import pm.dvl.organizer.tag.domain.model.TagName
import pm.dvl.organizer.tag.domain.query.repository.TagProjectionRepositoryInterface
import pm.dvl.organizer.tag.infrastructure.input.facade.payload.FindAllOrCreateTagPayload
import pm.dvl.organizer.tag.infrastructure.input.facade.payload.ProvideRankedTagByRankedTagNamePayload
import pm.dvl.organizer.tag.infrastructure.input.facade.result.FindAllOrCreateTagResult
import java.util.*

@Component
class TagFacade(
    @Autowired private val commandGateway: CommandGateway,
    @Autowired private val tagProjectionRepository: TagProjectionRepositoryInterface
) {
    fun findAllOrCreate(payload: FindAllOrCreateTagPayload): FindAllOrCreateTagResult {
        val resultingTagNamesToIds = mutableMapOf<TagName, TagId>()
        val existingTags = tagProjectionRepository.findAllByOwnerIdAndNames(
            payload.ownerId,
            *payload.tagNames.toTypedArray()
        )

        payload.tagNames.forEach { tagName ->
            val existingTag = existingTags.find { tagProjection -> tagProjection.name == tagName }
            if (existingTag != null) {
                resultingTagNamesToIds[tagName] = existingTag.id
                return@forEach
            }

            val newTagId = TagId(UUID.randomUUID())

            commandGateway.sendAndWait<Unit>(CreateTagCommand(
                newTagId,
                tagName,
                payload.ownerId,
            ))

            resultingTagNamesToIds[tagName] = newTagId
        }

        return FindAllOrCreateTagResult(resultingTagNamesToIds)
    }

    fun provideRankedTagByRankedTagName(payload: ProvideRankedTagByRankedTagNamePayload): List<RankedTag> {
        val tagNamesToIds = findAllOrCreate(FindAllOrCreateTagPayload(
            payload.ownerId,
            payload.rankedTagNames.map { it.tagName }
        )).tagNamesToIds

        return payload.rankedTagNames.mapNotNull { rankedTagName ->
            tagNamesToIds[rankedTagName.tagName]?.let { tagId ->
                RankedTag(tagId, rankedTagName.tagName, rankedTagName.rank)
            }
        }
    }
}
