package pm.dvl.organizer.tag.infrastructure.adapter.domain.query.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pm.dvl.organizer.tag.domain.model.TagId
import pm.dvl.organizer.tag.domain.model.TagName
import pm.dvl.organizer.tag.domain.query.projection.TagProjection
import pm.dvl.organizer.user.domain.model.UserId

@Repository
interface TagProjectionJpaRepository : JpaRepository<TagProjection, TagId>
{
    fun findAllByOwnerId(ownerId: UserId): List<TagProjection>
    fun findAllByOwnerIdAndNameIn(ownerId: UserId, names: List<TagName>): List<TagProjection>
}
