package pm.dvl.organizer.tag.infrastructure.input.facade.payload

import pm.dvl.organizer.tag.domain.model.RankedTagName
import pm.dvl.organizer.user.domain.model.UserId

data class ProvideRankedTagByRankedTagNamePayload(
    val ownerId: UserId,
    val rankedTagNames: List<RankedTagName>
)
