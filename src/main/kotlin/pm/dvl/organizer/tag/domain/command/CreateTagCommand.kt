package pm.dvl.organizer.tag.domain.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import pm.dvl.organizer.tag.domain.model.TagId
import pm.dvl.organizer.tag.domain.model.TagName
import pm.dvl.organizer.user.domain.model.UserId

data class CreateTagCommand(
    @TargetAggregateIdentifier
    val tagId: TagId,
    val tagName: TagName,
    val tagOwnerId: UserId,
    val tagParentIds: MutableList<TagId> = mutableListOf()
)
