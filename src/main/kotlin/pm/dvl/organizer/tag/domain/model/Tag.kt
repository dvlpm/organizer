package pm.dvl.organizer.tag.domain.model

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.extensions.kotlin.applyEvent
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.spring.stereotype.Aggregate
import pm.dvl.organizer.tag.domain.command.AddParentToTagCommand
import pm.dvl.organizer.tag.domain.command.CreateTagCommand
import pm.dvl.organizer.tag.domain.command.RemoveParentFromTagCommand
import pm.dvl.organizer.tag.domain.event.ParentAddedToTagEvent
import pm.dvl.organizer.tag.domain.event.ParentRemovedFromTagEvent
import pm.dvl.organizer.tag.domain.event.TagCreatedEvent
import pm.dvl.organizer.user.domain.model.UserId

@Aggregate(snapshotTriggerDefinition = "aggregateSnapshotTriggerDefinition")
class Tag() {
    @AggregateIdentifier
    private lateinit var id: TagId
    private lateinit var name: TagName
    private lateinit var ownerId: UserId
    private lateinit var parents: MutableList<TagId>

    @CommandHandler
    constructor(command: CreateTagCommand): this() {
        applyEvent(TagCreatedEvent(
            command.tagId,
            command.tagName,
            command.tagOwnerId,
            command.tagParentIds
        ))
    }

    @EventSourcingHandler
    fun on(event: TagCreatedEvent) {
        this.id = event.tagId
        this.name = event.tagName
        this.ownerId = event.tagOwnerId
        this.parents = event.tagParentIds
    }

    @CommandHandler
    fun addParent(command: AddParentToTagCommand) {
        if (id === command.tagParentId || parents.contains(command.tagParentId)) return

        applyEvent(ParentAddedToTagEvent(
            id,
            command.tagParentId
        ))
    }

    @EventSourcingHandler
    fun on(event: ParentAddedToTagEvent) {
        parents.add(event.tagParentId)
    }

    @CommandHandler
    fun removeParent(command: RemoveParentFromTagCommand) {
        applyEvent(ParentRemovedFromTagEvent(
            id,
            command.tagParentId
        ))
    }

    @EventSourcingHandler
    fun on(event: ParentRemovedFromTagEvent) {
        parents.remove(event.tagParentId)
    }
}
