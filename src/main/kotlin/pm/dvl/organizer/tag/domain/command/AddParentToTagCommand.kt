package pm.dvl.organizer.tag.domain.command

import pm.dvl.organizer.tag.domain.model.TagId

data class AddParentToTagCommand(
    val tagId: TagId,
    val tagParentId: TagId
)
