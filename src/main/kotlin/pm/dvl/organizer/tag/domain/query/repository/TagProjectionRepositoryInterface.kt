package pm.dvl.organizer.tag.domain.query.repository

import pm.dvl.organizer.tag.domain.model.TagId
import pm.dvl.organizer.tag.domain.model.TagName
import pm.dvl.organizer.tag.domain.query.projection.TagProjection
import pm.dvl.organizer.user.domain.model.UserId

interface TagProjectionRepositoryInterface {
    fun save(projection: TagProjection)
    fun findOneOrNullById(id: TagId): TagProjection?
    fun findAllByOwnerId(ownerId: UserId): List<TagProjection>
    fun findAllByIds(vararg tagIds: TagId): List<TagProjection>
    fun findAllByOwnerIdAndNames(ownerId: UserId, vararg tagNames: TagName): List<TagProjection>
}
