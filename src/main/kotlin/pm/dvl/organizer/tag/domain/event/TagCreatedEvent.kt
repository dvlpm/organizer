package pm.dvl.organizer.tag.domain.event

import pm.dvl.organizer.tag.domain.model.TagId
import pm.dvl.organizer.tag.domain.model.TagName
import pm.dvl.organizer.user.domain.model.UserId

data class TagCreatedEvent(
    val tagId: TagId,
    val tagName: TagName,
    val tagOwnerId: UserId,
    val tagParentIds: MutableList<TagId>
)
