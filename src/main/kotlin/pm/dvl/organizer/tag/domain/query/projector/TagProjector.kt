package pm.dvl.organizer.tag.domain.query.projector

import org.axonframework.eventhandling.EventHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pm.dvl.organizer.tag.domain.event.ParentAddedToTagEvent
import pm.dvl.organizer.tag.domain.event.ParentRemovedFromTagEvent
import pm.dvl.organizer.tag.domain.event.TagCreatedEvent
import pm.dvl.organizer.tag.domain.query.projection.TagProjection
import pm.dvl.organizer.tag.domain.query.repository.TagProjectionRepositoryInterface

@Component
class TagProjector(
    @Autowired private val repository: TagProjectionRepositoryInterface
) {
    @EventHandler
    fun on(event: TagCreatedEvent) {
        val projection = TagProjection(
            event.tagId,
            event.tagName,
            event.tagOwnerId
        )

        // TODO make same thing (findAll) in other projectors with parents
        repository.findAllByIds(*event.tagParentIds.toTypedArray()).forEach {
            projection.addParent(it)
        }

        repository.save(projection)
    }

    @EventHandler
    fun on(event: ParentAddedToTagEvent) {
        val projection = repository.findOneOrNullById(event.tagId) ?: return
        val parentProjection = repository.findOneOrNullById(event.tagParentId) ?: return

        projection.addParent(parentProjection)

        repository.save(projection)
    }

    @EventHandler
    fun on(event: ParentRemovedFromTagEvent) {
        val projection = repository.findOneOrNullById(event.tagId) ?: return
        val parentProjection = repository.findOneOrNullById(event.tagParentId) ?: return

        projection.removeParent(parentProjection)

        repository.save(projection)
    }
}
