package pm.dvl.organizer.tag.domain.command

import pm.dvl.organizer.tag.domain.model.TagId

data class RemoveParentFromTagCommand(
    val tagId: TagId,
    val tagParentId: TagId
)
