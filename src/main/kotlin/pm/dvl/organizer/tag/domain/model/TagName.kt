package pm.dvl.organizer.tag.domain.model

import pm.dvl.organizer.common.annotations.Persistable
import java.io.Serializable

@Persistable
data class TagName(var value: String) : Serializable
