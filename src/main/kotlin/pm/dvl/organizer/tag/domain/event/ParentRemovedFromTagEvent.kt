package pm.dvl.organizer.tag.domain.event

import pm.dvl.organizer.tag.domain.model.TagId

data class ParentRemovedFromTagEvent(
    val tagId: TagId,
    val tagParentId: TagId
)
