package pm.dvl.organizer.tag.domain.query.projection

import com.fasterxml.jackson.annotation.JsonIgnore
import pm.dvl.organizer.common.annotations.Persistable
import pm.dvl.organizer.tag.domain.model.TagId
import pm.dvl.organizer.tag.domain.model.TagName
import pm.dvl.organizer.user.domain.model.UserId

@Persistable
class TagProjection(
    var id: TagId,
    var name: TagName,
    var ownerId: UserId,
    @JsonIgnore
    var _parents: MutableSet<TagId>? = mutableSetOf(),
    @JsonIgnore
    var _children: MutableSet<TagId>? = mutableSetOf(),
) {
    val parents: MutableSet<TagId>
        get() = _parents ?: mutableSetOf()

    val children: MutableSet<TagId>
        get() = _children ?: mutableSetOf()

    fun addParent(tagProjection: TagProjection): TagProjection {
        return apply {
            _parents = parents.apply { add(tagProjection.id) }
            tagProjection._children = tagProjection.children.apply { add(this@TagProjection.id) }
        }
    }

    fun removeParent(tagProjection: TagProjection): TagProjection {
        return apply {
            _parents = parents.apply { remove(tagProjection.id) }
            tagProjection._children = tagProjection.children.apply { remove(this@TagProjection.id) }
        }
    }
}
