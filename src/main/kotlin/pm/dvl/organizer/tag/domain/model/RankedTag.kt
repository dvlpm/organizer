package pm.dvl.organizer.tag.domain.model

import pm.dvl.organizer.common.annotations.Persistable
import java.io.Serializable

@Persistable
data class RankedTag(
    var tagId: TagId,
    var tagName: TagName,
    var rank: String
) : Serializable
