package pm.dvl.organizer.tag.domain.model

import pm.dvl.organizer.common.annotations.Persistable
import java.io.Serializable
import java.util.*

@Persistable
data class TagId(var value: UUID): Serializable
