package pm.dvl.organizer.tag.domain.model

data class RankedTagName(
    val tagName: TagName,
    val rank: String
)
