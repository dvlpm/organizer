package pm.dvl.organizer.user.domain.model

import pm.dvl.organizer.common.annotations.Persistable
import java.util.*
import java.io.*

@Persistable
data class UserId(
    var value: UUID
) : Serializable
