package pm.dvl.organizer.schedule.domain.model

import net.fortuna.ical4j.model.Calendar

data class Schedule(
    val calendar: Calendar
)
