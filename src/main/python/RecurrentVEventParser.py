import sys
from recurrent.event_parser import RecurringEvent

r = RecurringEvent(preferred_time_range=(0, 24))
print(r.parse(" ".join(sys.argv[1:])))
