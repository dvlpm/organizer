create table if not exists chat
(
    id             bigint not null
        primary key,
    global_user_id uuid,
    time_zone      varchar(255)
);

create table if not exists tag
(
    id                uuid not null
    primary key,
    name text,
    owner_id  uuid
);

create table if not exists tag_relation
(
    child_tag_id uuid not null references tag(id),
    parent_tag_id uuid not null references tag(id),
    primary key (child_tag_id, parent_tag_id)
    );


create table if not exists task
(
    id                uuid not null
        primary key,
    state             varchar(255),
    summary text,
    reporter_user_id  uuid,
    assignee_user_id  uuid
);

create table if not exists task_occurrence_ranked_tags
(
    task_occurrence_id uuid not null references task(id),
    tag_id uuid not null references tag(id),
    tag_name text not null,
    tag_rank text not null
);

create table if not exists task_occurrence_relation
(
    child_task_occurrence_id uuid not null references task(id),
    parent_task_occurrence_id uuid not null references task(id),
    primary key (child_task_occurrence_id, parent_task_occurrence_id)
);

create table if not exists task
(
    id                         uuid not null
        primary key,
    state                      varchar(255),
    summary          text,
    reporter_user_id uuid,
    assignee_user_id uuid
);

create table if not exists task_ranked_tags
(
    task_id uuid not null references task(id),
    tag_id uuid not null references tag(id),
    tag_name text not null,
    tag_rank text not null
);

create table if not exists task_relation
(
    child_task_id uuid not null references task(id),
    parent_task_id uuid not null references task(id),
    primary key (child_task_id, parent_task_id)
);

create table if not exists task_occurrence_produced_by_task
(
    task_occurrence_id                         uuid not null
        primary key,
    task_id                    uuid not null
);

create table if not exists task_message
(
    id             uuid not null
    primary key,
    task_id uuid
);

create table if not exists task_occurrence_message
(
    id                uuid not null
    primary key,
    task_occurrence_id uuid
);
