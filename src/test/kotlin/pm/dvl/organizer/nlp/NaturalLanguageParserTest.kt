package pm.dvl.organizer.nlp

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import pm.dvl.organizer.common.extensions.format
import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.nlp.infrastructure.input.NaturalLanguageParser
import pm.dvl.organizer.tag.domain.model.RankedTagName
import pm.dvl.organizer.tag.domain.model.TagName
import java.time.LocalDateTime
import java.util.*

@SpringBootTest
class NaturalLanguageParserTest {
    @Autowired
    private lateinit var naturalLanguageParser: NaturalLanguageParser

    @ParameterizedTest
    @MethodSource("provideTestData")
    fun test(
        naturalLanguageString: String,
        timeZone: TimeZone,
        expectedText: String,
        expectedCalendarRegex: Regex,
        expectedTags: MutableList<RankedTagName> = mutableListOf()
    ) {
        val naturalLanguageParserResult = naturalLanguageParser.parse(naturalLanguageString, timeZone)
        val calendarString = naturalLanguageParserResult.schedule?.calendar.toString().trimIndent()

        Assertions.assertEquals(expectedText, naturalLanguageParserResult.text)
        Assertions.assertInstanceOf(Schedule::class.java, naturalLanguageParserResult.schedule)
        Assertions.assertTrue(calendarString.contains(expectedCalendarRegex))
        Assertions.assertEquals(expectedTags, naturalLanguageParserResult.tags)
    }

    companion object {
        @JvmStatic
        fun provideTestData(): List<Arguments> {
            val emptyTagList = mutableListOf<RankedTagName>()

            return listOf(
                Arguments.of(
                    "go to smoke at 01:47 #habits",
                    TimeZone.getTimeZone("GMT+4"),
                    "go to smoke at 01:47",
                    """
                        BEGIN:VCALENDAR
                        BEGIN:VEVENT
                        DTSTART:${LocalDateTime.now().format("yyyyMMdd")}T014700
                        END:VEVENT
                        BEGIN:VTIMEZONE
                        TZOFFSETTO:\+0400
                        TZNAME:GMT\+04:00
                        TZID:GMT\+04:00
                        END:VTIMEZONE
                        END:VCALENDAR
                    """.trimIndent().toRegex(),
                    mutableListOf(RankedTagName(TagName("habits"), "0"))
                ),
                Arguments.of(
                    "go to smoke at 0:25 #duplicate #duplicate",
                    TimeZone.getTimeZone("GMT+4"),
                    "go to smoke at 0:25",
                    """
                        BEGIN:VCALENDAR
                        BEGIN:VEVENT
                        DTSTART:${LocalDateTime.now().format("yyyyMMdd")}T002500
                        END:VEVENT
                        BEGIN:VTIMEZONE
                        TZOFFSETTO:\+0400
                        TZNAME:GMT\+04:00
                        TZID:GMT\+04:00
                        END:VTIMEZONE
                        END:VCALENDAR
                    """.trimIndent().toRegex(),
                    mutableListOf(RankedTagName(TagName("duplicate"), "0"))
                ),
                Arguments.of(
                    "go to smoke in 5 minutes",
                    TimeZone.getTimeZone("GMT+4"),
                    "go to smoke in 5 minutes",
                    """
                        BEGIN:VCALENDAR
                        BEGIN:VEVENT
                        DTSTART:(.*)T${LocalDateTime.now().plusHours(4).plusMinutes(5).format("HHmm")}(.*)
                        END:VEVENT
                        BEGIN:VTIMEZONE
                        TZOFFSETTO:\+0400
                        TZNAME:GMT\+04:00
                        TZID:GMT\+04:00
                        END:VTIMEZONE
                        END:VCALENDAR
                    """.trimIndent().toRegex(),
                    emptyTagList
                ),
                Arguments.of(
                    "go to smoke in a one minute",
                    TimeZone.getTimeZone("UTC"),
                    "go to smoke in a one minute",
                    """
                        BEGIN:VCALENDAR
                        BEGIN:VEVENT
                        DTSTART:(.*)T${LocalDateTime.now().plusMinutes(1).format("HHmm")}(.*)
                        END:VEVENT
                        BEGIN:VTIMEZONE
                        TZOFFSETTO:\+0000
                        TZNAME:Coordinated Universal Time
                        TZID:UTC
                        END:VTIMEZONE
                        END:VCALENDAR
                    """.trimIndent().toRegex(),
                    emptyTagList
                ),
                Arguments.of(
                    "wake up everyday at 6 am",
                    TimeZone.getTimeZone("GMT+4"),
                    "wake up everyday at 6 am",
                    """
                        BEGIN:VCALENDAR
                        BEGIN:VEVENT
                        DTSTART:(.*)
                        RRULE:FREQ=DAILY;INTERVAL=1;BYHOUR=6;BYMINUTE=0
                        END:VEVENT
                        BEGIN:VTIMEZONE
                        TZOFFSETTO:\+0400
                        TZNAME:GMT\+04:00
                        TZID:GMT\+04:00
                        END:VTIMEZONE
                        END:VCALENDAR
                    """.trimIndent().toRegex(),
                    emptyTagList
                ),
                Arguments.of(
                    "go to gym every weekday at 8 pm",
                    TimeZone.getTimeZone("GMT-3"),
                    "go to gym every weekday at 8 pm",
                    """
                        BEGIN:VCALENDAR
                        BEGIN:VEVENT
                        DTSTART:(.*)
                        RRULE:FREQ=WEEKLY;INTERVAL=1;BYDAY=MO,TU,WE,TH,FR;BYHOUR=20;BYMINUTE=0
                        END:VEVENT
                        BEGIN:VTIMEZONE
                        TZOFFSETTO:-0300
                        TZNAME:GMT-03:00
                        TZID:GMT-03:00
                        END:VTIMEZONE
                        END:VCALENDAR
                    """.trimIndent().toRegex(),
                    emptyTagList
                ),
                Arguments.of(
                    "go to smoke at 19:25",
                    TimeZone.getTimeZone("UTC"),
                    "go to smoke at 19:25",
                    """
                        BEGIN:VCALENDAR
                        BEGIN:VEVENT
                        DTSTART:(.*)T192500
                        END:VEVENT
                        BEGIN:VTIMEZONE
                        TZOFFSETTO:\+0000
                        TZNAME:Coordinated Universal Time
                        TZID:UTC
                        END:VTIMEZONE
                        END:VCALENDAR
                    """.trimIndent().toRegex(),
                    emptyTagList
                ),
                Arguments.of(
                    "go to smoke 19:25",
                    TimeZone.getTimeZone("UTC"),
                    "go to smoke 19:25",
                    """
                        BEGIN:VCALENDAR
                        BEGIN:VEVENT
                        DTSTART:(.*)T192500
                        END:VEVENT
                        BEGIN:VTIMEZONE
                        TZOFFSETTO:\+0000
                        TZNAME:Coordinated Universal Time
                        TZID:UTC
                        END:VTIMEZONE
                        END:VCALENDAR
                    """.trimIndent().toRegex(),
                    emptyTagList
                ),
                Arguments.of(
                    "go to smoke 8pm",
                    TimeZone.getTimeZone("UTC"),
                    "go to smoke 8pm",
                    """
                        BEGIN:VCALENDAR
                        BEGIN:VEVENT
                        DTSTART:(.*)T200000
                        END:VEVENT
                        BEGIN:VTIMEZONE
                        TZOFFSETTO:\+0000
                        TZNAME:Coordinated Universal Time
                        TZID:UTC
                        END:VTIMEZONE
                        END:VCALENDAR
                    """.trimIndent().toRegex(),
                    emptyTagList
                ),
                // TODO Arguments.of("go to gym every weekday from jan 1 2010 at 8 am"),
            )
        }
    }
}
