package pm.dvl.organizer.calendar.acceptance

import ai.applica.spring.boot.starter.temporal.WorkflowFactory
import com.fasterxml.jackson.databind.ObjectMapper
import io.temporal.client.WorkflowClient
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import pm.dvl.organizer.calendar.application.workflow.CalendarWorkflow
import pm.dvl.organizer.calendar.application.workflow.CalendarWorkflowInterface
import pm.dvl.organizer.calendar.application.workflow.properties.CalendarWorkflowProperties
import pm.dvl.organizer.calendar.domain.model.Calendar
import pm.dvl.organizer.calendar.domain.model.CalendarId
import pm.dvl.organizer.calendar.domain.model.EventOccurrence
import pm.dvl.organizer.calendar.domain.service.EventOccurrenceSchedulerInterface
import pm.dvl.organizer.common.extensions.toPeriodTo
import pm.dvl.organizer.nlp.infrastructure.input.NaturalLanguageParser
import java.time.Duration
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

@SpringBootTest
class CalendarAcceptanceTest {
    @Autowired
    private lateinit var workflowFactory: WorkflowFactory

    @Autowired
    private lateinit var calendarWorkflowProperties: CalendarWorkflowProperties

    @Autowired
    private lateinit var naturalLanguageParser: NaturalLanguageParser

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Test
    fun `test start workflow`() {
        val workflow = workflowFactory.makeStub(
            CalendarWorkflowInterface::class.java,
            CalendarWorkflow::class.java
        )

        val workflowExecution = WorkflowClient.start {
            workflow.start(
                newCalendar(),
                calendarWorkflowProperties
            )
        }

        Assertions.assertNotNull(workflowExecution.workflowId)
    }

    @Test
    fun `test schedule`() {
        val now = ZonedDateTime.now()
        val periodEnd = now.plus(Duration.parse("PT168H"))

        newCalendar().scheduleEventOccurrences(
            object : EventOccurrenceSchedulerInterface {
                override fun schedule(eventOccurrence: EventOccurrence) {
                    Assertions.assertNotNull(eventOccurrence)
                }

                override fun disable(eventOccurrence: EventOccurrence) {
                    TODO("Not yet implemented")
                }

                override fun enable(eventOccurrence: EventOccurrence) {
                    TODO("Not yet implemented")
                }
            },
            now.toPeriodTo(periodEnd)
        )
    }

    @Test
    fun `test serialize`() {
        val calendar = newCalendar()
        val serializedCalendar = objectMapper.writeValueAsString(calendar)
        val deserializedCalendar = objectMapper.readValue(serializedCalendar, Calendar::class.java)

        val serializedAgainCalendar = objectMapper.writeValueAsString(deserializedCalendar)
        val deserializedAgainCalendar = objectMapper.readValue(serializedAgainCalendar, Calendar::class.java)

        Assertions.assertEquals(calendar, deserializedAgainCalendar)
    }

    private fun newCalendar(): Calendar {
        val zoneId = ZoneId.of("GMT+4")
        val nlpResult = naturalLanguageParser.parse("every weekday at 10:00", TimeZone.getTimeZone(zoneId))

        return Calendar(
            CalendarId(UUID.randomUUID()),
            nlpResult.schedule!!.calendar
        )
    }
}
