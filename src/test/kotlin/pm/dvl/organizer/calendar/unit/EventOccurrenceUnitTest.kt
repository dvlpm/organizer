package pm.dvl.organizer.calendar.unit

import net.fortuna.ical4j.model.TimeZone
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import pm.dvl.organizer.common.extensions.*

class EventOccurrenceUnitTest {
    @Test
    fun `test zoned date time`() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        val dateTime = "2022-01-01 06:00:00".toDateTime()
        val dateTimeEpochSecond = dateTime.time / 1000
        val timeZone = TimeZone.getTimeZone("GMT+4")

        val zonedDateTime = dateTime.shiftFromOffset(timeZone.toZoneOffset()).toZonedDateTime()
        val zonedDateTimeEpochSecond = zonedDateTime.toEpochSecond()

        Assertions.assertEquals(1641016800, dateTimeEpochSecond)
        Assertions.assertEquals(1641002400, zonedDateTimeEpochSecond)
    }
}
