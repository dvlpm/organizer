package pm.dvl.organizer.task

import kotlinx.coroutines.runBlocking
import net.fortuna.ical4j.model.Calendar
import org.axonframework.commandhandling.gateway.CommandGateway
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import pm.dvl.organizer.task.domain.command.CreateTaskCommand
import pm.dvl.organizer.task.domain.command.ProduceTaskOccurrenceCommand
import pm.dvl.organizer.schedule.domain.model.Schedule
import pm.dvl.organizer.tag.domain.model.RankedTagName
import pm.dvl.organizer.tag.domain.model.TagName
import pm.dvl.organizer.tag.infrastructure.input.facade.TagFacade
import pm.dvl.organizer.tag.infrastructure.input.facade.payload.ProvideRankedTagByRankedTagNamePayload
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskId
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.query.repository.TaskOccurrenceProjectionRepositoryInterface
import pm.dvl.organizer.task.domain.query.repository.TaskProjectionRepositoryInterface
import pm.dvl.organizer.user.domain.model.UserId
import java.time.ZonedDateTime
import java.util.*

@SpringBootTest
class TaskAcceptanceTest {
    @Autowired
    private lateinit var commandGateway: CommandGateway

    @Autowired
    private lateinit var taskProjectionRepository: TaskProjectionRepositoryInterface

    @Autowired
    private lateinit var taskOccurrenceProjectionRepository: TaskOccurrenceProjectionRepositoryInterface

    @Autowired
    private lateinit var tagFacade: TagFacade

    @Test
    fun `create task and check projections`() {
        val expectedTaskId = TaskId(UUID.randomUUID())
        val expectedUserId = UserId(UUID.randomUUID())
        val expectedTags = tagFacade.provideRankedTagByRankedTagName(
            ProvideRankedTagByRankedTagNamePayload(
            expectedUserId,
            listOf(RankedTagName(TagName("ScheduleTag"), "0"))
        )
        ).toMutableSet()
        val expectedTaskData = TaskData("schedule text", expectedUserId, tags = expectedTags)

        commandGateway.sendAndWait<TaskId>(
            CreateTaskCommand(
                expectedTaskId,
                expectedTaskData,
                Schedule(Calendar()),
            )
        )

        val taskOccurrenceId = TaskOccurrenceId(UUID.randomUUID())

        commandGateway.sendAndWait<TaskId>(
            ProduceTaskOccurrenceCommand(
                expectedTaskId,
                taskOccurrenceId,
                ZonedDateTime.now()
            )
        )

        runBlocking {
            val projectionFoundById = taskProjectionRepository.waitForFindOneOrNullById(expectedTaskId)

            Assertions.assertNotNull(projectionFoundById)

            val taskProjection = taskOccurrenceProjectionRepository.waitForFindOneOrNullById(taskOccurrenceId)

            Assertions.assertNotNull(taskProjection)
            Assertions.assertEquals(expectedTags, taskProjection!!.tags)
        }
    }
}
