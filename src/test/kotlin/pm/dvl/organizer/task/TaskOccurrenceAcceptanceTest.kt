package pm.dvl.organizer.task

import kotlinx.coroutines.runBlocking
import org.axonframework.commandhandling.gateway.CommandGateway
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import pm.dvl.organizer.tag.domain.model.RankedTagName
import pm.dvl.organizer.tag.domain.model.TagName
import pm.dvl.organizer.tag.infrastructure.input.facade.TagFacade
import pm.dvl.organizer.tag.infrastructure.input.facade.payload.ProvideRankedTagByRankedTagNamePayload
import pm.dvl.organizer.task.domain.command.CreateTaskOccurrenceCommand
import pm.dvl.organizer.task.domain.model.TaskData
import pm.dvl.organizer.task.domain.model.TaskOccurrenceId
import pm.dvl.organizer.task.domain.query.repository.TaskOccurrenceProjectionRepositoryInterface
import pm.dvl.organizer.user.domain.model.UserId
import java.util.*

@SpringBootTest
class TaskOccurrenceAcceptanceTest {
    @Autowired
    private lateinit var commandGateway: CommandGateway
    @Autowired
    private lateinit var tagFacade: TagFacade

    @Autowired
    private lateinit var taskProjectionRepository: TaskOccurrenceProjectionRepositoryInterface

    @Test
    fun `create task occurrence and check projection`() {
        val expectedUserId = UserId(UUID.fromString("7ec6ca62-467e-4d32-9a1c-7f51df107cc0"))
        val expectedParentTaskOccurrenceId = TaskOccurrenceId(UUID.randomUUID())

        commandGateway.sendAndWait<Unit>(
            CreateTaskOccurrenceCommand(
                expectedParentTaskOccurrenceId,
                TaskData("parent summary", expectedUserId),
            )
        )

        val expectedTaskOccurrenceId = TaskOccurrenceId(UUID.randomUUID())
        val expectedTags = tagFacade.provideRankedTagByRankedTagName(ProvideRankedTagByRankedTagNamePayload(
            expectedUserId,
            listOf(RankedTagName(TagName("SpecificTag"), "0"))
        )).toMutableSet()
        val expectedTaskData = TaskData(
            "summary",
            expectedUserId,
            tags = expectedTags
        )

        commandGateway.sendAndWait<Unit>(
            CreateTaskOccurrenceCommand(
                expectedTaskOccurrenceId,
                expectedTaskData,
                taskOccurrenceParentIds = mutableListOf(expectedParentTaskOccurrenceId)
            )
        )

        runBlocking {
            val projection = taskProjectionRepository.waitForFindOneOrNullById(expectedTaskOccurrenceId)

            Assertions.assertNotNull(projection)
            Assertions.assertTrue(projection!!.parents.contains(expectedParentTaskOccurrenceId))
            Assertions.assertEquals(expectedTags, projection!!.tags)
        }
    }
}
