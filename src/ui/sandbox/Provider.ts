import AbstractProvider from "@/AbstractProvider";
import { injectable } from "tsyringe";

@injectable()
export default class Provider implements AbstractProvider {
  doSome(): string {
    return "Hi from provider";
  }
}
