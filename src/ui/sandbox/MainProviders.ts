import { registry } from "tsyringe";
import Provider from "./Provider";
import AnotherProvider from "./AnotherProvider";
import AbstractProvider from "./AbstractProvider";

@registry([
  {token: MainProviders, useClass: Provider},
  {token: MainProviders, useClass: AnotherProvider}
])
export default class MainProviders extends AbstractProvider {}
