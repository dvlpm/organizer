import "reflect-metadata"
import { container } from "tsyringe";
import ServiceInterface from "./ServiceInterface";
import ServiceRegistry from "./ServiceRegistry";
import MainProviders from "./MainProviders";

let serviceRegistry = new ServiceRegistry();
let services = container.resolveAll<ServiceInterface>("ServiceInterface");
let providers = container.resolveAll(MainProviders);

services.map((service) => console.log(service.doSome()));
providers.map((provider) => console.log(provider.doSome()));
