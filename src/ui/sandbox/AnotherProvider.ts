import AbstractProvider from "@/AbstractProvider";

export default class AnotherProvider implements AbstractProvider {
  doSome(): string {
    return "Hi from another provider";
  }
}
