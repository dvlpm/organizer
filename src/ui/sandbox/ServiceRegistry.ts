import { registry } from "tsyringe";
import Service from "./Service";

@registry([{token: "ServiceInterface", useClass: Service}])
export default class ServiceRegistry {}
