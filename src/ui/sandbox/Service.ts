import ServiceInterface from "@/ServiceInterface";
import { injectable } from "tsyringe";

@injectable()
export default class Service implements ServiceInterface {
  doSome(): string {
    return "Hi from service";
  }
}
