import { Editor, NodeEntry } from "slate";
import { TaskNode } from "@/app/TaskNode";

export class EditorHelper {
  static currentTaskNodeEntry(editor): NodeEntry | undefined {
    let {selection} = editor

    return Editor.previous(
      editor,
      {
        at: selection,
        match: n => n.type === TaskNode.TYPE,
        mode: 'lowest'
      }
    )
  }

  static taskNodeEntries(editor): NodeEntry[] {
    let {selection} = editor

    return Array.from(Editor.nodes(
      editor,
      {
        at: selection,
        match: n => n.type === TaskNode.TYPE,
        mode: 'all'
      }
    ) ?? [])
  }

  static possibleParentTaskNodeEntry(editor, taskNodeEntry: NodeEntry): NodeEntry | undefined {
    const [taskNode, path] = taskNodeEntry

    return Editor.previous(
      editor,
      {
        at: path,
        match: n => n.type === TaskNode.TYPE
          // has depth less than passed taskNode meaning could be future parent
          && (taskNode.data?.depth ?? 0) >= (n.data?.depth ?? 0),
        mode: 'lowest'
      }
    ) ?? []
  }

  static childTaskNodeEntries(editor, taskNodeEntry: NodeEntry): NodeEntry[] {
    const [taskNode] = taskNodeEntry

    return Array.from(Editor.nodes(
      editor,
      {
        at: [],
        match: n => n.type === TaskNode.TYPE
          // is child of passed taskNode
          && (n?.data?.parents ?? []).includes(taskNode.data.id),
        mode: 'all'
      }
    ) ?? [])
  }

  static newChildTaskNodeEntries(editor, taskNodeEntry: NodeEntry): NodeEntry[] {
    const [taskNode, taskNodePath] = taskNodeEntry
    const [nextSameDepthTaskNode, nextSameDepthTaskNodePath] = Editor.next(
      editor,
      {
        at: taskNodePath,
        match: n => n.type === TaskNode.TYPE
          && (Math.max(taskNode.data?.depth - 1, 0)) === (n.data?.depth ?? 0),
        mode: 'highest'
      }
    ) ?? []

    const at = {
      anchor: {path: taskNodePath, offset: 0},
      focus: nextSameDepthTaskNodePath
        ? {path: nextSameDepthTaskNodePath, offset: 0}
        : {path: Editor.last(editor, [])?.[1], offset: 0}
    }

    const allWithDeeperDepth = Array.from(Editor.nodes(
      editor,
      {
        at,
        match: n => n.type === TaskNode.TYPE
          && (Math.max(taskNode.data?.depth - 1, 0)) < (n.data?.depth ?? 0)
          && n?.data?.id !== taskNode?.data?.id
          && !(n?.data?.parents?.includes(taskNode?.data?.id)),
        mode: 'lowest'
      }
    ) ?? [])

    const allWithDeeperDepthIds = []
    const taskNodeEntriesWithDeeperDepth = []

    for (let taskNodeEntryWithDeeperDepth of allWithDeeperDepth) {
      taskNodeEntriesWithDeeperDepth.push(taskNodeEntryWithDeeperDepth)
      allWithDeeperDepthIds.push(taskNodeEntryWithDeeperDepth[0].data.id)
    }

    const newChildTaskNodeEntries = []

    for (let taskNodeEntryWithDeeperDepth of taskNodeEntriesWithDeeperDepth) {
      if (!allWithDeeperDepthIds.includes(taskNodeEntryWithDeeperDepth?.[0]?.data?.parents?.[0] ?? undefined)) {
        newChildTaskNodeEntries.push(taskNodeEntryWithDeeperDepth)
      }
    }

    return newChildTaskNodeEntries
  }

  static parentTaskNodeEntry(editor, taskNodeEntry: NodeEntry): NodeEntry | undefined {
    const [taskNode, path] = taskNodeEntry

    return Editor.previous(
      editor,
      {
        at: path,
        match: n => n.type === TaskNode.TYPE
          && (taskNode?.data?.parents ?? []).includes(n.data.id),
        mode: 'lowest'
      }
    ) ?? []
  }

  static newParentTaskNodeEntry(editor, taskNodeEntry: NodeEntry): NodeEntry | undefined {
    const [taskNode, path] = taskNodeEntry

    return Editor.previous(
      editor,
      {
        at: path,
        match: n => n.type === TaskNode.TYPE
          && (Math.max(taskNode.data?.depth - 1, 0)) > (n.data?.depth ?? 0),
        mode: 'lowest'
      }
    ) ?? []
  }
}
