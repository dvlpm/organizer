import { Editor, NodeEntry, Operation as EditorOperation } from "slate";
import AbstractOperationApplier from "@/app/TabEditor/OperationApplier/AbstractOperationApplier";
import { TabEditor } from "@/app/TabEditor/TabEditor";
import Mutation from "@/app/TabEditor/Mutation";

export default class TabOperation {
  public readonly mutation: Mutation
  private cachedNodeEntry: NodeEntry | null = null
  private cachedParentNodeEntry: NodeEntry | null = null

  constructor(
    private editor: TabEditor,
    public editorOperation: EditorOperation,
    private appliedAppliers = []
  ) {
    this.mutation = this.editorOperation?.newProperties?.mutation ?? new Mutation()

    if (this.editorOperation.newProperties !== undefined) {
      delete this.editorOperation?.newProperties['mutation']

      this.editorOperation.newProperties = {
        ...this.editorOperation.newProperties,
        mutations: [
          ...(this.node?.mutations ?? []),
          this.mutation
        ],
      }
    }
  }

  get type() {
    return this.editorOperation.type
  }

  get text() {
    return this.editorOperation.text
  }

  get path() {
    return this.editorOperation.path
  }

  get offset() {
    return this.editorOperation.offset
  }

  get newProperties() {
    return this.editorOperation?.newProperties
  }

  set newProperties(newProperties) {
    return this.editorOperation.newProperties = newProperties
  }

  get node() {
    return this.editorOperation.node ?? this.nodeEntry?.[0]
  }

  get nodeEntryOrFail(): NodeEntry {
    return this.nodeEntry!
  }

  get nodeEntry(): NodeEntry | null {
    try {
      this.cachedNodeEntry = this.cachedNodeEntry ?? Editor.node(this.editor, this.editorOperation?.path)

      return this.cachedNodeEntry
    } catch (Error) {
      return null
    }
  }

  get parentNode() {
    return this.parentNodeEntry?.[0]
  }

  get parentNodeEntry(): NodeEntry | null {
    try {
      this.cachedParentNodeEntry = this.cachedParentNodeEntry
        ?? Editor.node(this.editor, this.editorOperation?.path?.slice(0, -1))

      return this.cachedParentNodeEntry
    } catch (Error) {
      return null
    }
  }

  get nodeHasMutation(): boolean {
    return this.node?.mutations?.some(mutation => mutation.id === this.mutation.id)
  }

  wasApplied(): boolean {
    return this.appliedAppliers.length > 0;
  }

  addApplier(applier: AbstractOperationApplier) {
    this.appliedAppliers.push(applier)
  }
}
