import { AbstractCommand } from "@/app/Command/AbstractCommand";
import { TabEditor } from "@/app/TabEditor/TabEditor";

export default class Tab {
  constructor(
    public editor: TabEditor,
    public commands: AbstractCommand[] = [],
    private updatedAt: Date = new Date(),
  ) {
  }

  pushCommands(...commands: AbstractCommand[]): this {
    this.commands.push(...commands)
    this.updateTimestamp()

    return this
  }

  updateTimestamp(): this {
    this.updatedAt = new Date()

    return this
  }
}
