import { Operation } from 'slate'

export default interface TabEditorApply {
  (operation: Operation): void
}