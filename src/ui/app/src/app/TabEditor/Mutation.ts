import { v4 } from 'uuid';

export default class Mutation {
  constructor(
    public id = v4(),
    public at = new Date()
  ) {
  }
  
  withNewDate(): Mutation {
    return new Mutation(
      this.id,
      new Date()
    )
  }
}
