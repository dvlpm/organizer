import AbstractOperationApplier from "@/app/TabEditor/OperationApplier/AbstractOperationApplier";
import { TaskNode } from "@/app/TaskNode";
import { RemoveTaskCommand } from "@/app/Command/RemoveTaskCommand";

export default class RemoveTaskOperationApplier extends AbstractOperationApplier {
  doApply(tab, operation, apply) {
    tab.pushCommands(new RemoveTaskCommand(operation.node.data.id))

    apply(operation.editorOperation)
  }

  protected supports(_: never, operation): boolean {
    return ['remove_node', 'merge_node'].includes(operation.type)
      && operation.node?.type === TaskNode.TYPE
  }
}
