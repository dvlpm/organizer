import AbstractOperationApplier from "@/app/TabEditor/OperationApplier/AbstractOperationApplier";

export default class LogOperationApplier extends AbstractOperationApplier {
  doApply(tab, operation, apply) {
    console.log(operation.type, {
      operation: operation.editorOperation,
      children: tab.editor.children,
      commands: tab.commands,
      appliedAppliers: operation.appliedAppliers
    })
  }

  protected supports(_: never, operation): boolean {
    return !(
      operation.type === 'set_selection' ||
      (operation.type === 'insert_node' && operation?.editorOperation?.node?.text === '')
    )
  }
}
