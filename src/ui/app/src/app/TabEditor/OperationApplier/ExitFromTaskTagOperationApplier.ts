import AbstractOperationApplier from "@/app/TabEditor/OperationApplier/AbstractOperationApplier";
import { Editor, Transforms } from "slate";
import { TaskTagNode } from "@/app/TaskTagNode";

export default class ExitFromTaskTagOperationApplier extends AbstractOperationApplier {
  doApply(tab, operation, apply) {
    // find end after current tag
    const afterTagPath = Editor.after(tab.editor, operation.path);

    // remove last space in tag
    apply({type: 'remove_text', path: operation.path, offset: operation.offset - 1, text: ' '})

    // add new text node right after tag
    Transforms.insertNodes(
      tab.editor,
      {text: ' '},
      {
        at: afterTagPath,
        select: true
      }
    )
  }

  protected supports(_: never, operation): boolean {
    return operation.type === 'insert_text'
      && operation.text === ' '
      && operation.parentNode?.type === TaskTagNode.TYPE
      && operation.parentNode?.children?.at(0)?.text.slice(-1) === ' '
  }
}
