import AbstractOperationApplier from "@/app/TabEditor/OperationApplier/AbstractOperationApplier";
import { registry } from "tsyringe";
import InsertTaskTagOperationApplier from "@/app/TabEditor/OperationApplier/InsertTaskTagOperationApplier";
import ExitFromTaskTagOperationApplier from "@/app/TabEditor/OperationApplier/ExitFromTaskTagOperationApplier";
import CreateTaskOperationApplier from "@/app/TabEditor/OperationApplier/CreateTaskOperationApplier";
import RemoveTaskOperationApplier from "@/app/TabEditor/OperationApplier/RemoveTaskOperationApplier";
import ChangeTaskStatusOperationApplier from "@/app/TabEditor/OperationApplier/ChangeTaskStatusOperationApplier";
import ChangeTaskDepthOperationApplier from "@/app/TabEditor/OperationApplier/ChangeTaskDepthOperationApplier";
import FallbackOperationApplier from "@/app/TabEditor/OperationApplier/FallbackOperationApplier";
import LogOperationApplier from "@/app/TabEditor/OperationApplier/LogOperationApplier";

@registry([
  {token: TabEditorOperationAppliers, useClass: InsertTaskTagOperationApplier},
  {token: TabEditorOperationAppliers, useClass: ExitFromTaskTagOperationApplier},
  {token: TabEditorOperationAppliers, useClass: CreateTaskOperationApplier},
  {token: TabEditorOperationAppliers, useClass: RemoveTaskOperationApplier},
  {token: TabEditorOperationAppliers, useClass: ChangeTaskStatusOperationApplier},
  {token: TabEditorOperationAppliers, useClass: ChangeTaskDepthOperationApplier},
  {token: TabEditorOperationAppliers, useClass: FallbackOperationApplier}, // should be after transforming appliers
  {token: TabEditorOperationAppliers, useClass: LogOperationApplier},
])
export default class TabEditorOperationAppliers extends AbstractOperationApplier {}
