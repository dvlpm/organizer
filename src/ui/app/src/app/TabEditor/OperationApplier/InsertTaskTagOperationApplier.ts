import AbstractOperationApplier from "@/app/TabEditor/OperationApplier/AbstractOperationApplier";
import Tab from "@/app/TabEditor/Tab";
import TabOperation from "@/app/TabEditor/TabOperation";
import { Transforms } from "slate";
import { TaskTagNode } from "@/app/TaskTagNode";

export default class InsertTaskTagOperationApplier extends AbstractOperationApplier {
  doApply(tab: Tab, operation: TabOperation) {
    Transforms.insertNodes(
      tab.editor,
      {type: TaskTagNode.TYPE, children: [{text: '#'}]},
      {
        select: true,
      }
    )
  }

  protected supports(tab: Tab, operation: TabOperation): boolean {
    return operation.type === 'insert_text' && operation.text === '#'
  }
}
