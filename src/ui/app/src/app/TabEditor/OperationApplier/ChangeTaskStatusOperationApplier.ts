import AbstractOperationApplier from "@/app/TabEditor/OperationApplier/AbstractOperationApplier";
import { ChangeTaskStatusCommand } from "@/app/Command/ChangeTaskStatusCommand";

export default class ChangeTaskStatusOperationApplier extends AbstractOperationApplier {
  doApply(tab, operation, apply) {
    tab.pushCommands(new ChangeTaskStatusCommand(
      operation.node.data.id,
      operation.newProperties.data.status
    ))

    apply(operation.editorOperation)
  }

  protected supports(_: never, operation): boolean {
    return operation.type === 'set_node'
      && operation?.newProperties?.data?.status !== undefined
      && operation.node?.data?.status !== operation?.newProperties?.data?.status
  }
}
