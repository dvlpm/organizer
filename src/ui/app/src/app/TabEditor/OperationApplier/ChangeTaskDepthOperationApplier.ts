import AbstractOperationApplier from "@/app/TabEditor/OperationApplier/AbstractOperationApplier";
import { EditorHelper } from "@/app/EditorHelper";
import { RemoveParentFromTaskCommand } from "@/app/Command/RemoveParentFromTaskCommand";
import { AddParentToTaskCommand } from "@/app/Command/AddParentToTaskCommand";
import { Transforms } from "slate";
import Tab from "@/app/TabEditor/Tab";
import TabOperation from "@/app/TabEditor/TabOperation";
import TabEditorApply from "@/app/TabEditor/TabEditorApply";

export default class ChangeTaskDepthOperationApplier extends AbstractOperationApplier {
  doApply(tab, operation, apply) {
    if (operation.newProperties.data.depth < 0) {
      return
    }

    if (operation.node.data.depth > operation.newProperties.data.depth) {
      this.removeParentFromNode(tab, operation, apply)
    } else {
      this.addParentToNode(tab, operation, apply)
    }
  }

  private removeParentFromNode(tab: Tab, operation: TabOperation, apply: TabEditorApply): void {
    const [parentTaskNode] = EditorHelper.parentTaskNodeEntry(tab.editor, operation.nodeEntryOrFail)

    if (
      parentTaskNode
      // check that node is child of parent
      && operation.node.data.parents.includes(parentTaskNode.data.id)
      // new depth of task is less or equal parent depth
      && (operation.newProperties?.data?.depth ?? 0) <= (parentTaskNode?.data?.depth ?? 0)
    ) {
      const [newPossibleParentNode] = EditorHelper.newParentTaskNodeEntry(tab.editor, operation.nodeEntryOrFail)

      operation.newProperties = {
        ...operation.newProperties,
        data: {
          ...operation.newProperties.data,
          parents: newPossibleParentNode
            ? [newPossibleParentNode.data.id]
            : [],
        }
      }

      tab.pushCommands(new RemoveParentFromTaskCommand(
        operation.node.data.id,
        parentTaskNode.data.id
      ));
      if (newPossibleParentNode) {
        tab.pushCommands(new AddParentToTaskCommand(
          operation.node.data.id,
          newPossibleParentNode.data.id
        ))
      }
    }

    apply(operation.editorOperation)

    // search child nodes
    const childTaskNodeEntries = EditorHelper.childTaskNodeEntries(tab.editor, operation.nodeEntryOrFail)

    for (let childTaskNodeEntry of childTaskNodeEntries) {
      const [childTaskNode, childTaskNodePath] = childTaskNodeEntry;

      Transforms.setNodes(
        tab.editor,
        {
          data: {
            ...childTaskNode.data,
            depth: childTaskNode.data.depth - 1,
          },
          mutation: operation.mutation.withNewDate()
        },
        {
          at: childTaskNodePath
        }
      )
    }

    const newChildTaskNodeEntries = EditorHelper.newChildTaskNodeEntries(tab.editor, operation.nodeEntryOrFail)

    for (let newChildTaskNodeEntry of newChildTaskNodeEntries) {
      const [newChildTaskNode, newChildTaskNodePath] = newChildTaskNodeEntry;

      (newChildTaskNode?.data?.parents?.length > 0
        ? newChildTaskNode.data.parents.map(parentTaskId => {
          return new RemoveParentFromTaskCommand(newChildTaskNode.data.id, parentTaskId)
        })
        : []).forEach(command => tab.pushCommands(command))

      Transforms.setNodes(
        tab.editor,
        {
          ...newChildTaskNode,
          data: {
            ...newChildTaskNode.data,
            parents: [operation.node.data.id]
          }
        },
        {
          at: newChildTaskNodePath
        }
      )

      tab.pushCommands(new AddParentToTaskCommand(newChildTaskNode.data.id, operation.node.data.id))
    }
  }

  private addParentToNode(tab: Tab, operation: TabOperation, apply: TabEditorApply) {

    // search possible parent in tree if not passed
    const [possibleParentTaskNode] = EditorHelper.possibleParentTaskNodeEntry(tab.editor, operation.nodeEntryOrFail)

    if (
      possibleParentTaskNode
      // check that node is not already has suitable parent
      && !(operation.node?.data?.parents?.includes(possibleParentTaskNode.data.id))
    ) {
      operation.newProperties = {
        ...operation.newProperties,
        data: {
          ...operation.newProperties.data,
          parents: [
            possibleParentTaskNode.data.id
          ]
        },
      }
      tab.pushCommands(new AddParentToTaskCommand(
        operation.node.data.id,
        possibleParentTaskNode.data.id
      ));
      (operation.node?.data?.parents?.length > 0
        ? operation.node.data.parents.map(parentTaskId => {
          return new RemoveParentFromTaskCommand(operation.node.data.id, parentTaskId)
        })
        : []).forEach(command => tab.pushCommands(command))
    }

    apply(operation.editorOperation)

    // search child nodes
    const childTaskNodeEntries = EditorHelper.childTaskNodeEntries(tab.editor, operation.nodeEntryOrFail)

    for (let childTaskNodeEntry of childTaskNodeEntries) {
      const [childTaskNode, childTaskNodePath] = childTaskNodeEntry;

      Transforms.setNodes(
        tab.editor,
        {
          data: {
            ...childTaskNode.data,
            depth: childTaskNode.data.depth + 1,
          },
          mutation: operation.mutation.withNewDate()
        },
        {
          at: childTaskNodePath
        }
      )
    }
  }

  protected supports(_: never, operation): boolean {
    return operation.type === 'set_node'
      && operation.newProperties?.data?.depth !== undefined
      && operation.node?.data?.depth !== operation.newProperties.data.depth
  }
}
