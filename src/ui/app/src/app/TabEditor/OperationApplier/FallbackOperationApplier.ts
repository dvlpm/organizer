import AbstractOperationApplier from "@/app/TabEditor/OperationApplier/AbstractOperationApplier";

export default class FallbackOperationApplier extends AbstractOperationApplier {
  doApply(tab, operation, apply) {
    apply(operation.editorOperation)
  }

  protected supports(_: never, operation): boolean {
    return !operation.wasApplied()
  }
}
