import TabOperation from "@/app/TabEditor/TabOperation";
import Tab from "@/app/TabEditor/Tab";
import TabEditorApply from "@/app/TabEditor/TabEditorApply";

export default abstract class AbstractOperationApplier {
  apply(tab: Tab, operation: TabOperation, apply: TabEditorApply): void {
    if (!this.supports(tab, operation, apply)) {
      return
    }

    if (operation.nodeHasMutation) {
      return
    }

    this.doApply(tab, operation, apply)

    tab.updateTimestamp()

    operation.addApplier(this)
  }

  protected doApply(tab: Tab, operation: TabOperation, apply: TabEditorApply): void {}

  protected supports(tab: Tab, operation: TabOperation, apply: TabEditorApply): boolean {
    return true
  }
}
