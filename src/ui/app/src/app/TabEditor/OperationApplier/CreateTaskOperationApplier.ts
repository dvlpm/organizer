import AbstractOperationApplier from "@/app/TabEditor/OperationApplier/AbstractOperationApplier";
import CreateTaskCommand from "@/app/Command/CreateTaskCommand";
import { TaskNode } from "@/app/TaskNode";
import { TaskDataGenerator } from "@/app/TaskDataGenerator";

export default class CreateTaskOperationApplier extends AbstractOperationApplier {
  doApply(tab, operation, apply) {
    const taskData = new TaskDataGenerator().duplicate(operation.node.data)

    tab.pushCommands(new CreateTaskCommand(taskData))

    if (operation.editorOperation.properties) {
      operation.editorOperation.properties.data = taskData
    }

    if (operation.editorOperation.node) {
      operation.editorOperation.node.data = taskData
    }

    apply(operation.editorOperation)
  }

  supports(_: never, operation): boolean {
    return ['insert_node', 'split_node'].includes(operation.type)
      && operation.node?.type === TaskNode.TYPE
  }
}
