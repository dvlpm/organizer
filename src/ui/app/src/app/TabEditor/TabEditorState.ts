export default class TabEditorState {
  constructor(
    public children: Array<any> = [],
    public selection: Object = {}
  ) {
  }
}
