import AbstractOnKeyDownHandler from "@/app/TabEditor/OnKeyDownHandler/AbstractOnKeyDownHandler";
import { registry } from "tsyringe";
import TabOnKeyDownEventHandler from "@/app/TabEditor/OnKeyDownHandler/TabOnKeyDownEventHandler";

@registry([
  {token: TabEditorOnKeyDownHandlers, useClass: TabOnKeyDownEventHandler}
])
export default class TabEditorOnKeyDownHandlers extends AbstractOnKeyDownHandler {}
