import type AbstractOnKeyDownHandler from "@/app/TabEditor/OnKeyDownHandler/AbstractOnKeyDownHandler";
import { EditorHelper } from "@/app/EditorHelper";
import { Transforms } from "slate";
import Tab from "@/app/TabEditor/Tab";
import Mutation from "@/app/TabEditor/Mutation";

export default class TabOnKeyDownEventHandler implements AbstractOnKeyDownHandler {
  handle(tab: Tab, event: KeyboardEvent): void {
    if (!this.supports(tab, event)) {
      return
    }
    
    event.preventDefault()
    const taskNodeEntries = EditorHelper.taskNodeEntries(tab.editor)
    const mutation = new Mutation();

    for (let taskNodeEntry of taskNodeEntries) {
      const [taskNode, taskNodePath] = taskNodeEntry

      if (event.shiftKey) {
        Transforms.setNodes(
          tab.editor,
          {
            data: {
              ...taskNode.data,
              depth: taskNode.data.depth - 1,
            },
            mutation
          },
          {
            at: taskNodePath
          }
        )

        continue
      }

      Transforms.setNodes(
        tab.editor,
        {
          data: {
            ...taskNode.data,
            depth: taskNode.data.depth + 1,
          },
          mutation
        },
        {
          at: taskNodePath
        }
      )
    }
  }

  private supports(tab: Tab, event: KeyboardEvent): boolean {
    return event.key === 'Tab';
  }
}
