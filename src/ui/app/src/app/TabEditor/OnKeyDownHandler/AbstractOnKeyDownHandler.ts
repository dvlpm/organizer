import Tab from "@/app/TabEditor/Tab";

export default abstract class AbstractOnKeyDownHandler {
  handle(tab: Tab, event: KeyboardEvent): void {
  }
}
