import { BaseEditor } from 'slate'
import { ReactEditor } from 'slate-react'
import { HistoryEditor } from 'slate-history'

export type TabEditor = BaseEditor & ReactEditor & HistoryEditor
