import { TaskData } from "@/app/TaskData";
import { TaskTagNode } from "@/app/TaskTagNode";
import { TaskNode } from "@/app/TaskNode";
import { Descendant } from "slate";

export const initialValue = [
  {
    type: TaskNode.TYPE,
    data: {
      id: '94539861-20d9-44fa-ae18-1bbb30f4e56c',
      status: 'DONE',
      updatedAt: new Date(),
      depth: 0,
    } as TaskData,
    children: [
      {
        text: 'Summary '
      },
      {
        type: TaskTagNode.TYPE,
        children: [
          {
            text: '#task'
          }
        ]
      },
      {
        text: ''
      },
    ],
    mutations: [],
  },
  {
    type: TaskNode.TYPE,
    data: {
      id: '49c2a69d-b58d-4232-8a9f-71bdb51cdac8',
      status: 'PENDING',
      depth: 1,
      updatedAt: new Date(),
      parents: [
        '94539861-20d9-44fa-ae18-1bbb30f4e56c',
      ]
    },
    children: [
      {
        text: 'Children'
      },
    ],
    mutations: [],
  },
  {
    type: TaskNode.TYPE,
    data: {
      id: '5001bb66-c111-4712-b028-df28f1a8daf4',
      summary: 'Summary #tag',
      status: 'PENDING',
      updatedAt: new Date(),
      depth: 0,
    },
    children: [
      {
        text: 'Summary '
      },
      {
        type: TaskTagNode.TYPE,
        data: {
          id: '2f50fd2a-a2a7-4732-8ef3-4613f1fda159',
          name: 'tag',
          updatedAt: new Date(),
        },
        children: [
          {
            text: '#tag'
          }
        ]
      },
      {
        text: ''
      },
    ],
    mutations: [],
  },
] as Descendant[]
