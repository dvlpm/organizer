export class TaskData {
  constructor(
    public id: String,
    public status: 'DONE' | 'PENDING',
    public children: { id: String }[],
    public parents: { id: String }[],
    public updatedAt: String,
    public depth: Number = 0
  ) {
  }
}
