import { TaskData } from "@/app/TaskData";
import { v4 } from 'uuid';

export class TaskDataGenerator {
  generate() {
    return {
      id: v4(),
      status: 'PENDING',
      parents: [],
      updatedAt: Date.now().toString(),
      depth: 0
    } as TaskData
  }

  duplicate(taskNode: TaskData) {
    return {
      ...this.generate(),
      parents: taskNode.parents,
      depth: taskNode.depth
    } as TaskData
  }
}
