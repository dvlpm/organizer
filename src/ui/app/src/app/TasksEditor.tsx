/**
 * SAVING FLOW
 *
 * Add updated-at property to tab
 * Send tab to backend
 * Filter by updated-at and ignore older edits
 * Apply commands from all nodes & update command statuses
 */
/**
 * Mutations - entities that calculates future node data transformation
 * - used when user input is not direct operation in editor such as insert/text
 * but something like checkbox changing or hotkey
 *
 * OperationAppliers - entities that handles editor operations and mixes them with commands. Basically every command
 * should be triggered by operation applier because of history (ctrl+z and ctrl+shift+z)
 * that will apply compensating logic through operation)
 */

import React, { useCallback, useMemo } from 'react'
import { Editable, Slate, withReact, } from 'slate-react'
import { createEditor, Editor, Operation, Transforms, } from 'slate'
import { withHistory } from 'slate-history'
import { initialValue } from "@/app/InitialValue";
import { TaskTagNode } from "@/app/TaskTagNode";
import { Text } from "@/app/Component/Text";
import { Element } from "@/app/Component/Element";
import { container } from "tsyringe";
import TabEditorOnKeyDownHandlers from "@/app/TabEditor/OnKeyDownHandler/TabEditorOnKeyDownHandlers";
import { TabEditor } from "@/app/TabEditor/TabEditor";
import Tab from "@/app/TabEditor/Tab";
import TabEditorOperationAppliers from "@/app/TabEditor/OperationApplier/TabEditorOperationAppliers";
import TabOperation from "@/app/TabEditor/TabOperation";

const TasksEditor = () => {
  const renderElement = useCallback(props => <Element {...props} />, [])
  const tab = useMemo(
    () => {
      const editor = withReact(
        withTaskTags(
          withHistory(
            createEditor()
          )
        )
      ) as TabEditor

      return withOperationBus(
        new Tab(editor)
      )
    },
    []
  ) as Tab;
  const handlers = container.resolveAll(TabEditorOnKeyDownHandlers);

  return (
    <Slate editor={tab.editor} initialValue={initialValue}>
      <Editable
        onKeyDown={(event) => {
          handlers.forEach(handler => handler.handle(tab, event.nativeEvent))
        }}
        renderElement={renderElement}
        renderLeaf={props => <Text {...props} />}
        spellCheck
        autoFocus
      />
    </Slate>
  )
}

const withOperationBus = (tab: Tab) => {
  const editor = tab.editor
  const {apply} = editor
  const appliers = container.resolveAll(TabEditorOperationAppliers)

  editor.apply = (operation: Operation) => {
    const tabOperation = new TabOperation(editor, operation)

    appliers.forEach(applier => applier.apply(tab, tabOperation, apply))
  }

  return tab
}

const withTaskTags = (editor: Editor) => {
  const types = [TaskTagNode.TYPE];
  const {isInline, normalizeNode} = editor

  editor.isInline = element => types.includes(element.type) || isInline(element)

  editor.normalizeNode = ([node, path]) => {
    if (!types.includes(node.type)) {
      return normalizeNode([node, path])
    }

    // if tag node has no text so remove it
    if (node.children.at(0)?.text == '') {
      Transforms.removeNodes(editor, {
        at: path
      })
    }

    // if tag was splat or somehow else left without # unwrap it
    if (!node.children.at(0)?.text?.includes('#') && node.children.at(0)?.text !== '') {
      Transforms.unwrapNodes(editor, {
        at: path
      })
    }

    // if tag is last element in the node then add space
    const nextNode = Editor.next(editor, {at: path, mode: "lowest"})

    if (nextNode === undefined) {
      Transforms.insertNodes(
        editor,
        {text: ''},
        {
          at: path,
          select: false
        }
      )
    }
  }

  return editor
}

export default TasksEditor
