import { v4 } from 'uuid';

export abstract class AbstractCommand {
  commandId: string
  type: string
  createdAt: Date

  protected constructor(type: string) {
    this.commandId = v4();
    this.type = type;
    this.createdAt = new Date();
  }
}