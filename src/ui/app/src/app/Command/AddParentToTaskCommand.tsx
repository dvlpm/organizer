import { AbstractCommand } from "@/app/Command/AbstractCommand";

export class AddParentToTaskCommand extends AbstractCommand {
  constructor(private taskId: string, private parentTaskId: string) {
    super('add-parent-to-task');
  }
}
