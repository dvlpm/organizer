import { AbstractCommand } from "@/app/Command/AbstractCommand";

export class RemoveTaskCommand extends AbstractCommand {
  constructor(private taskId: string) {
    super('remove-task');
  }
}