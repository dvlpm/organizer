import { TaskData } from "@/app/TaskData";
import { AbstractCommand } from "@/app/Command/AbstractCommand";

export default class CreateTaskCommand extends AbstractCommand {
  constructor(private taskData: TaskData) {
    super('create-task');
  }
}
