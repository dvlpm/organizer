import { AbstractCommand } from "@/app/Command/AbstractCommand";

export class RemoveParentFromTaskCommand extends AbstractCommand {
  constructor(private taskId: string, private parentTaskId: string) {
    super('remove-parent-from-task');
  }
}