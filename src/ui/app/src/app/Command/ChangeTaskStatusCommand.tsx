import { AbstractCommand } from "@/app/Command/AbstractCommand";

export class ChangeTaskStatusCommand extends AbstractCommand {
  constructor(private taskId: string, private status: string) {
    super('change-task-status');
  }
}