import { TaskDataGenerator } from "@/app/TaskDataGenerator";
import { TaskNode } from "@/app/TaskNode";

export class TaskNodeGenerator {
  generate() {
    return {
      type: TaskNode.TYPE,
      data: new TaskDataGenerator().generate(),
      children: [
        {
          text: ''
        },
      ],
    }
  }
}