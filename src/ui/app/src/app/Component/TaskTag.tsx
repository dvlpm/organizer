import { useReadOnly } from "slate-react";
import { css } from '@emotion/css';
import { InlineChromiumBugfix } from "@/app/Component/InlineChromiumBugfix";
import React from "react";

export const TaskTag = ({attributes, children, element}) => {
  const readOnly = useReadOnly()

  return <span
    className={css`
      padding: 1px 2px;
      background: brown;
      border-radius: 5px;
    `}
    {...attributes}
    contentEditable={!readOnly}
    suppressContentEditableWarning
  >
    <InlineChromiumBugfix/>
    {children}
    <InlineChromiumBugfix/>
  </span>
}
