import { ReactEditor, useReadOnly, useSlateStatic } from "slate-react";
import { css } from '@emotion/css';
import { Element as SlateElement, Transforms } from "slate";
import React from "react";

export const Task = ({attributes, children, element}) => {
  const editor = useSlateStatic()
  const readOnly = useReadOnly()
  const {data} = element
  const isDone = data.status === 'DONE'
  return (
    <span
      {...attributes}
      className={css`
        display: flex;
        flex-direction: row;
        align-items: center;

        & + & {
          margin-top: 0;
        }
      `}
    >
      <span
        contentEditable={false}
        className={css`
          margin-right: 0.75em;
          tab-size: 40px;
        `}
      >
        {data?.depth > 0 ? [
          ...Array(data?.depth),
        ].map((value: undefined, index: number) => (
          <span key={index} className={css`
            margin-left: 1em;
          `}></span>
        )) : null}
        <input
          type="checkbox"
          checked={isDone}
          onChange={event => {
            // TODO move to mutationHandler?
            const path = ReactEditor.findPath(editor, element)
            const targetStatus = event.target.checked ? 'DONE' : 'PENDING'
            const newProperties: Partial<SlateElement> = {
              data: {
                ...data,
                status: targetStatus
              },
            }
            Transforms.setNodes(editor, newProperties, {
              at: path,
            })
          }}
        />
      </span>
      <span
        contentEditable={!readOnly}
        suppressContentEditableWarning
        className={css`
          flex: 1;
          opacity: ${isDone ? 0.666 : 1};
          text-decoration: ${!isDone ? 'none' : 'line-through'};

          &:focus {
            outline: none;
          }
        `}
      >
        {children}
      </span>
    </span>
  )
}
