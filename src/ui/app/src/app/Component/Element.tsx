import { TaskTagNode } from "@/app/TaskTagNode";
import { TaskTag } from "@/app/Component/TaskTag";
import { TaskNode } from "@/app/TaskNode";
import { Task } from "@/app/Component/Task";
import React from "react";

export const Element = props => {
  const {attributes, children, element} = props

  switch (element.type) {
    case TaskTagNode.TYPE:
      return <TaskTag {...props} />
    case TaskNode.TYPE:
      return <Task {...props} />
    default:
      return <p {...attributes}>{children}</p>
  }
}