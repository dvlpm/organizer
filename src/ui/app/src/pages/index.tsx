import TasksEditor from "@/app/TasksEditor";
import RootLayout from "@/layout/RootLayout";
import Head from "next/head";
import { NextPageWithLayout } from "@/pages/_app";
import { ReactElement } from "react";

const Home: NextPageWithLayout = () => {
  return <>
    <Head>
      <title>Organizer</title>
    </Head>
    <main>
      <TasksEditor/>
    </main>
  </>
}

Home.getLayout = function getLayout(page: ReactElement) {
  return (
    <RootLayout>
      {page}
    </RootLayout>
  )
}

export default Home
