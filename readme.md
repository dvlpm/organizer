# Organizer

## Pre

#### Run

```shell
make cp-env
```

#### Fill env.local:

```dotenv
DOCKER_MACHINE=#name of docker machine you using to make IDE configurations work
TELEGRAM_BOT_TOKEN=#real telegram bot token for telegram bot testing
TELEGRAM_BOT_WEBHOOK_HOST=#public domain for telegram webhook
```

#### Public

Use ngrok to get public domain for telegram webhook (SSL REQUIRED, USE HTTPS)

```shell
brew install ngrok
make ngrok
```

## Init

```shell
make init #use for the first time
```

## Build

```shell
make build #build app/node/nginx images and app.jar
make build-app-jar #Build only app jar
```

## Run

```shell
make up-infrastructure #up temporal postgres and other containers
make up #up app container
make ups #up app container without daemon

make up-web-app-telegram #up web-app container
make up-nginx #up nginx container
```

# Known issues

## Serializer

There is some strange behaviour with serializer - 
it's just ignores last property of a class if its type is array while deserialization.

```
Example: Task::children hadn't been deserialized until it was moved above schedule.
```

## Persistence

### Mutable sets

Mutable sets can't be not null because of some jpa specifics. 
That's why nullable list property and non-nullable getter are needed.

```
Example: pm.dvl.organizer.task.domain.query.projection.TaskProjection::_children
```

### Unable to instantiate default tuplizer

FIX:

* Check @Persistable annotation and : Serializer implementation
* Check that properties has setters (change `val` to `var` in data class)

### Nested sets cascade operations not working

It's working OK until it is placed right under ```<hibernate-mapping><class>...```

```xml
<hibernate-mapping>
    <class...>
        ...
        <set name="tags"
             table="task_occurrence_ranked_tags"
             lazy="extra"
             fetch="join"
             cascade="all"
        >
            <key not-null="true">
                <column name="task_occurrence_id"/>
            </key>
            <composite-element class="pm.dvl.organizer.tag.domain.model.RankedTag">
                <nested-composite-element class="pm.dvl.organizer.tag.domain.model.TagId" name="tagId">
                    <property name="value" column="tag_id" not-null="true"/>
                </nested-composite-element>
                <property name="rank" column="tag_rank" not-null="true"/>
            </composite-element>
        </set>
        ...
    </class...>
</hibernate-mapping>
```

AND DOES NOT WORK INSIDE OTHER COMPONENT (one or more level deeper):

```xml
<hibernate-mapping>
    <class...>
        <component...>
            <set...></set...>
        </component...>
    </class...>
</hibernate-mapping>
```
