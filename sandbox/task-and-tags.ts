// TODO make high order tag definition related by current tag (had no parent in results, not in general)
// TODO make tag order editable by user somehow

class Data {
    constructor(
        public tasks: Task[],
        public tags: Tag[]
    ) {
    }
}

class Task {
    constructor(
        public id: string,
        public tags: string[],
        public children: string[] = [],
        public parents: string[] = [],
        public isDisplayed: boolean = false
    ) {
    }

    *display(tasks: Tasks): any {
        if (this.isDisplayed) {
            return
        }

        this.isDisplayed = true
        yield this

        for (let childrenTaskId of this.children) {
            yield *tasks.findById(childrenTaskId).display(tasks)
        }
    }
}

class Tag {
    constructor(
        public id: string,
        public name: string,
        public children: string[] = [],
        public parents: string[] = [],
        public isDisplayed: boolean = false
    ) {
    }

    hasChildrenWithId(tags: Tags, targetChildrenId: string): boolean {
        if (this.children.includes(targetChildrenId)) {
            return true
        }

        let childrenHasChildrenWithId = false

        for (let childrenId of this.children) {
            const tag = tags.findById(childrenId)

            childrenHasChildrenWithId = childrenHasChildrenWithId || tag.hasChildrenWithId(tags, targetChildrenId)
        }

        return childrenHasChildrenWithId
    }

    *display(tags: Tags, tasks: Tasks): any {
        if (this.isDisplayed || (this.children.length === 0 && tasks.findAllByTagNotDisplayed(this).length === 0)) {
            return
        }

        yield this
        this.isDisplayed = true

        let childTasks = tasks.findAllByTag(this)

        for (let childTask of childTasks) {
            yield *childTask.display(tasks)
        }

        for (let childrenTagId of this.children) {
            yield *tags.findById(childrenTagId).display(tags, tasks)
        }
    }
}

class Tasks {
    constructor(public tasks: Task[]) {
    }

    findById(id: string): Task {
        const task = this.tasks.find((task) => task.id === id)

        if (task === undefined) {
            throw Error
        }

        return task
    }

    findAllByTag(tag: Tag): Task[] {
        return this.tasks.filter((task) => task.tags.includes(tag.name))
    }

    findAllByTagNotDisplayed(tag: Tag): Task[] {
        return this.findAllByTag(tag).filter((task) => !task.isDisplayed)
    }
}

class Tags {
    constructor(public tags: Tag[]) {
    }

    findById(id: string): Tag {
        const tag = this.tags.find((tag) => tag.id === id)

        if (tag === undefined) {
            throw Error
        }

        return tag
    }

    findByName(name: string): Tag {
        const tag = this.tags.find((tag) => tag.name === name)

        if (tag === undefined) {
            throw Error
        }

        return tag
    }
}

class Displayer {
    constructor(private tags: Tags, private tasks: Tasks) {
    }

    *display(): any {
        for (const tag of this.tags.tags) {
            if (tag.parents.length === 0) {
                yield *tag.display(this.tags, this.tasks)
            }
        }
    }
}

main();

function main() {
    const task = getData()
    const tags = new Tags(task.tags)
    const tasks = new Tasks(task.tasks)

    prepareTaskTags(tasks, tags)

    const elementsToDisplay = [...new Displayer(
        new Tags(task.tags),
        new Tasks(task.tasks)
    ).display()]

    elementsToDisplay.map((element) => console.log(element))
}

function prepareTaskTags(tasks: Tasks, tags: Tags) {
    for (let task of tasks.tasks) {
        for (let currentTagName of task.tags) {
            const currentTag = tags.findByName(currentTagName)

            if (currentTag.children.length === 0) {
                continue
            }

            for (let childrenTagName of task.tags) {
                const childrenTag = tags.findByName(childrenTagName)

                if (currentTag.hasChildrenWithId(tags, childrenTag.id)) {
                    task.tags = task.tags.filter((tag) => tag === childrenTagName)
                }
            }
        }
    }
}

function getData() {
    return new Data([
            new Task(
                "1",
                ["gym", "sport"],
                ["2", "4"]
            ),
            new Task(
                "2",
                ["gym", "sport", "exercise"],
                [],
                ["1"]
            ),
            new Task(
                "4",
                ["gym", "sport", "exercise"],
                [],
                ["1"]
            ),
            new Task(
                "3",
                ["work"],
            ),
        ],
        [
            new Tag(
                "11",
                "sport",
                ["22"]
            ),
            new Tag(
                "22",
                "gym",
                ["33"],
                ["11"]
            ),
            new Tag(
                "33",
                "exercise",
                [],
                ["22"]
            ),
            new Tag(
                "44",
                "work",
            ),
        ],
    )
}
