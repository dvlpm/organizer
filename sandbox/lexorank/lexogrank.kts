data class Item(val id: String, var rank: String)

fun moveItemToPosition(items: MutableList<Item>, itemId: String, newPosition: Int) {
    val itemToMove = items.find { it.id == itemId } ?: return
    items.remove(itemToMove)
    items.add(newPosition, itemToMove)

    val totalItems = items.size

    itemToMove.rank = (newPosition + 1).toString().padStart(totalItems.toString().length, '0')
}

fun main() {
    val items = mutableListOf(
        Item("item3", "0"),
        Item("item2", "1"),
        Item("item1", "2"),
        Item("item4", "3")
    )

    items.forEach { item ->
        println("Item: ${item.id}, Rank: ${item.rank}")
    }

    val itemId = "item2"
    val newPosition = 0

    moveItemToPosition(items, itemId, newPosition)

    items.forEach { item ->
        println("Item: ${item.id}, Rank: ${item.rank}")
    }
}

main()
