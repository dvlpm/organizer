data class Item(val id: String, var score: Double, var rank: Double)

fun calculateLexorank(items: List<Item>): List<Item> {
    val sortedItems = items.sortedByDescending { it.score }
    val totalItems = items.size
    val maxRank = totalItems.toDouble() + 1.0

    for (i in 0 until totalItems) {
        val currentItem = sortedItems[i]
        val previousItem = if (i > 0) sortedItems[i - 1] else null
        val nextItem = if (i < totalItems - 1) sortedItems[i + 1] else null

        val gap = if (previousItem != null && nextItem != null) {
            (nextItem.rank - previousItem.rank) / 2.0
        } else if (previousItem != null) {
            previousItem.rank + 1.0
        } else if (nextItem != null) {
            nextItem.rank - 1.0
        } else {
            maxRank / 2.0
        }

        currentItem.rank = if (currentItem.score == 0.0) {
            if (previousItem != null) {
                previousItem.rank + 1.0
            } else {
                1.0
            }
        } else {
            if (previousItem != null && currentItem.score == previousItem.score) {
                previousItem.rank
            } else {
                previousItem?.rank?.plus(gap) ?: (gap / 2.0)
            }
        }
    }

    return sortedItems.sortedBy { it.rank }
}

fun main() {
    val items = listOf(
        Item("item1", 10.0, 0.0),
        Item("item2", 5.0, 0.0),
        Item("item3", 7.0, 0.0),
        Item("item4", 3.0, 0.0)
    )

    val rankedItems = calculateLexorank(items)
    rankedItems.forEach { item ->
        println("Item: ${item.id}, Rank: ${item.rank}")
    }
}

main()
