#!/bin/sh

set -e

injectList=/usr/local/etc/inject.list

if [ -f "$injectList" ]; then
  whoami

  echo '-- Injecting list --'

  echo
  for i in $(cat $injectList); do
      echo "Injecting [$(ls -l "$i")]"
      inject_envs $i
  done
  echo "-- Injecting done --"
fi

