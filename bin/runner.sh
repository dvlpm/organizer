#!/usr/bin/env bash

PROJECT_DIR="$(cd $(dirname -- "$(readlink -f ${BASH_SOURCE[0]})")/..; pwd)"
VOLUME_DIR=/root/www
WORKING_DIR=$VOLUME_DIR"${PWD/$PROJECT_DIR/}"

docker run -w $WORKING_DIR -v $PROJECT_DIR:$VOLUME_DIR $@
